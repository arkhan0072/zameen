<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Feature extends Model
{
    use SoftDeletes;

    public static function onlyTrashed()
    {
    }

    public function category()
    {

        return $this->belongsTo('App\FeatureCategory');
    }
    public function listing()
    {

        return $this->belongsToMany('App\Listing');
    }

    public function project(){
        return $this->belongsToMany('App\Project');
    }
}

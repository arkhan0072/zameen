<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    public function image()
    {
        return $this->morphMany('App\Photo', 'imageable');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function address(){
        return $this->hasMany('App\Address');
    }

    public function project(){
        return $this->belongsTo('App\Project');
    }
    public function purpose(){
        return $this->belongsTo('App\Purpose');
    }
    public function photos(){
        return $this->hasMany('App\Photo');
    }
    public function features(){
        return $this->belongsToMany('App\Feature')->withPivot('feature_value');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function featuredImage(){
        return $this->hasOne('App\Photo');
    }

    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
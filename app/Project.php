<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function image()
    {
        return $this->morphMany('App\Photo', 'imageable');
    }

    public function photos(){
         return $this->hasMany('App\Photo');
    }

    public function features(){
        return $this->belongsToMany('App\Feature')->withPivot('feature_value');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function featuredImage(){
        return $this->hasOne('App\Photo');
    }

    public function city(){
        return $this->belongsTo('App\City');
    }

    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}

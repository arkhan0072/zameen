<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Country;
use App\FeatureCategory;
use App\Photo;
use App\Project;
use App\Purpose;
use App\State;
use App\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects=Project::all();
        return view('admin.project.index',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user= Auth::user();
        $categories=Category::where('parent',0)->get();
        $purposes=Purpose::all();
        $states= State::all();
        $countries=Country::all();
        $feature_categories= FeatureCategory::all();
        return view('admin.project.create',compact('user','categories','purposes','states','feature_categories','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'title'=>'required|max:64',
            'country'=>'required',
            'city'=>'required',
            'address'=>'required',
            'zipcode'=>'required',
            'description'=>'required',
            'price'=>'required',
            'area'=>'required',
            'areaType'=>'required',
            'picture'=>'required',
            'feature_category'=>'required',
            'feature'=>'required',
            'feature_value'=>'required',
        ]);

        $user = Auth::user();
        $project = new Project();

        $project->title=$request->title;
        $project->slug;
        $project->user_id=$user->id;
        $project->country_id=$request->country;
        $project->description=$request->description;
        $project->price=$request->price;
        $project->city_id=$request->city;
        $project->address=$request->address;
        $project->area=$request->area;
        $project->state=$request->state;
        $project->zipcode=$request->zipcode;
        $project->area_type=$request->areaType;
        $project->save();

        $allFeatures = [];
        if ($request->has('feature_value')) {
            foreach ($request->feature_value as $key => $feature) {
                $allFeatures [$key] = [
                    "feature_value" => $feature
                ];
            }
        }

        $project->features()->syncWithoutDetaching($allFeatures);

        if ($request->hasFile('picture')) {
            foreach ($request->file('picture') as $file) {
                $file_extension = $file->getClientOriginalExtension();
                $filename = $project->id . '_' . str_random(8) . '.' . $file_extension;
                Storage::disk('project_images')->put($filename, File::get($file));
                $image = new Photo();
                $image->project_id = $project->id;
                $image->name = $filename;
                $image->save();
            }
        }

        $address= new Address();
        $address->address=$request->address;
        $address->city_id=$request->city;
        $address->zipcode=$request->zipcode;
        $address->country_id=$request->country;
        $address->project_id=$project->id;
        $address->save();

        return redirect(route('project.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $user=Auth::user();
        $project->load('city.state');
        $cities=City::where('state_id',$project->city->state_id)->get();
        $categories=Category::where('parent',0)->get();
        $purposes=Purpose::all();
        $countries=Country::all();
        $feature_categories= FeatureCategory::all();
        $SubCat=Category::where('id', $project->category_id)->get();
        $states=State::all();
//        return $project->photos;
        return view('admin.project.edit',compact('project','user','states','cities','categories',
            'purposes','countries','feature_categories','SubCat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $request->validate([
            'title'=>'required|max:64',
            'country'=>'required',
            'city'=>'required',
            'address'=>'required',
            'zipcode'=>'required',
            'description'=>'required',
            'price'=>'required',
            'area'=>'required',
            'areaType'=>'required',
            'picture_project'=>'required',
            'feature_category'=>'required',
            'feature'=>'required',
            'feature_value'=>'required',
        ]);

        $user = Auth::user();
        $project->title=$request->title;
        $project->slug;
        $project->user_id=$user->id;
        $project->country_id=$request->country;
        $project->description=$request->description;
        $project->price=$request->price;
        $project->city_id=$request->city;
        $project->address=$request->address;
        $project->area=$request->area;
        $project->state=$request->state;
        $project->zipcode=$request->zipcode;
        $project->area_type=$request->areaType;
        $project->save();

        $allFeatures = [];
        if ($request->has('feature_value')) {
            foreach ($request->feature_value as $key => $feature) {
                $allFeatures [$key] = [
                    "feature_value" => $feature
                ];
            }
        }

        $project->features()->syncWithoutDetaching($allFeatures);

        if ($request->hasFile('picture_project')) {
            foreach ($request->file('picture_project') as $file) {
                $file_extension = $file->getClientOriginalExtension();
                $filename = $project->id . '_' . str_random(8) . '.' . $file_extension;
                Storage::disk('project_images')->put($filename, File::get($file));
                $image = new Photo();
                $image->project_id = $project->id;
                $image->name = $filename;
                $image->save();
            }
        }

        $address= new Address();
        $address->address=$request->address;
        $address->city_id=$request->city;
        $address->zipcode=$request->zipcode;
        $address->country_id=$request->country;
        $address->project_id=$project->id;
        $address->save();

        return redirect(route('project.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect()->back();
    }
    public function subCity(Request $request){
        $Cities = City::where('state_id',$request->id)->get();
        $data = view('admin.project.stateCities', compact('Cities'))->render();
        return response()->json($data);
    }


}

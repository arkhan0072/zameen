<?php

namespace App\Http\Controllers;

use App\FeatureCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
class FeatureCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.featuresCategory.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.featuresCategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:64'
        ]);

        $features = new FeatureCategory();
        $features->name = request('name');
        $features->slug;
        $features->save();
        return redirect()->route('featuresCategory.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FeatureCategory  $featureCategory
     * @return \Illuminate\Http\Response
     */
    public function show(FeatureCategory $featureCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FeatureCategory  $featureCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(FeatureCategory $featuresCategory)
    {
        return view('admin.featuresCategory.edit', compact('featuresCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FeatureCategory  $featureCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeatureCategory $featuresCategory)
    {
        $request->validate([
            'name' => 'required|max:64',
        ]);
        $featuresCategory->name = $request->name;
        $featuresCategory->slug;
        $featuresCategory->save();
        return redirect()->route('featuresCategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FeatureCategory  $featureCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(FeatureCategory $featureCategory)
    {

        $featureCategory->delete();
        return redirect()->back();
    }
    public function featuresCategoryList(){
        $features = FeatureCategory::where('id','>',0);
//return response()->json($features);
        return DataTables::Eloquent($features)
            ->addColumn('action', function($featuresCategory){
                $str = '<a href="'.route('featuresCategory.edit', $featuresCategory->id).'" class="btn btn-primary feature_edit">Edit</a>';
//                if(!$feature->deleted_at){
                $str.='<a class="btn btn-danger feature_delete" data-id="'.$featuresCategory->id.'">Delete</a>';
//                }else{
//                    $str.='<a class="btn btn-success feature_restore" data-id="'.$feature->id.'">Restore</a>';
//                }
                return $str;
            })->toJson();
    }
}

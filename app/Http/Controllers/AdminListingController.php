<?php

namespace App\Http\Controllers;

use App\Address;
use App\Category;
use App\City;
use App\Country;
use App\Feature;
use App\FeatureCategory;
use App\Listing;
use App\Photo;
use App\Purpose;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Project;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class AdminListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        $listing=Listing::with('photos','features')->paginate('10');
//        return response()->json($listing);
        return view('admin.listing.index',compact('user','listing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user= Auth::user();
        $categories=Category::where('parent',0)->get();
        $purposes=Purpose::all();
        $projects=Project::all();
        $states= State::all();
        $countries=Country::all();
        $feature_categories= FeatureCategory::all();
        return view('admin.listing.create',compact('user','categories','purposes','projects','states','feature_categories','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user= Auth::id();
        $request->validate([
            'title'=>'required|max:64',
            'question1'=>'nullable',
            'question2'=>'nullable',
            'question3'=>'nullable',
            'purpose'=>'required',
            'category'=>'required',
            'project'=>'required',
            'description'=>'required',
            'price'=>'required',
            'picture'=>'required',
            'area'=>'required',
            'areaType'=>'required',
            'feature'=>'required',
            'no_of_rooms'=>'required',
            //address
            'address'=>'required',
            'city'=>'required',
            'zipcode'=>'required',
            'country'=>'required',
        ]);
        $list = new Listing();

        $list->title=$request->title;
        $list->slug;
        $list->purpose_id=$request->purpose;
        $list->is_installment=($request->question1 or 0);
        $list->is_insurance=($request->question2 or 0) ;
        $list->is_labilities=($request->question3 or 0);
        $list->user_id=$user;
        $list->category_id=$request->subcategory;
        $list->project_id=$request->project;
        $list->description=$request->description;
        $list->price=$request->price;
        $list->area=$request->area;
        $list->area_type=$request->areaType;
        $list->no_of_rooms=$request->no_of_rooms;
        $list->save();

        $allFeatures = [];
        if ($request->has('feature_value')) {
            foreach ($request->feature_value as $key => $feature) {
                $allFeatures [$key] = [
                    "feature_value" => $feature
                ];
            }
        }

        $list->features()->syncWithoutDetaching($allFeatures);
        if ($request->hasFile('picture')) {
            foreach ($request->file('picture') as $file) {
                $file_extension = $file->getClientOriginalExtension();
                $file_path = $file->getFilename();
                $filename = $user . $list->id . '_' . str_random(8) . '.' . $file_extension;
//                $file->store('uploads/ads');
                Storage::disk('listing_images')->put($filename, File::get($file));
                $image = new Photo();
                $image->listing_id = $list->id;
                $image->name = $filename;
                $image->save();
            }
        }
        $address= new Address();
        $address->address=$request->address;
        $address->listing_id=$list->id;
        $address->city_id=$request->city;
        $address->zipcode=$request->zipcode;
        $address->country_id=$request->country;
        $address->save();


        return redirect(route('admin.listing.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function show(Listing $listing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function edit(Listing $listing,$list)
    {

        $user= Auth::user();
        $listing=Listing::where('id',$list)->first();
        $categories=Category::where('parent',0)->get();
        $purposes=Purpose::all();
        $states=State::all();
        $cities=City::where('state_id',$listing->project->city->state_id)->get();
        $SubCat=Category::where('id',$listing->category_id)->get();
        $projects=Project::where('id',$listing->project_id)->get();
        $feature_categories= FeatureCategory::all();

        $listing->load('photos','features.category');
        return view('admin.listing.edit', compact('listing','categories','purposes','states','SubCat','cities','projects','user','feature_categories','feature_categoriesS'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Listing $listing,$list)
    {
        $user= Auth::id();
//        return $listing->feature_id;
        $request->validate([
            'title'=>'required|max:64',
            'purpose'=>'required',
            'category'=>'required',
            'project'=>'required',
            'description'=>'required',
            'price'=>'required',
            'picture'=>'sometimes',
            'area'=>'required',
            'areaType'=>'required',
            'feature'=>'required',
        ]);
        $listing->title=$request->title;
        $listing->slug;
        $listing->purpose_id=$request->purpose;
        $listing->user_id=$user;
        $listing->category_id=$request->subcategory;
        $listing->project_id=$request->project;
        $listing->description=$request->description;
        $listing->price=$request->price;
        $listing->area=$request->area;
        $listing->area_type=$request->areaType;
        $listing->feature_id=$request->feature;
        $listing->save();
        $allFeatures = [];
        if ($request->has('feature_value')) {
            foreach ($request->feature_value as $key => $feature) {
                $allFeatures [$key] = [
                    "feature_value" => $feature
                ];
            }
        }

        $listing->features()->sync($allFeatures);
        if ($request->hasFile('picture')) {
            foreach ($request->file('picture') as $file) {
                $file_extension = $file->getClientOriginalExtension();
                $file_path = $file->getFilename();
                $filename = $user . $listing->id . '_' . str_random(8) . '.' . $file_extension;
//                $file->store('uploads/ads');
                Storage::disk('listing_images')->put($filename, File::get($file));
                $image = new Photo();
                $image->listing_id = $listing->id;
                $image->name = $filename;
                $image->save();
            }
        }
        return redirect(route('listing.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Listing $listing)
    {
        $listing->delete();
        return redirect()->back();
    }
    public function subCat(Request $request){
        $SubCat = Category::where('parent',$request->id)->get();
        $data = view('admin.category.subcategory', compact('SubCat'))->render();
        return response()->json($data);
    }
    public function cityProject(Request $request){
        $projectsC = Project::where('city_id',$request->id)->get();
        $data = view('admin.project.cityprojects', compact('projectsC'))->render();
        return response()->json($data);
    }
    public function features_category(Request $request)
    {
        $features = Feature::where('category_id', $request->id)->select('id', 'name')->get();
        $options = "";
        foreach ($features as $feature)
            $options .= "<option value='$feature->id'>$feature->name</option>";
        return $options;
    }
}

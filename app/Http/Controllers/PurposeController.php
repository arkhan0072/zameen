<?php

namespace App\Http\Controllers;

use App\Purpose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function Sodium\randombytes_uniform;

class PurposeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::User();
        $purposes=Purpose::all();
        return view('admin.purpose.index',compact('user','purposes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user= Auth::User();
        return view('admin.purpose.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'max:64|required|unique:purposes,name',
        ]);
        $purpose = new Purpose();
        $purpose->name = $request->name;
        $purpose->slug ;
        $purpose->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purpose  $purpose
     * @return \Illuminate\Http\Response
     */
    public function show(Purpose $purpose)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purpose  $purpose
     * @return \Illuminate\Http\Response
     */
    public function edit(Purpose $purpose)
    {
        $user=Auth::User();
        return view('admin.purpose.edit',compact('user','purpose'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purpose  $purpose
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purpose $purpose)
    {
        $request->validate([
            'name' => 'max:64|required',
        ]);
        $purpose->name = $request->name;
        $purpose->slug;
        $purpose->save();
        return redirect(route('purpose.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purpose  $purpose
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purpose $purpose)
    {
        $purpose->delete();
        return redirect(route('admin.purpose.index'))->with(['deleted'=>'State successfully updated.']);
    }
}

<?php

namespace App\Http\Controllers;


use App\Listing;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UsersAccountController extends Controller
{
    function myProfile(){
        $user=Auth::user();
        $user->load('agency');
        return view('users.my_profile',compact('user'));
    }

    function updateMyProfile(Request $request, $user){

        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'max:25'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'about_me' => ['required'],
            'address' => ['required'],
            'city_state' => ['required'],
            'zipcode' => ['required'],
            'dob' => ['required'],
            'skype_id' => ['required'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'user_image' => ['sometimes'],
            'a_email' => ['nullable'],
            'a_phone' => ['nullable'],
            'about_agency' => ['nullable'],
        ]);

        $user = User::where('id', $user)->first();

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->mobile = $request->phone;
        $user->email = $request->email;
        $user->about = $request->about_me;
        $user->address = $request->address;
        $user->city_state = $request->city_state;
        $user->zipcode = $request->zipcode;
        $user->dob = $request->dob;
        $user->skype_id = $request->skype_id;

        if ($request->filled('password'))
            $user->password = bcrypt($request->password);


        if ($request->hasFile('user_image')) {
            $file_extension = ($request->user_image)->getClientOriginalExtension();
            $file_path = ($request->user_image)->getFilename();
            $filename = $user->id . '_' . str_random(8) . '.' . $file_extension;
            Storage::disk('user_images')->put($filename, File::get($request->user_image));
            $user->profile_picture = $filename;
        }

        $user->save();

        $agency = DB::table('agencies')
            ->where('id',$request->agency)
            ->update(
            ['agency_email'=> $request->a_email,
                   'number'=> $request->a_phone,
                   'description'=>$request->about_agency
            ]
        );
        return redirect()->back();
    }

    function socialMedia()
    {

        return view('users.social_media');
    }

    function updateSocialMedia(Request $request){
        $request->validate([
            'facebook' => ['required', 'string', 'max:255'],
            'twitter' => ['required', 'string', 'max:255'],
            'linked_in' => ['required', 'string', 'max:255'],
            'google_plus' => ['required', 'string', 'max:255'],
            'vimo' => ['required', 'string', 'max:255'],
        ]);

        $slinks = DB::table('users')
            ->where('id', Auth::id())
            ->update(
                [
                    'facebook'=> $request->facebook,
                    'twitter'=> $request->twitter,
                    'linked_in'=> $request->linked_in,
                    'google_plus'=> $request->google_plus,
                    'vimo' => $request->vimo
                ]
        );
        return redirect()->back();

    }

    function myProperties(){
        $user=Auth::user();
        $mylistings=Listing::with('category','project','photos')->where('user_id',$user->id)->paginate(5);
        return view('users.properties.my_properties',compact('user','mylistings'));
    }

    function favoritedProperties(){
        return view('users.favorited_properties');
    }

    function Message(){
        return view('users.message');
    }

    function feedbackAndComments(){
        return view('users.feedback_and_comments');
    }

    function paymentsAndInvoice(){
        return view('users.payments_and_invoice');
    }

    function changePassword(){
        return view('users.change_password');
    }

    function updatePassword(Request $request, $user)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        DB::table('users')
            ->where('id', Auth::id())
            ->update(
                ['password'=>  bcrypt($request->password)]);
        return redirect()->route('user.profile');
    }

}

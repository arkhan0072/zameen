<?php
namespace App\Http\Controllers;
use App\City;
use App\Project;
use Illuminate\Http\Request;
use Auth;
use App\State;
class cityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::User();
        $cities=City::with('state')->get();
        return view('admin.city.index',compact('user','cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user= Auth::User();
        $states = State::all();
        return view('admin.city.create', compact('states','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'max:64|required|unique:cities,name',
            'state' => 'required',
        ]);
        $city = new City;
        $city->name = $request->name;
        $city->slug;
        $city->state_id = $request->state;
        $city->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        $user=Auth::User();
        $cities=Project::with('city.state')->where('city_id',$city->id)->get();
        return view('admin.city.show',compact('cities','user','city','state'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        $user=Auth::User();
        $states=State::all();
        return view('admin.city.edit',compact('user','city','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $request->validate([
            'name' => 'max:64|required|unique:cities,name',$city->id,
            'state' => 'required',
        ]);
        $city->name = $request->name;
        $city->slug;
        $city->state_id=$request->state;
        $city->save();
        return redirect(route('city.index'))->with(['update'=>'City successfully updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();
        return redirect(route('city.index'))->with(['deleted'=>'City successfully Deleted.']);
    }

    public function ajaxCities(Request $request){
        $cities = City::where('state_id', $request->id)->get();
        $str = '';
        foreach ($cities as $city) {
            $str.="<option value='$city->id'>$city->name</option>";
        }
        return $str;
    }
}

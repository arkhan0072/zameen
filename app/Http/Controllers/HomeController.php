<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Country;
use App\Listing;
use App\Photo;
use App\Purpose;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listings = Listing::all();

        $purposes=Purpose::all();
//        return $purposes;
        $categories=Category::where('parent',0)->get();
        $listing=Listing::with('category','project.city.state','photos')->get();
        $countires=Country::all();
//        return $listing;
        return view('indexlayout.index',compact('purposes','categories','listing','countires'));
    }
    public function single(Request $request)
    {
        $listing=Listing::with('category','project','purpose','photos','features')->where('slug',$request->slug)->first();
        $photo=Photo::where('listing_id',$request->id)->get();
//                return response()->json($listing);
//                return response()->json($photo);
        return view('indexlayout.singleview',compact('listing','photo'));
    }
    public function filter(Request $request){
        $listing=Listing::with('category')->where('category_id',$request->id)->get();
        $data = view('indexlayout.partial_filter', compact('listing'))->render();
        return response()->json($data);
    }
    public function propertysubmit(){
        return view('indexlayout.submitproperty');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Request $request){
        $purposes = Purpose::all();
        $categories = Category::where('parent', 0)->get();
        $sub_categories = Category::where('parent', '>', 0)->get();
        $states = State::all();
        $cities = City::all();
        $listing = Listing::with('purpose','featuredImage','project','category','address')->get();

        $max_price = Listing::selectRaw('max(price) as price')->get();

        $max_area = DB::table('listings')
            ->select(DB::raw('max(area) as area'))
            ->get();
//       return $max_area;
        return view('indexlayout.view',compact('listing','max_price','purposes','sub_categories','states','cities','categories','max_area'));
    }

    public function search(Request $request){
        $purposes = Purpose::all();
        $categories = Category::where('parent', 0)->get();
        $sub_categories = Category::where('parent', '>', 0)->get();
        $states = State::all();
        $cities = City::all();
        $max_price = Listing::selectRaw('max(price) as price')->get();
        $max_area = DB::table('listings')
            ->select(DB::raw('max(area) as area'))
            ->get();
        $listing=Listing::with('category','project','purpose','photos','features','address')
            ->when($request->filled('title'), function ($q) use ($request) {
                $q->where('title', 'like', "%$request->title%");
            })
            ->when($request->filled('subcategory') && $request->filled('category'),
                function ($q) use ($request) {
                    $q->where('category_id', $request->subcategory);
                })
            ->when($request->filled('category') && !$request->filled('subcategory'),
                function ($q) use ($request) {
                    $q->whereHas('category', function ($p) use ($request){
                        $p->where('parent', $request->category);
                    });
                })
            ->when($request->filled('purpose') && strtolower($request->purpose) != 'all', function ($q) use ($request) {
                $q->where('purpose_id', $request->purpose);
            })
            ->paginate(5);
        return view('indexlayout.view',compact('listing','max_price','max_area','purposes','categories','sub_categories','states','cities'));
//        return $listings;
    }
    public function subCat(Request $request){
        $SubCat = Category::where('parent',$request->id)->get();
        $data = view('admin.category.subcategory', compact('SubCat'))->render();
        return response()->json($data);
    }

    function advancedSearch(Request $request){
        $purposes = Purpose::all();
        $categories = Category::where('parent', 0)->get();
        $sub_categories = Category::where('parent', '>', 0)->get();
        $states = State::all();
        $cities = City::all();
        $max_price = Listing::selectRaw('max(price) as price')->get();
        $max_area = DB::table('listings')
            ->select(DB::raw('max(area) as area'))
            ->get();
        $listing = Listing::with('purpose','featuredImage','project','category','address')
            ->when($request->filled('price'), function ($q) use ($request) {
                $q->whereBetween('price', explode(';',$request->price));
            })
            ->when($request->filled('area'), function ($p) use ($request) {
                $p->whereBetween('area',explode(';',$request->area));
            })
            ->when($request->purpose, function ($q) use ($request) {
                $q->where('purpose_id',  $request->purpose);
            })
            ->when($request->filled('subcategory') && $request->filled('category'),
                function ($q) use ($request) {
                    $q->where('category_id', $request->subcategory);
                })
            ->when($request->filled('category') && !$request->filled('subcategory'),
                function ($q) use ($request) {
                    $q->whereHas('category', function ($p) use ($request){
                        $p->where('parent', $request->category);
                    });
                })
            ->when($request->filled('state'),
                function ($q) use ($request) {
                    $q->whereHas('project', function ($p) use ($request){
                        $p->whereHas('city', function ($e) use ($request){
                            $e->where('state_id',$request->state);
                        });
                    });
                })
              ->when($request->filled('city'),function ($q) use ($request) {
                    $q->whereHas('project', function ($p) use ($request){
                     $p->where('city_id', $request->city);
                 });
              })
            ->get();
        return view('indexlayout.view',compact('listing','max_price','max_area','purposes','categories','sub_categories','states','cities'));
//        return $filteredlistings;
    }

    function projects(){

    }

}

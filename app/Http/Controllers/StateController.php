<?php

namespace App\Http\Controllers;
use App\City;
use Auth;
use App\State;
use Illuminate\Http\Request;

class stateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::User();
        $states=State::all();
        return view('admin.state.index',compact('user','states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user= Auth::User();
        return view('admin.state.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'max:64|required|unique:states,name',
        ]);
        $state = new State;
        $state->name = $request->name;
        $state->slug;
        $state->save();



        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        $user=Auth::User();
       $cities = City::where('state_id',$state->id)->simplePaginate('10');

       return view('admin.state.show',compact('cities','user','state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit(State $state)
    {
        $user=Auth::User();

        return view('admin.state.edit',compact('user','state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, State $state)
    {
        $request->validate([
            'name' => 'max:64|required'
        ]);
        $state->name = $request->name;
        $state->slug;
        $state->save();
        return redirect(route('states.index'))->with(['update'=>'State successfully updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
        $state->delete();
        return redirect(route('states.index'))->with(['deleted'=>'State successfully updated.']);
    }
}

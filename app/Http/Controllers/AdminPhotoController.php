<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AdminPhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::User();
        return view('admin.listing.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        return view('admin.listing.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo,$list)
    {

        $user= Auth::id();
        $request->validate([
            'picture'=>'sometimes',
        ]);

        if ($request->hasFile('picture')) {
            $file_extension = ($request->picture)->getClientOriginalExtension();
            $file_path = ($request->picture)->getFilename();
            $filename = $user . $list . '_' . str_random(8) . '.' . $file_extension;
//                $file->store('uploads/ads');
            Storage::disk('listing_images')->put($filename, File::get($request->picture));
            $newpicture = DB::table('photos')
                ->where('id', $list)
                ->update(
                    [
                        'name' => $filename
                    ]
                );
        }
        return redirect()->back();


    }

    public function updateProjectPhoto(Request $request, $project){
        $user= Auth::id();
        $request->validate([
            'picture_project'=>'sometimes',
        ]);
        if ($request->hasFile('picture_project')) {
            $file_extension = ($request->picture_project)->getClientOriginalExtension();
            $file_path = ($request->picture_project)->getFilename();
            $filename = $user . $project . '_' . str_random(8) . '.' . $file_extension;
//                $file->store('uploads/ads');
            Storage::disk('project_images')->put($filename, File::get($request->picture_project));
            $newpicture = DB::table('photos')
                ->where('id', $project)
                ->update(
                    [
                        'name' => $filename
                    ]
                );
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        $photo->delete();
        return redirect()->back();
    }
    public function destroyProjectPhoto($project)
    {
//        return $project;
        $nrd = DB::delete('delete from photos where id='.$project);
//        $project->delete();
        return redirect()->back();
    }
}

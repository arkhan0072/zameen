<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Auth;
use Entrust;
use App\User;
class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $user = Auth::User();
        $category=Category::all();
        if (!$user->hasRole('admin'))
            return redirect()->route('admin.index')->with('baby', 'Baby Hold On! you are not an admin.');
        return view('admin.index', compact('user','category'));
    }



}

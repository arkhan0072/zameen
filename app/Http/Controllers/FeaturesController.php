<?php

namespace App\Http\Controllers;

use App\Category;
use App\Feature;
use App\FeatureCategory;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;

class FeaturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.features.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = FeatureCategory::all();
        return view('admin.features.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'name' => 'required|max:64'
        ]);

        $features = new Feature();
        $features->category_id = request('category');
        $features->name = request('name');
        $features->save();
        return redirect()->route('features.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Feature $feature)
    {
        //return $feature;
        $categories=FeatureCategory::all();
        return view('admin.features.edit', compact('feature','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeatureCategory $feature)
    {
        $request->validate([
            'name' => 'required|max:64',
            'category' => 'required|max:64'
        ]);

        $feature->category_id = $request->category;
        $feature->name = $request->name;
        $feature->save();
        return redirect()->route('admin.features.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feature $feature)
    {
        $feature->delete();
        return redirect()->back();
    }
    public function restore($feature){
        Feature::onlyTrashed()->where('id', $feature)->restore();
        return redirect()->back();
    }
    public function featuresList(){
        $features = Feature::with('category');
//return response()->json($features);
        return DataTables::Eloquent($features)
            ->addColumn('action', function($feature){
                $str = '<a href="'.route('features.edit', $feature->id).'" class="btn btn-primary feature_edit">Edit</a>';
//                if(!$feature->deleted_at){
                    $str.='<a class="btn btn-danger feature_delete" data-id="'.$feature->id.'">Delete</a>';
//                }else{
//                    $str.='<a class="btn btn-success feature_restore" data-id="'.$feature->id.'">Restore</a>';
//                }
                return $str;
            })->toJson();
    }
}

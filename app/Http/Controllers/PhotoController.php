<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::User();
        return view('users.submitProperty',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        return view('users.updateProperty');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        $user= Auth::id();
        $request->validate([
            'picture'=>'sometimes',
        ]);
        $photo->listing_id=$photo->listing_id;
        if ($request->hasFile('picture')) {
            $file_extension = ($request->picture)->getClientOriginalExtension();
            $file_path = ($request->picture)->getFilename();
            $filename = $user . $photo->listing_id . '_' . str_random(8) . '.' . $file_extension;
//                $file->store('uploads/ads');
            Storage::disk('listing_images')->put($filename, File::get($request->picture));
            $photo->listing_id=$photo->listing_id;
            $photo->name = $filename;
            $photo->save();
        }
        return redirect()->back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
       $photo->delete();
       return redirect()->back();
    }
}

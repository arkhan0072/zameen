<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public function imageable()
    {
        return $this->morphTo();
    }

    public function listing(){
        return $this->belongsTo('App\Listing');
    }

    public function project(){
        return $this->belongsTo('App\Project');
    }
}

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from unicoderbd.com/theme/html/uniland/index_4.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 10 Dec 2018 08:43:04 GMT -->
<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Uniland - Real Estate HTML5 Template">
    <meta name="keywords" content="real estate, property, property search, agent, apartments, booking, business, idx, housing, real estate agency, rental">
    <meta name="author" content="unicoder">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Uniland - Real Estate HTML5 Template</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo e(asset('uniland/img/favicon.ico')); ?>">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo e(asset('uniland/css/bootstrap.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('uniland/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('uniland/css/font-awesome.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('uniland/fonts/flaticon.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('uniland/css/color.css')); ?>" id="color-change">
    <link rel="stylesheet" href="<?php echo e(asset('uniland/css/jslider.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('uniland/css/responsive.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('uniland/css/loader.css')); ?>">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="pagewrap single-property-page">

<!-- Page Loader -->
<div class="loading-page">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!-- End Loader -->
<!-- Color Settings -->
<div class="color-panel">
    <div class="on-panel"><img src="<?php echo e(asset('uniland/img/icons/settings.png')); ?>" alt=""></div>
    <div class="panel-box">
        <span class="panel-title">Change Colors</span>
        <ul class="color-box">
            <li class="green" data-path="<?php echo e(asset('uniland/css/colors/green.css')); ?>" data-image="<?php echo e(asset('uniland/img/logo1.png')); ?>" data-target="<?php echo e(asset('uniland/img/logo2.png')); ?>"></li>
            <li class="blue" data-path="<?php echo e(asset('uniland/css/colors/blue.css')); ?>" data-image="<?php echo e(asset('uniland/img/logo1_blue.png')); ?>" data-target="<?php echo e(asset('uniland/img/logo2_blue.png')); ?>"></li>
            <li class="red" data-path="<?php echo e(asset('uniland/css/colors/red.css')); ?>" data-image="<?php echo e(asset('uniland/img/logo1_red.png')); ?>" data-target="<?php echo e(asset('uniland/img/logo2_red.png')); ?>"></li>
            <li class="purple" data-path="<?php echo e(asset('uniland/css/colors/purple.css')); ?>" data-image="<?php echo e(asset('uniland/img/logo1_purple.png')); ?>" data-target="<?php echo e(asset('uniland/img/logo2_purple.png')); ?>"></li>
            <li class="yellow" data-path="<?php echo e(asset('uniland/css/colors/yellow.css')); ?>" data-image="<?php echo e(asset('uniland/img/logo1_yellow.png')); ?>" data-target="<?php echo e(asset('uniland/img/logo2_yellow.png')); ?>"></li>
            <li class="orange" data-path="<?php echo e(asset('uniland/css/colors/orange.css')); ?>" data-image="<?php echo e(asset('uniland/img/logo1_orange.png')); ?>" data-target="<?php echo e(asset('uniland/img/logo2_orange.png')); ?>"></li>
            <li class="magento" data-path="<?php echo e(asset('uniland/css/colors/magento.css')); ?>" data-image="<?php echo e(asset('uniland/img/logo1_magento.png')); ?>" data-target="<?php echo e(asset('uniland/img/logo2_magento.png')); ?>"></li>
            <li class="turquoise" data-path="<?php echo e(asset('uniland/css/colors/turquoise.css')); ?>" data-image="<?php echo e(asset('uniland/img/logo1_turquoise.png')); ?>" data-target="<?php echo e(asset('uniland/img/logo2_turquoise.png')); ?>"></li>
            <li class="lemon" data-path="<?php echo e(asset('uniland/css/colors/lemon.css')); ?>" data-image="<?php echo e(asset('uniland/img/logo1_lemon.png')); ?>" data-target="<?php echo e(asset('uniland/img/logo2_lemon.png')); ?>"></li>
        </ul>
    </div>
</div>
<!-- End Color Settings -->
<?php echo $__env->make('indexlayout.include.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- Slider Part Start -->
<?php echo $__env->yieldContent('content'); ?>
<!-- Register Section End -->

<!-- Footer Section Start -->
<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="footer_widget">
                    <div class="footer-logo"><a href="index_1.html"><img class="logo-bottom" src="img/logo2.png" alt=""></a></div>
                    <div class="footer_contact">
                        <p>Netus ut pede mus vestibulum montes. Mus. Pretium. Mattis habitant netus ligula ridiculus a nam bibendum fusce litora. Ac ullamcorper blandit, viverra pellentesque scelerisque. Phasellus aptent sociosqu nec posuere.</p>
                    </div>
                    <div class="socail_area">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="footer_widget">
                    <div class="footer-title">
                        <h4>Get In Touch</h4>
                    </div>
                    <div class="footer_contact">
                        <ul>
                            <li> <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <div class="ftr-list">
                                    <h6 class="touch-title">Office Address</h6>
                                    <span>1707 Orlando Central pkwy ste 100 Orlando FL, USA</span>
                                </div>
                            </li>
                            <li> <i class="fa fa-phone" aria-hidden="true"></i>
                                <div class="ftr-list">
                                    <h6 class="touch-title">Call Us 24/7</h6>
                                    <span>(+241) 542 34251, (+241) 234 88232</span>
                                </div>
                            </li>
                            <li> <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <div class="ftr-list">
                                    <h6 class="touch-title">Email Address</h6>
                                    <span>info@webmail.com</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="footer_widget">
                    <div class="footer-title">
                        <h4>Quick Links</h4>
                    </div>
                    <div class="footer_contact">
                        <ul>
                            <li><a href="faq.html">Freequinly Ask Question</a></li>
                            <li><a href="about.html">About Our Company</a></li>
                            <li><a href="our_service.html">Our Professional Services</a></li>
                            <li><a href="terms_and_condition.html">Terms and Conditions</a></li>
                            <li><a href="submit_property.html">Submit Your Property</a></li>
                            <li><a href="#">Become A Member</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="footer_area">
                    <div class="footer-title">
                        <h4>Newslatter</h4>
                    </div>
                    <div class="footer_contact">
                        <p>Subscribe to our newsletter and we will inform your about newset projects.</p>
                        <div class="news_letter">
                            <form action="#" method="post">
                                <input type="email" name="email" placeholder="Enter Your Email" class="news_email">
                                <button type="submit" name="submit" class="btn btn-default">subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Footer Section End -->

<!-- Bottom Footer Start -->
<div id="bottom_footer">
    <div class="reserve_text"> <span>Copyright &copy; 2017 Uniland All Right Reserve</span> </div>
</div>
<!-- Bottom Footer End -->

<!-- Scroll to top -->
<div class="scroll-to-top">
    <a href="#" class="scroll-btn" data-target=".pagewrap"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
</div>
<!-- End Scroll To top -->


<!-- All Javascript Plugin File here -->
<script src="<?php echo e(asset('uniland/js/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/bootstrap-select.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/YouTubePopUp.jquery.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/jquery.fancybox.pack.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/jquery.fancybox-media.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/owl.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/mixitup.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/wow.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/jshashtable-2.1_src.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/jquery.numberformatter-1.2.3.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/tmpl.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/jquery.dependClass-0.1.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/draggable-0.1.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/jquery.slider.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/jquery.barrating.js')); ?>"></script>
<script src="<?php echo e(asset('uniland/js/custom.js')); ?>"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADAvRonq8GcS5dNWMPgDMf17hgiaTHs7E&amp;callback=initMap"></script>
<script>
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        var mapOptions = {
            zoom: 9,
            center: new google.maps.LatLng(28.538335, -81.379236), // New York
            styles: [{"featureType":"water","elementType":"all","stylers":[{"hue":"#76aee3"},{"saturation":38},{"lightness":-11},{"visibility":"on"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"hue":"#8dc749"},{"saturation":-47},{"lightness":-17},{"visibility":"on"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"hue":"#c6e3a4"},{"saturation":17},{"lightness":-2},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"hue":"#cccccc"},{"saturation":-100},{"lightness":13},{"visibility":"on"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"hue":"#5f5855"},{"saturation":6},{"lightness":-31},{"visibility":"on"}]},{"featureType":"road.local","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"all","stylers":[]}]
        };

        var mapElement = document.getElementById('map');

        var map = new google.maps.Map(mapElement, mapOptions);

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(28.538335, -81.379236),
            map: map,
            icon: 'http://unicoderbd.com/theme/html/uniland/img/marker_blue.png',
            title: 'Snazzy!'
        });
    }

</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-106986305-1', 'auto');
    ga('send', 'pageview');
</script>
<?php echo $__env->yieldContent('page_level_script'); ?>
</body>

<!-- Mirrored from unicoderbd.com/theme/html/uniland/index_4.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 10 Dec 2018 08:44:24 GMT -->
</html>
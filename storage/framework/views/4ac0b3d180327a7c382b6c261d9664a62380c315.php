<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://crypto-admin-templates.multipurposethemes.com/images/favicon.ico">

    <title>Crypto Admin - Dashboard</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="<?php echo e(asset('crypto/assets/vendor_components/bootstrap/dist/css/bootstrap.css')); ?>">

    <!--amcharts -->
    <link href="../../www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap-extend -->
    <link rel="stylesheet" href="<?php echo e(asset('crypto/css/bootstrap-extend.css')); ?>">

    <!-- theme style -->
    <link rel="stylesheet" href="<?php echo e(asset('crypto/css/master_style.css')); ?>">

    <!-- Crypto_Admin skins -->
    <link rel="stylesheet" href="<?php echo e(asset('crypto/css/skins/_all-skins.css')); ?>">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<!-- Mirrored from crypto-admin-templates.multipurposethemes.com/src2/pages/samplepage/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Aug 2018 10:45:45 GMT -->
<body class="hold-transition login-page">
<div class="container">
    <div class="row justify-content-center">
<div class="login-box" style="margin-top: 50px!important;margin-bottom: 80px!important; overflow-y:hidden!important; height: 50%!important;">
    <div class="login-logo">
        <a href="../../index.html"><b>Crypto</b>Admin</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <form method="POST" action="<?php echo e(route('login')); ?>">
            <?php echo csrf_field(); ?>

            <div class="form-group<?php echo e($errors->has('email') ? 'has-error': ''); ?> has-feedback">
                <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" required autofocus>
                <span class="ion ion-email form-control-feedback"></span>
            </div>
            <?php if($errors->has('email')): ?>
                <span class="help-block">
          <strong><?php echo e($errors->first('email')); ?></strong>
        </span>
            <?php endif; ?>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <span class="ion ion-locked form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="checkbox">
                        
                        
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>

                        <label class="form-check-label" for="remember">
                            <?php echo e(__('Remember Me')); ?>

                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-6">
                    <div class="fog-pwd">
                        <?php if(Route::has('password.request')): ?>
                            <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>">
                                <i class="ion ion-locked"></i>  <?php echo e(__('Forgot Your Password?')); ?>

                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-info btn-block margin-top-10">
                        <?php echo e(__('Login')); ?>

                    </button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        
            
            
            
        
        

        
            
        

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
    </div>
</div>


<!-- jQuery 3 -->
<script src="<?php echo e(asset('crypto/assets/vendor_components/jquery/dist/jquery.js')); ?>"></script>

<!-- popper -->
<script src="<?php echo e(asset('crypto/assets/vendor_components/popper/dist/popper.min.js')); ?>"></script>

<!-- Bootstrap 4.0-->
<script src="<?php echo e(asset('crypto/assets/vendor_components/bootstrap/dist/js/bootstrap.js')); ?>"></script>

<!-- Slimscroll -->
<script src="<?php echo e(asset('crypto/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js')); ?>"></script>

<!-- FastClick -->
<script src="<?php echo e(asset('crypto/assets/vendor_components/fastclick/lib/fastclick.js')); ?>"></script>











<!-- webticker -->
<script src="<?php echo e(asset('crypto/assets/vendor_components/Web-Ticker-master/jquery.webticker.min.js')); ?>"></script>

<!-- EChartJS JavaScript -->
<script src="<?php echo e(asset('crypto/assets/vendor_components/echarts-master/dist/echarts-en.min.js')); ?>"></script>
<script src="<?php echo e(asset('crypto/assets/vendor_components/echarts-liquidfill-master/dist/echarts-liquidfill.min.js')); ?>"></script>

<!-- This is data table -->
<script src="<?php echo e(asset('crypto/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js')); ?>"></script>

<!-- Sparkline -->
<script src="<?php echo e(asset('crypto/assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.min.js')); ?>"></script>

<!-- Crypto_Admin App -->
<script src="<?php echo e(asset('crypto/js/template.js')); ?>"></script>

<!-- Crypto_Admin dashboard demo (This is only for demo purposes) -->
<script src="<?php echo e(asset('crypto/js/pages/dashboard.js')); ?>"></script>
<!--<script src="js/pages/dashboard-chart.js"></script>-->

<!-- Crypto_Admin for demo purposes -->
<script src="<?php echo e(asset('crypto/js/demo.js')); ?>"></script>

</body>

<!-- Mirrored from crypto-admin-templates.multipurposethemes.com/src2/pages/samplepage/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Aug 2018 10:45:45 GMT -->
</html>


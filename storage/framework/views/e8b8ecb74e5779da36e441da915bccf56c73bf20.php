<?php $__env->startSection('content'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    
    
    
    
    
    
    
    
    
    

    <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Listings</h3>
                </div>

            </div>
                <div class="grid-container">
                    <?php $__currentLoopData = $listing; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-12 col-xl-12">
                        <div class="box pull-up">
                            <div class="box-body">
                                <div class="row align-center">
                                    <div class="col-3">
                                       <img src="<?php echo e(asset('uploads/listings')); ?>/<?php echo e($list->photos[0]->name); ?>" class="img-thumb">
                                    </div>
                                    <div class="col-6">
                                        <h1><?php echo e($list->title); ?></h1>
                                        <h4><?php echo e($list->project->title); ?>,<?php echo e($list->project->city->name); ?></h4>
                                        <h4><?php echo e($list->price); ?></h4>
                                        <p><?php echo e(strip_tags( str_limit($list->description,'170', ' ...' ))); ?></p>
                                        <p><?php echo e($list->area); ?></p>
                                    </div>
                                    <div class="col-3">
                                        <a href="<?php echo e(route('alisting.edit', $list->id)); ?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        <a onclick="frmdlt<?php echo e($list->id); ?>.submit();" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> Delete</a>
                                    </div>
                                    <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="frmdlt<?php echo e($list->id); ?>" action="<?php echo e(route('alisting.destroy', $list->id)); ?>" method="post">
            <?php echo e(method_field('delete')); ?>

            <?php echo e(csrf_field()); ?>


            </form>
</div>
</div>
</div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php echo e($listing->links()); ?>

</div>


        
                                            
                                            
                                            
                                                
                                                    
                                                    

                                                
                                        


        </section>
                        </div>
                        <!-- /.box-body -->


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
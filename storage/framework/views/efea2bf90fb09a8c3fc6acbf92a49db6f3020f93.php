<?php $__env->startSection('content'); ?>
  <!-- Banner Section Start -->
  <section id="banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="banner_area">
            <h3 class="page_title">My Profile</h3>
            <div class="page_location">
              <a href="index_1.html">Home</a>
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <a href="index_1.html">Pages</a>
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <span>My Profile</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Banner Section End -->

  <!-- Profile Setting Start -->
  <section id="profile_setting">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-12">
          <?php echo $__env->make('users.settings', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <div class="col-md-8 col-sm-12">
          <div class="row">
            <form class="profile_area" method="post" action="<?php echo e(route('user.updateProfile', $user->id)); ?>" enctype="multipart/form-data">
              <?php echo csrf_field(); ?>
              <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div><?php echo e($error); ?></div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <div class="col-md-8 col-sm-12">


                
                <div class="personal_infor">
                  <h4 class="inner-title">personal information</h4>
                  <p>Euismod Ac penatibus magna vel tempor, porttitor ullamcorper urna, massa natoque venenatis mollis libero neque velit risus, magnis vehicula nam.</p>
                  <div class="information_form">
                    <div class="row">
                      <?php if($user->hasRole('agent')): ?>
                        <div class="col-md-12">
                          <label class="profile_label">Agent ID</label>
                          <input type="text" name="agentid" class="form-control" value="<?php echo e(str_limit($user->agency[0]->name, $limit = 1,$end="")); ?><?php echo e($user->agency[0]->id); ?><?php echo e(str_limit($user->first_name, $limit = 1,$end="")); ?><?php echo e($user->id); ?>" disabled >
                        </div>
                      <?php elseif($user->hasRole('agency')): ?>
                        <div class="col-md-12">
                          <label class="profile_label">Agency ID</label>
                          <input type="text" name="agencyid" class="form-control" value="<?php echo e(str_limit($user->agency[0]->name, $limit = 1,$end="")); ?><?php echo e($user->agency[0]->id); ?><?php echo e(str_limit($user->first_name, $limit = 1,$end="")); ?><?php echo e($user->id); ?>" disabled >
                        </div>
                      <?php endif; ?>
                      <div class="col-md-6 <?php echo e($errors->has('first_name') ? ' is-invalid' : ''); ?>">
                        <label class="profile_label">First Name</label>
                        <input type="text" name="first_name" class="form-control" value="<?php echo e($user->first_name); ?>">
                        <?php if($errors->has('first_name')): ?>
                          <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('first_name')); ?></strong>
                                    </span>
                        <?php endif; ?>
                      </div>
                      <div class="col-md-6 <?php echo e($errors->has('last_name') ? ' is-invalid' : ''); ?>">
                        <label class="profile_label">Last Name</label>
                        <input type="text" name="last_name" class="form-control" value="<?php echo e($user->last_name); ?>">
                        <?php if($errors->has('last_name')): ?>
                          <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('last_name')); ?></strong>
                                    </span>
                        <?php endif; ?>
                      </div>
                      <?php if($user->hasRole('agent')): ?>
                        <div class="col-md-12">
                          <label class="profile_label">Your Title</label>
                          <input type="text" name="agent_title" class="form-control" value="Property Agent" disabled="disabled">
                        </div>
                      <?php elseif($user->hasRole('agent')): ?>
                        <div class="col-md-12">
                          <label class="profile_label">Your Title</label>
                          <input type="text" name="agency_title" class="form-control" value="Agency" disabled="disabled">
                        </div>
                      <?php endif; ?>
                      <?php if($user->hasRole('user')): ?>
                        <div class="col-md-12 <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">Email Address :</label>
                          <input type="text" name="email" class="form-control" placeholder="Enter Your Email" value="<?php echo e($user->email); ?>">
                          <?php if($errors->has('email')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                        <div class="col-md-12 <?php echo e($errors->has('phone') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">Phone :</label>
                          <input type="tel" name="phone" class="form-control" placeholder="000-000-000" value="<?php echo e(str_limit($user->mobile, $limit = 11,$end="")); ?>">
                          <?php if($errors->has('phone')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('phone')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                      <?php elseif($user->hasRole('agent')): ?>
                        <div class="col-md-12 <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">Email Address :</label>
                          <input type="text" name="email" class="form-control" placeholder="Enter Your Email" value="<?php echo e($user->email); ?>">
                          <?php if($errors->has('email')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                        <div class="col-md-12 <?php echo e($errors->has('phone') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">Phone :</label>
                          <input type="tel" name="phone" class="form-control" placeholder="000-000-000" value="<?php echo e(str_limit($user->mobile, $limit = 11,$end="")); ?>">
                          <?php if($errors->has('phone')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('phone')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                        <div class="col-md-12 <?php echo e($errors->has('a_email') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">Agency Email Address :</label>
                          <input type="text" name="a_email" class="form-control" placeholder="Enter Your Email" value="<?php echo e($user->agency[0]->agency_email); ?>">
                          <?php if($errors->has('a_email')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('a_email')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                        <div class="col-md-12 <?php echo e($errors->has('a_phone') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">Agency Phone Number:</label>
                          <input type="tel" name="a_phone" class="form-control" placeholder="000-000-000" value="<?php echo e(str_limit($user->agency[0]->number, $limit = 11,$end="")); ?>">
                          <?php if($errors->has('a_phone')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('a_phone')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                      <?php elseif($user->hasRole('agency')): ?>
                        <div class="col-md-12 <?php echo e($errors->has('a_email') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">Agency Email Address :</label>
                          <input type="text" name="a_email" class="form-control" placeholder="Enter Your Email" value="<?php echo e($user->agency[0]->agency_email); ?>">
                          <?php if($errors->has('a_email')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('a_email')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                        <div class="col-md-12 <?php echo e($errors->has('a_phone') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">Agency Phone Number:</label>
                          <input type="tel" name="a_phone" class="form-control" placeholder="000-000-000" value="<?php echo e(str_limit($user->agency[0]->number, $limit = 11,$end="")); ?>">
                          <?php if($errors->has('a_phone')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('a_phone')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                      <?php endif; ?>
                      <?php if($user->hasRole('user')): ?>
                        <div class="col-md-12 <?php echo e($errors->has('about_me') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">About Me :</label>
                          <textarea class="form-control" placeholder="Write About Here....." name="about_me"><?php echo e($user->about); ?></textarea>
                          <?php if($errors->has('about_me')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('about_me')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                      <?php elseif($user->hasRole('agent')): ?>
                        <div class="col-md-12 <?php echo e($errors->has('about_me') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">About Me :</label>
                          <textarea class="form-control" placeholder="Write About Here....." name="about_me"><?php echo e($user->about); ?></textarea>
                          <?php if($errors->has('about_me')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('about_me')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                        <div class="col-md-12 <?php echo e($errors->has('about_agency') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">About My Agency :</label>
                          <textarea class="form-control" placeholder="Write About Here....." name="about_agency"><?php echo e($user->agency[0]->about_agency); ?></textarea>
                          <?php if($errors->has('about_agency')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('about_agency')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                      <?php elseif($user->hasRole('agency')): ?>
                        <div class="col-md-12 <?php echo e($errors->has('about_agency') ? ' is-invalid' : ''); ?>">
                          <label class="profile_label">About Agency :</label>
                          <textarea class="form-control" placeholder="Write About Here....." name="about_agency"><?php echo e($user->agency[0]->about_agency); ?></textarea>
                          <?php if($errors->has('about_agency')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('about_agency')); ?></strong>
                                    </span>
                          <?php endif; ?>
                        </div>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
                <div class="more_info">
                  <h4 class="inner-title">More Info</h4>
                  <div class="information_form">
                    <div class="row">
                      <div class="col-md-12 form-group <?php echo e($errors->has('address') ? ' is-invalid' : ''); ?>">
                        <label class="profile_label">Address</label>
                        <input type="text" name="address" class="form-control" placeholder="Your Location Address" value="<?php echo e($user->address); ?>">
                        <?php if($errors->has('address')): ?>
                          <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('address')); ?></strong>
                                    </span>
                        <?php endif; ?>
                      </div>
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-md-6 col-xs-12 <?php echo e($errors->has('city_state') ? ' is-invalid' : ''); ?>">
                            <label class="profile_label">City/state :</label>
                            <input type="text" name="city_state" class="form-control" placeholder="City Or State" value="<?php echo e($user->city_state); ?>">
                            <?php if($errors->has('city_state')): ?>
                              <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('city_state')); ?></strong>
                                    </span>
                            <?php endif; ?>
                          </div>
                          <div class="col-md-6 col-xs-12 <?php echo e($errors->has('zipcode') ? ' is-invalid' : ''); ?>">
                            <label class="profile_label">Zip code :</label>
                            <input type="text" name="zipcode" class="form-control" placeholder="Zip Code" value="<?php echo e($user->zipcode); ?>">
                            <?php if($errors->has('zipcode')): ?>
                              <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('zipcode')); ?></strong>
                                    </span>
                            <?php endif; ?>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12 <?php echo e($errors->has('dob') ? ' is-invalid' : ''); ?>">
                        <label class="profile_label">Date of Birth :</label>
                        <input type="date" class="form-control name" name="dob" value="<?php echo e($user->dob); ?>">
                        <?php if($errors->has('dob')): ?>
                          <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('dob')); ?></strong>
                                    </span>
                        <?php endif; ?>
                      </div>
                      <div class="col-md-12 <?php echo e($errors->has('skype_id') ? ' is-invalid' : ''); ?>">
                        <label class="profile_label">Skype ID :</label>
                        <input type="text" class="form-control" name="skype_id" placeholder="Your Skype ID" value="<?php echo e($user->skype_id); ?>">
                        <?php if($errors->has('skype_id')): ?>
                          <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('skype_id')); ?></strong>
                                    </span>
                        <?php endif; ?>
                      </div>
                      <div class="form-group <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>">
                        <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
                        <?php if($errors->has('password')): ?>
                          <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                        <?php endif; ?>
                      </div>
                      <div class="form-group <?php echo e($errors->has('password_confirmation') ? ' is-invalid' : ''); ?>">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                      </div>
                    </div>
                  </div>
                </div>
                <?php if($user->hasRole('agent|agency')): ?>
                  <input type="hidden" name="agency" value="<?php echo e($user->agency[0]->id); ?>">
                <?php endif; ?>
                <div class="save_change">
                  <button class="btn btn-default" type="submit">Save Changes</button>
                  <p><span>Note : </span>Morbi nibh dis. Pede. Erat, porta urna. Adipiscing Ipsum nibh morbi taciti proin quisque sit quam curae; vulputate ridiculus. Dictumst ullamcorper nullam Parturient, urna. Etiam nascetur enim lectus torquent pellentesque.</p>
                </div>

              </div>
              <div class="col-md-4 col-sm-12">
                <div class="prifile_picture avata-form">

                  <img src="<?php echo e(asset('uploads/profile_pictures')); ?>/<?php echo e($user->profile_picture); ?>" alt="" />
                  <input type="file" name="user_image" id="avata-upload">
                  <label class="avata-edit" for="avata-upload"><i class="flaticon-tool-1"></i></label>

                </div>
                
                
                
              </div>
            </form>

          </div>
        </div>

      </div>
    </div>
  </section>
  <!-- Profile Setting End -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('indexlayout.singlemaster', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
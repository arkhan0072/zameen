<header id="header">
    <!-- Top Header Start -->
    <div id="top_header">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-5">
                    <div class="top_contact">
                        <ul>
                            <li><i class="fa fa-phone" aria-hidden="true"></i> Need Support ? +1-435-782-4312</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-7">
                    <div class="top_right">
                        <ul>
                            <li>
                                <div class="lan-drop"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Eng <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">sp</a></li>
                                        <li><a href="#">ch</a></li>
                                        <li><a href="#">ud</a></li>
                                    </ul>
                                </div>
                            </li>

                            <?php if(auth()->guard()->check()): ?>
                            <li><a href="<?php echo e(route('logout')); ?>"
                               onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">Logout
                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST"
                                          style="display: one;">
                                        <?php echo e(csrf_field()); ?>

                                    </form>
                            </a></li>
                            <?php else: ?>
                            <li><a href="<?php echo e(route('register')); ?>" class="toogle_btn" >Register</a></li>
                            <li><a href="<?php echo e(route('login')); ?>" class="toogle_btn" >Login</a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Top Header End -->

    <!-- Nav Header Start -->
    <div id="nav_header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <nav class="navbar navbar-default nav_edit">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#"><img class="nav-logo" src="<?php echo e(asset('uniland/img/logo1.png')); ?>" alt=""></a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse my_nav" id="bs-example-navbar-collapse-1">
                            <div class="submit_property">
                                <a href="<?php echo e(route('propertysubmit')); ?>"><i class="fa fa-plus" aria-hidden="true"></i>Submit Property</a>
                            </div>

                            <ul class="nav navbar-nav navbar-right nav_text">
                                <li class="dropdown">
                                    <a href="index_1.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="index_1.html">Simple Image Slider</a></li>
                                        <li><a href="index_2.html">Fixed Height Map</a></li>
                                        <li><a href="index_3.html">Video Banner + Search</a></li>
                                        <li class="active"><a href="index_4.html">Fixed Banner + Search</a></li>
                                        <li><a href="index_5.html">Property Slide</a></li>
                                        <li><a href="index_6.html">Full Height Map + Nav</a></li>
                                        <li><a href="index_7.html">Fixed Height Map + Search</a></li>
                                        <li><a href="index_8.html">Full Height Map + Search</a></li>
                                        <li><a href="index_9.html">Map Left + Search Right</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Properties <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Property List <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="property_list.html">Right Sidebar</a></li>
                                                <li><a href="property_list_left.html">Left Sidebar</a></li>
                                                <li><a href="property_list_full.html">Full Width</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Property Grid <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="property_grid.html">Right Sidebar</a></li>
                                                <li><a href="property_grid_left.html">Left Sidebar</a></li>
                                                <li><a href="property_grid_full.html">Full Width</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Single Properties <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="single_property.html">Single Property 1</a></li>
                                                <li><a href="single_property_2.html">Single Property 2</a></li>
                                                <li><a href="single_property_3.html">Single Property 3</a></li>
                                                <li><a href="single_property_4.html">Single Property 4</a></li>
                                                <li><a href="single_property_5.html">Single Property 5</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Property With Map <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="property_grid_map.html">Grid View</a></li>
                                                <li><a href="property_list_map.html">List View</a></li>
                                                <li><a href="property_grid_fullmap.html">Grid View Full Map</a></li>
                                                <li><a href="property_list_fullmap.html">List View Full Map</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Agents <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="agents.html">Agents</a></li>
                                        <li><a href="agent_profile_grid.html">Agent Profile Grid</a></li>
                                        <li><a href="agent_profile_list.html">Agent Profile List</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="about.html">About Us</a></li>
                                                <li><a href="mission.html">Our Mission</a></li>
                                                <li><a href="career.html">Careers</a></li>
                                                <li><a href="award.html">Awards</a></li>
                                                <li><a href="testimonial.html">Testimonials</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo e(route('user.profile')); ?>">My Profile</a></li>
                                                <li><a href="<?php echo e(route('user.smedia')); ?>">Social Media</a></li>
                                                <li><a href="<?php echo e(route('user.properties')); ?>">My Properties</a></li>
                                                <li><a href="<?php echo e(route('user.fproperties')); ?>">Favorited Properties</a></li>
                                                <li><a href="<?php echo e(route('propertysubmit')); ?>">Submit New Property</a></li>
                                                <li><a href="<?php echo e(route('user.message')); ?>">Message</a></li>
                                                <li><a href="<?php echo e(route('user.feedback')); ?>">Feedback and Comments</a></li>
                                                <li><a href="<?php echo e(route('user.payments')); ?>">Payments and Invoice</a></li>
                                                <li><a href="<?php echo e(route('user.changepassword')); ?>">Change Password</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="our_service.html">Our Service</a></li>
                                        <li><a href="submit_property.html">Submit Property</a></li>
                                        <li><a href="terms_and_condition.html">Terms And Condition</a></li>
                                        <li><a href="pricing_table.html">Pricing Table</a></li>
                                        <li><a href="invoice_details.html">Invoice</a></li>
                                        <li><a href="message_view.html">Message</a></li>
                                        <li><a href="error.html">Error Page</a></li>
                                        <li><a href="faq.html">FAQ</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Blog <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="blog_grid.html">Blog Grid</a></li>
                                        <li><a href="blog.html">Blog List</a></li>
                                        <li><a href="blog_detail.html">Blog Detail</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- Nav Header End -->
</header>
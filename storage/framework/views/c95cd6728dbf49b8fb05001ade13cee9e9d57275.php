<?php $__env->startSection('content'); ?>
  <!-- Banner Section Start -->
  <section id="banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="banner_area">
            <h3 class="page_title">My Properties</h3>
            <div class="page_location">
              <a href="index_1.html">Home</a>
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <a href="index_1.html">Pages</a>
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <span>My Properties</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Banner Section End -->
  <!-- Profile Setting Start -->
  <section id="profile_setting">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-12">

        </div>
        <div class="col-md-8 col-sm-12">
          <div class="my_property_list">
            <table class="list_table" border="0" cellpadding="0" cellspacing="0">
              <thead>
              <tr>
                <td>Properties</td>
                <td>Added Date</td>
                <td>Views</td>
                <td>Review</td>
                <td>Action</td>
              </tr>
              </thead>
              <tbody>
              <?php $__currentLoopData = $mylistings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mylisting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td>
                    <img src="<?php echo e(asset('uploads/listings')); ?>/<?php echo e($mylisting->photos[0]->name); ?>" alt="" >
                    <div class="property-text">
                      <a href="#"><h6 class="property-title">Condos In the Middle of Roseland</h6></a>
                      <span><i class="fa fa-map-marker" aria-hidden="true"></i>3896 Longview Avenue</span>
                      <div class="listing_price">$1800/mo</div>
                    </div>
                  </td>
                  <td>
                    <span>July 28, 2017</span>
                  </td>
                  <td>
                    <span>2</span>
                  </td>
                  <td class="rating">
											<span title="0 Review">
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											</span>
                  </td>
                  <td>
                    <span><a href="<?php echo e(route('listing.edit', $mylisting->id)); ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a></span>
                    <span><a href="#"><i class="fa fa-times" aria-hidden="true"></i> Delete</a></span>
                  </td>
                </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
          <!-- Pagination -->
          <div class="row">
            <div class="col-md-12">
              <div class="pagination_area">
                <nav aria-label="Page navigation">
                  <ul class="pagination pagination_edit">
                    <li>
                      <a href="#" aria-label="Previous">
                        <span aria-hidden="true">prev</span>
                      </a>
                    </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                      <a href="#" aria-label="Next">
                        <span aria-hidden="true">next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          <!-- End Pagination -->
        </div>
      </div>
    </div>
  </section>
  <!-- Profile Setting End -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('indexlayout.singlemaster', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
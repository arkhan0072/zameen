<div class="settings_links">
    <ul>
        <li><span>Account Settings</span></li>
        <li class="active"><a href="<?php echo e(route('user.profile')); ?>"><i class="flaticon-user"></i>My Profile</a></li>
        <li><a href="<?php echo e(route('user.smedia')); ?>"><i class="flaticon-share"></i>Social Media</a></li>
    </ul>
    <ul>
        <li><span>Manage Properties</span></li>
        <li><a href="<?php echo e(route('user.properties')); ?>"><i class="flaticon-home"></i>My Properties</a></li>
        <li><a href="<?php echo e(route('user.fproperties')); ?>"><i class="flaticon-star"></i>Favorited Properties</a></li>
        <li><a href="<?php echo e(route('propertysubmit')); ?>"><i class="flaticon-export"></i>Submit New Property</a></li>
    </ul>
    <ul>
        <li><a href="<?php echo e(route('user.message')); ?>"><i class="flaticon-notification"></i>Message <sup><i class="fa fa-circle" aria-hidden="true"></i></sup> <span>( 20 )</span></a></li>
        <li><a href="<?php echo e(route('user.feedback')); ?>l"><i class="flaticon-chat"></i>Feedback and Comments</a></li>
        <li><a href="<?php echo e(route('user.payments')); ?>"><i class="flaticon-invoice"></i>Payments and Invoice</a></li>
    </ul>
    <ul>
        <li><a href="<?php echo e(route('user.changepassword')); ?>"><i class="flaticon-locked"></i>Change Password</a></li>
        <li><a onclick="event.preventDefault();document.getElementById('logout_form').submit();"><i class="flaticon-upload"></i>Log Out</a>
            <form  id='logout_form' action="<?php echo e(route('logout')); ?>" method="post">
                <?php echo e(csrf_field()); ?>

            </form>
        </li>
    </ul>
</div>
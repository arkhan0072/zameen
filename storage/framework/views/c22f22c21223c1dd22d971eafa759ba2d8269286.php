<?php $__env->startSection('content'); ?>
<!-- Banner Section Start -->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner_area">
                    <h3 class="page_title"><?php echo e($listing->title); ?></h3>
                    
                        
                        
                        
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->

<!-- Single Property Start -->
<section id="single_property">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="property_slider">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            
                                
                            
                            <?php $__currentLoopData = $listing->photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $picture): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="item <?php echo e($key ?'':'active'); ?>">

                                <img src="<?php echo e(asset('uploads/listings')); ?>/<?php echo e($picture->name); ?>" alt="" style="height: 620px!important;width: 1170px!important;">
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                                
                            
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <div class="lef_btn">prev</div>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <div class="right_btn">next</div>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="property-summary2">
                    <div class="profile_data">
                        <ul>
                            <li><span>Property ID :</span> <?php echo e(str_limit($listing->project->city->state->name, $limit = 1,$end="")); ?><?php echo e($listing->project->city->state->id); ?><?php echo e(str_limit($listing->project->city->name, $limit = 1,$end="")); ?><?php echo e($listing->project->city_id); ?><?php echo e($listing->id); ?></li>
                            <li><span>Listing Type :</span> <?php echo e($listing->purpose->name); ?></li>
                            <li><span>Property Type :</span> <?php echo e($listing->category->name); ?></li>
                            <li><span>Price :</span> $<?php echo e($listing->price); ?></li>
                            <li><span>Area :</span> <?php echo e($listing->area); ?> sqft</li>
                            <li><span>Car Garage :</span> Yes ( 5 Capacity )</li>
                            <li><span>Swimming :</span> Yes ( 1 Large )</li>
                            <li><span>Garden :</span> Yes</li>
                            <li><span>Rating :</span>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i> (12)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="detail_text">
                            <div class="property-text">
                                <h4 class="property-title"><?php echo e($listing->title); ?></h4>
                                <span><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo e($listing->project->city->name); ?>,<?php echo e($listing->project->city->state->name); ?> </span>
                                <blockquote>Dis at habitasse viverra nisl. Arcu nec id imperdiet venenatis, lacus sollicitudin in et libero sed senectus ullamcorper conubia velit metus lacinia duis fermentum dictumst ut phasellus aptent tempor lectus.</blockquote>
                                <p><?php echo e(strip_tags($listing->description)); ?></p>
                            </div>
                        </div>
                        <div class="more_information">
                            <h4 class="inner-title">More Information</h4>
                            <div class="profile_data">
                                <ul>
                                    <li><span>Age :</span> 10 Years</li>
                                    <li><span>Type :</span> Appartment</li>
                                    <li><span>Installment Facility :</span> Yes</li>
                                    <li><span>Insurance :</span> Yes</li>
                                    <li><span>3rd Party :</span> No</li>
                                    <li><span>Swiming Pool :</span> Yes</li>
                                    <li><span>Garden and Field :</span> 2400sqft</li>
                                    <li><span>Total Floor :</span> 3 Floor</li>
                                    <li><span>Security :</span> 3 Step Security</li>
                                    <li><span>Alivator :</span> 2 Large</li>
                                    <li><span>Dining Capacity :</span> 20 People</li>
                                    <li><span>Exit :</span> 3 Exit Gate</li>
                                    <li><span>Fire Place :</span> Yes</li>
                                    <li><span>Heating System :</span> Floor Heating</li>
                                </ul>
                            </div>
                        </div>
                        <div class="single_video">
                            <video width="400" controls poster="video/poster.png">
                                <source src="<?php echo e(asset('uniland/video/real_estate.html')); ?>" type="video/mp4">
                            </video>
                        </div>
                        <div class="single_feature">
                            <h4 class="inner-title">Features</h4>
                            <div class="info-list">
                                <ul>
                                    <?php $__currentLoopData = $listing->features; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$feature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span><?php echo e($feature->pivot->feature_value); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                        <div class="single_map">
                            <h4 class="inner-title">Location</h4>
                            <div id="map" class="map-canvas"> </div>
                        </div>
                        
                            
                            
                                
                                    
                                        
                                        
                                            
                                                
                                                
                                                
                                                
                                                
                                            
                                        
                                    
                                    
                                        
                                    
                                    
                                        
                                    
                                    
                                        
                                    
                                    
                                        
                                    
                                
                            
                        
                    </div>
                </div>

                
                
                    
                        
                            
                            
                                
                                    
                                        
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                    
                                                    
                                                    
                                                
                                            
                                            
                                            
                                        
                                    
                                
                                
                                    
                                        
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                    
                                                    
                                                    
                                                
                                            
                                            
                                            
                                        
                                    
                                
                                
                                    
                                        
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                    
                                                    
                                                    
                                                
                                            
                                            
                                            
                                        
                                    
                                
                                
                                    
                                        
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                    
                                                    
                                                    
                                                
                                            
                                            
                                            
                                        
                                    
                                
                            
                        
                    
                
                
            </div>
            <div class="col-md-4">
                <div class="property_sidebar">
                    <div class="advance_search sidebar-widget">
                        <h4 class="widget-title">Advance Search</h4>
                        <form method="post" class="search_form" action="#">
                            <div class="row">
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control">
                                        <option>Any Status</option>
                                        <option>For Rent</option>
                                        <option>For Sale</option>
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control">
                                        <option>Any Type</option>
                                        <option>House</option>
                                        <option>Office</option>
                                        <option>Appartment</option>
                                        <option>Condos</option>
                                        <option>Villa</option>
                                        <option>Small Family</option>
                                        <option>Single Room</option>
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control" data-live-search="true">
                                        <option>Any States</option>
                                        <option>New York</option>
                                        <option>Sydney</option>
                                        <option>Washington</option>
                                        <option>Las Vegas</option>
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control" data-live-search="true">
                                        <option>All City</option>
                                        <option>New York</option>
                                        <option>Sydney</option>
                                        <option>Washington</option>
                                        <option>Las Vegas</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <select class="selectpicker form-control">
                                        <option>Beds</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>6</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <select class="selectpicker form-control">
                                        <option>Baths</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="min-price" placeholder="Min Price (USD)">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="email" placeholder="Max Price (USD)">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <button name="search" class="btn btn-default" type="submit">search property</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="contact_agent sidebar-widget">
                        <div class="agent_details">
                            <div class="author_img">
                                <img src="img/testimonial/2.png" alt="">
                            </div>
                            <div class="agent_info">
                                <h5 class="author_name"><?php echo e($listing->user->name); ?></h5>
                                <span>+( 81 ) 84 538 91231</span>
                            </div>
                            <form class="message_agent" action="#" method="post">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="name" placeholder="Your Name">
                                </div>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="email" placeholder="Your Email">
                                </div>
                                <div class="input-group">
                                    <textarea class="form-control" name="message" placeholder="Message"></textarea>
                                </div>
                                <div class="input-group">
                                    <button type="submit" class="btn btn-default" name="submit">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="featured_sidebar sidebar-widget">
                        <h4 class="widget-title">Featured Property</h4>
                        <div class="slide_featured">
                            <div class="item">
                                <div class="thumb">
                                    <div class="sidebar_img">
                                        <img src="img/property_grid/property_grid-4.png" alt="">
                                        <div class="sale_btn">sale</div>
                                        <div class="quantity">
                                            <ul>
                                                <li><span>Area</span>1600 Sqft</li>
                                                <li><span>Rooms</span>7</li>
                                                <li><span>Beds</span>4</li>
                                                <li><span>Baths</span>3</li>
                                                <li><span>Garage</span>1</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="property-text">
                                        <a href="#"><h6 class="property-title">New Developed Condos</h6></a>
                                        <div class="property_price">$150,000</div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="sidebar_img">
                                        <img src="img/property_grid/property_grid-5.png" alt="">
                                        <div class="sale_btn">sale</div>
                                        <div class="quantity">
                                            <ul>
                                                <li><span>Area</span>1600 Sqft</li>
                                                <li><span>Rooms</span>7</li>
                                                <li><span>Beds</span>4</li>
                                                <li><span>Baths</span>3</li>
                                                <li><span>Garage</span>1</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="property-text">
                                        <a href="#"><h6 class="property-title">New Developed Condos</h6></a>
                                        <div class="property_price">$150,000</div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="sidebar_img">
                                        <img src="img/property_grid/property_grid-6.png" alt="">
                                        <div class="sale_btn">sale</div>
                                        <div class="quantity">
                                            <ul>
                                                <li><span>Area</span>1600 Sqft</li>
                                                <li><span>Rooms</span>7</li>
                                                <li><span>Beds</span>4</li>
                                                <li><span>Baths</span>3</li>
                                                <li><span>Garage</span>1</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="property-text">
                                        <a href="#"><h6 class="property-title">New Developed Condos</h6></a>
                                        <div class="property_price">$150,000</div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="sidebar_img">
                                        <img src="img/property_grid/property_grid-7.png" alt="">
                                        <div class="sale_btn">sale</div>
                                        <div class="quantity">
                                            <ul>
                                                <li><span>Area</span>1600 Sqft</li>
                                                <li><span>Rooms</span>7</li>
                                                <li><span>Beds</span>4</li>
                                                <li><span>Baths</span>3</li>
                                                <li><span>Garage</span>1</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="property-text">
                                        <a href="#"><h6 class="property-title">New Developed Condos</h6></a>
                                        <div class="property_price">$150,000</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Related Property -->
        <div class="row">
            <div class="col-md-12">
                <div class="inner-feature">
                    <h4 class="inner-title">Related Properties</h4>
                    <div class="property_slide">
                        <div class="item">
                            <div class="property_grid">
                                <div class="img_area">
                                    <div class="sale_btn">Rent</div>
                                    <a href="#"><img src="img/property_grid/property_grid-1.png" alt=""></a>
                                    <div class="sale_amount">12 Hours Ago</div>
                                    <div class="hover_property">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-text">
                                    <a href="#">
                                        <h5 class="property-title">Park Road Appartment Rent</h5>
                                    </a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 3225 George Street Brooksville, FL 34610</span>
                                    <div class="quantity">
                                        <ul>
                                            <li><span>Area</span>1600 Sqft</li>
                                            <li><span>Rooms</span>9</li>
                                            <li><span>Beds</span>4</li>
                                            <li><span>Baths</span>3</li>
                                            <li><span>Garage</span>1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bed_area">
                                    <ul>
                                        <li>$1250/mo</li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="property_grid">
                                <div class="img_area">
                                    <div class="sale_btn">Sale</div>
                                    <div class="featured_btn">Featured</div>
                                    <a href="#"><img src="img/property_grid/property_grid-2.png" alt=""></a>
                                    <div class="sale_amount">12 Hours Ago</div>
                                    <div class="hover_property">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-text">
                                    <a href="#">
                                        <h5 class="property-title">Park Road Appartment Rent</h5>
                                    </a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 3494 Lyon Avenue Middleboro, MA 02346 </span>
                                    <div class="quantity">
                                        <ul>
                                            <li><span>Area</span>1600 Sqft</li>
                                            <li><span>Rooms</span>9</li>
                                            <li><span>Beds</span>4</li>
                                            <li><span>Baths</span>3</li>
                                            <li><span>Garage</span>1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bed_area">
                                    <ul>
                                        <li>$1,410,000</li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="property_grid">
                                <div class="img_area">
                                    <div class="sale_btn">Rent</div>
                                    <a href="#"><img src="img/property_grid/property_grid-3.png" alt=""></a>
                                    <div class="sale_amount">12 Hours Ago</div>
                                    <div class="hover_property">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-text">
                                    <a href="#">
                                        <h5 class="property-title">Park Road Appartment Rent</h5>
                                    </a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 39 Parrill Court Oak Brook, IN 60523 </span>
                                    <div class="quantity">
                                        <ul>
                                            <li><span>Area</span>1600 Sqft</li>
                                            <li><span>Rooms</span>9</li>
                                            <li><span>Beds</span>4</li>
                                            <li><span>Baths</span>3</li>
                                            <li><span>Garage</span>1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bed_area">
                                    <ul>
                                        <li>$1200/mo</li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="property_grid">
                                <div class="img_area">
                                    <div class="sale_btn">Rent</div>
                                    <a href="#"><img src="img/property_grid/property_grid-4.png" alt=""></a>
                                    <div class="sale_amount">12 Hours Ago</div>
                                    <div class="hover_property">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-text">
                                    <a href="#">
                                        <h5 class="property-title">Central Road Appartment Rent</h5>
                                    </a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 39 Parrill Court Oak Brook, IN 60523 </span>
                                    <div class="quantity">
                                        <ul>
                                            <li><span>Area</span>1600 Sqft</li>
                                            <li><span>Rooms</span>9</li>
                                            <li><span>Beds</span>4</li>
                                            <li><span>Baths</span>3</li>
                                            <li><span>Garage</span>1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bed_area">
                                    <ul>
                                        <li>$1200/mo</li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Related Property -->
    </div>
</section>
<!-- Single Property End -->
    <?php $__env->stopSection(); ?>
		
<?php echo $__env->make('indexlayout.singlemaster', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
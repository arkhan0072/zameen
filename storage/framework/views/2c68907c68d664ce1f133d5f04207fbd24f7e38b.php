<?php $__env->startSection('content'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    
    
    
    
    
    
    
    
    
    

    <!-- Main content -->
        <section class="content">
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div><?php echo e($error); ?></div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Project</h3>
                </div>
                <!-- /.box-header -->
                <form role="form" method="post" action="<?php echo e(route('project.update', $project->id)); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div><?php echo e($error); ?></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php echo method_field('PUT'); ?>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('title') ? 'has-error': ''); ?>">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" placeholder="Title"
                                           required="required" value="<?php echo e(old('title',$project->title)); ?>">
                                </div>
                            </div>
                            <?php if($errors->has('title')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('title')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('country') ? 'has-error': ''); ?>">
                                    <label>Country</label>
                                    <select class="form-control" name="country">
                                        <option value="">Please Select Coutnry</option>
                                        <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($country->id); ?>" selected><?php echo e($country->country_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <?php if($errors->has('country')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('country')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group<?php echo e($errors->has('state') ? 'has-error': ''); ?>">
                                    <label>State</label>
                                    <select class="form-control" id="state" name="state">
                                        <option value="">Please Select State</option>
                                        <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($sat->id == $project->city->state->id): ?>
                                                <option value="<?php echo e($sat->id); ?>" selected><?php echo e($sat->name); ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo e($sat->id); ?>"><?php echo e($sat->name); ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <?php if($errors->has('state')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('state')); ?></strong>
                                </span>
                            <?php endif; ?>

                            <div class="col-md-6" id="subcity">
                                <div class="form-group<?php echo e($errors->has('city') ? 'has-error': ''); ?>">
                                    <label>City</label>
                                    <select class="form-control" id="city" name="city">
                                        <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($city->id == $project->city_id): ?>
                                                <option value="<?php echo e($city->id); ?>" selected><?php echo e($city->name); ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo e($city->id); ?>"><?php echo e($city->name); ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <?php if($errors->has('city')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('city')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('address') ? 'has-error': ''); ?>">
                                    <label>Address</label>
                                    <input type="text" class="form-control" name="address" value="<?php echo e($project->address); ?>" placeholder="Enter Your Property Address">
                                </div>
                            </div>
                            <?php if($errors->has('address')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('address')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('zipcode') ? 'has-error': ''); ?>">
                                    <label>ZipCode</label>
                                    <input type="text" class="form-control" name="zipcode" value="<?php echo e($project->zipcode); ?>" placeholder="Enter Your area Zipcode">
                                </div>
                            </div>
                            <?php if($errors->has('zipcode')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('zipcode')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <br>
                            <div class="col-md-12">
                                <div class="form-group<?php echo e($errors->has('description') ? 'has-error': ''); ?>">
                                    <label>Description</label>
                                    <textarea id="messageArea" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message..">
                                        <?php echo e($project->description); ?>

                                    </textarea>
                                </div>
                            </div>
                            <?php if($errors->has('description')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('description')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group<?php echo e($errors->has('price') ? 'has-error': ''); ?>">
                                    <label>Price</label>
                                    <input type="text" class="form-control" name="price" placeholder="Purpose"
                                           required="required" value="<?php echo e(old('price',$project->price)); ?>">
                                </div>
                            </div>
                            <?php if($errors->has('price')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('price')); ?></strong>
                                </span>
                            <?php endif; ?>
                            &nbsp;
                            <div class="col-md-6">
                                <div class="form-group<?php echo e($errors->has('price') ? 'has-error': ''); ?>">
                                    <label>Picture</label>
                                    <input type="file" class="form-control" name="picture_project[]" required="required" multiple>
                                </div>
                            </div>
                            <?php if($errors->has('price')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('price')); ?></strong>
                                </span>
                            <?php endif; ?>
                            &nbsp;
                            <div class="col-md-6">
                                <div class="form-group<?php echo e($errors->has('area') ? 'has-error': ''); ?>">
                                    <label>Area</label>
                                    <input type="text" class="form-control" name="area" placeholder="Area"
                                           required="required" value="<?php echo e($project->area); ?>">
                                </div>
                            </div>
                            <?php if($errors->has('area')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('area')); ?></strong>
                                </span>
                            <?php endif; ?>

                            <div class="col-md-6">
                                <div class="form-group<?php echo e($errors->has('area') ? 'has-error': ''); ?>">
                                    <label>Unit</label>
                                    <select class="form-control" name="areaType">

                                        <option value="Square Feet">Square Feet</option>
                                        <option value="Square Yards">Square Yards</option>
                                        <option value="Square Meters">Square Meters</option>
                                        <option value="Marla">Marla</option>
                                        <option value="Kanal">Kanal</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Features Category:</label>
                                <select id="category_feature" class="form-control" name="feature_category">
                                    <option value="">Select Features Category</option>
                                    <?php $__currentLoopData = $feature_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feature_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($feature_category->id); ?>"><?php echo e($feature_category->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>


                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Features:</label>
                                <select id="feature" class="form-control" name="feature">

                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="feature_value">Value&nbsp;</label>
                                <input type="text" class="form-control" id="feature_value" name="feature_value" placeholder="Enter feature value">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="add-button">&nbsp;</label>
                                <button id="add" class="btn btn-success form-control"><i class="fa fa-plus-square"></i> Add</button>
                            </div>



                            <div class="form-group col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Feature Category</th>
                                        <th>Feature</th>
                                        <th>Value</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="features_table">
                                    <?php $__currentLoopData = $project->features; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="deleteFromList">
                                            <td><?php echo e($feature->category->name); ?></td>
                                            <td><?php echo e($feature->name); ?></td>
                                            <td><input type="text" class="form-control" name="feature_value[<?php echo e($feature->id); ?>]"  value="<?php echo e($feature->pivot->feature_value); ?>"></td>
                                            <td><a class="btn btn-warning deleteFromList"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>

                                </table>
                            </div>&nbsp;
                            <div class="col-md-12">

                                <div class="form-group right-float">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-plus mr-5"></i> Update Project
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div>
                </form>
                <section class="content col-md-12">
                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header bg-dark">
                            <h4 class="box-title">Picture Gallery</h4>
                            <div class="box-controls pull-right">
                                <div class="pull-right btn-group">
                                    <button class="btn btn-info gallery-header-center-right-links-current" id="filter-all">All</button>
                                    <button class="btn btn-info gallery-header-center-right-links-current" id="filter-studio">Studio</button>
                                    <button class="btn btn-info gallery-header-center-right-links-current" id="filter-landscape">Landscapes</button>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">
                                        <div class="row">
                                            <?php $__currentLoopData = $project->photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="col-md-4">
                                                    <div class="pull-right">
                                                        <a title="Edit" class="btn btn-info" data-id="<?php echo e($project->id); ?>" data-toggle="modal" data-target="#myModal<?php echo e($project->id); ?>"><i class="fa fa-edit "></i></a>
                                                        <a title="Delete" class="btn btn-danger" onclick="delphoto<?php echo e($project->id); ?>.submit();"><i class="fa fa-trash "></i></a>
                                                        <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="delphoto<?php echo e($project->id); ?>" action="<?php echo e(route('destroyProjectPhoto',$project->id)); ?>" method="post">
                                                            <?php echo e(csrf_field()); ?>

                                                        </form>
                                                    </div>
                                                    <div class="p-3">
                                                        <div id="myModal<?php echo e($project->id); ?>" class="modal fade" role="dialog">
                                                            <form action="<?php echo e(route('projectphotoupdate', $project->id)); ?>" method="post" enctype="multipart/form-data">
                                                                <?php echo csrf_field(); ?>

                                                                <div class="modal-dialog">

                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            <h4 class="modal-title">Modal Header</h4>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                            <input type="file" class="form-control" name="picture_project">
                                                                            <button type="submit" name="upload" data-id="<?php echo e($project->id); ?>" id="editpicture">Edit Picture</button>

                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </form>
                                                        </div>
                                                        <a href="<?php echo e(asset('uploads/listings')); ?>/<?php echo e($project->name); ?>" data-toggle="lightbox" id="lightbox" data-gallery="multiimages" data-title="Image title will be apear here" ><img src="<?php echo e(asset('uploads/project_pictures')); ?>/<?php echo e($project->name); ?>" alt="gallery" class="all studio" /> </a>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </section>

                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_script'); ?>
    <script>
        $('#category').on('change',function () {
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '<?php echo e(route('adminfiltersubcat')); ?>',
                data: {id: id},
                success: function (result) {
                    $('#subcategory').html(result);
                    console.log(result);
                }
            })
        });
    </script>
    <script>
        $('#state').on('change', function(){
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '<?php echo e(route('adminfiltersubcity')); ?>',
                data: {id: id},
                success: function (result) {
                    $('#city').html(result);
                    console.log(result);
                }
            })
        });
    </script>
    <script>
        $('#city').on('change', function(){
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '<?php echo e(route('adminfiltercityproject')); ?>',
                data: {id: id},
                success: function (result) {
                    $('#cityProject').html(result);
                    console.log(result);
                }
            })
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#category_feature").change(function()
            {
                var id=$(this).val();
                $.ajax
                ({
                    type: "GET",
                    url: "<?php echo e(route('adminfeatures_category.list')); ?>",
                    data: {
                        id : id
                    },
                    success: function(html)
                    {
                        $("#feature").html(html);
                    },
                    error : function (error) {
                        console.log(error);
                    }
                });

            });

        });
        $('#add').on('click', function (event) {
            event.preventDefault();
            var category = $('#category_feature option:selected')
            var feature = $('#feature option:selected');

            var feature_value = $('#feature_value');

            if(category.val() === '' || feature_value.val() === ''){
                alert('Please enter the values.');
            }
            else {

                var featureID = $('#feature').val();

                var html = '<tr>\n' +
                    '  <td>' + category.text() + '</td>' +
                    '  <td>' + feature.text() + '</td> ' +
                    '  <td><input type="text" class="form-control" name="feature_value[' + featureID + ']"  value="' + feature_value.val() + '"></td> ' +
                    '  <td><a class="btn btn-warning deleteFromList"><i class="fa fa-trash"></i></a></td> ' +
                    '</tr>';


                $('#features_table').append(html);

                $('#feature_value').val('');
            }

        });



        $(document).on('click', '.deleteFromList', function(){
            $(this).closest('tr').remove();
        });
        $(document).on('click','editpicture',function(event){
            event.preventDefault();
        });
    </script>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    
    
    
    
    
    
    
    
    
    

    <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Listing</h3>
                </div>
                <!-- /.box-header -->
                <form role="form" method="post" action="<?php echo e(route('alisting.store')); ?>" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div><?php echo e($error); ?></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('title') ? 'has-error': ''); ?>">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" placeholder="Title"
                                           required="required" value="">
                                </div>
                            </div>
                            <?php if($errors->has('title')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('title')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('category') ? 'has-error': ''); ?>">
                                    <label>Category</label>
                                    <select class="form-control" id="category" name="category">
                                        <option value="">Please Select Category</option>
                                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <?php if($errors->has('category')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('category')); ?></strong>
                                </span>
                            <?php endif; ?>
                             <div class="col-md-6" id="subcat">
                                    <div class="form-group <?php echo e($errors->has('subcategory') ? 'has-error': ''); ?>">
                                        <label>Sub Category</label>
                                        <select class="form-control" id="subcategory" name="subcategory">

                                        </select>
                                    </div>
                                </div>
                                <?php if($errors->has('name')): ?>
                                    <span class="help-block">
                            <strong><?php echo e($errors->first('name')); ?></strong>
                                </span>
                                <?php endif; ?>

                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('purpose') ? 'has-error': ''); ?>">
                                    <label>Purpose</label>
                                    <select class="form-control" name="purpose">
                                        <option value="">Please Select Purpose</option>
                                        <?php $__currentLoopData = $purposes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $purpose): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($purpose->id); ?>"><?php echo e($purpose->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <?php if($errors->has('purpose')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('purpose')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('country') ? 'has-error': ''); ?>">
                                    <label>Country</label>
                                    <select class="form-control" name="country">
                                        <option value="">Please Select Coutnry</option>
                                        <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($country->id); ?>"><?php echo e($country->country_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <?php if($errors->has('country')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('country')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('state') ? 'has-error': ''); ?>">
                                    <label>State</label>
                                    <select class="form-control" id="state" name="state">
                                        <option value="">Please Select Category</option>
                                        <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($sat->id); ?>"><?php echo e($sat->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <?php if($errors->has('state')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('state')); ?></strong>
                                </span>
                            <?php endif; ?>

                            <div class="col-md-6" id="subcity">
                                <div class="form-group <?php echo e($errors->has('city') ? 'has-error': ''); ?>">
                                    <label>City</label>
                                    <select class="form-control" id="city" name="city">

                                    </select>
                                </div>
                            </div>
                            <?php if($errors->has('city')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('city')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('name') ? 'has-error': ''); ?>">
                                    <label>Project</label>
                                    <select class="form-control" id="cityProject" name="project">
                                        <option value="0">Please Select Project</option>
                                    </select>
                                </div>
                            </div>
                            <?php if($errors->has('name')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('name')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('address') ? 'has-error': ''); ?>">
                                    <label>Address</label>
                                  <input type="text" class="form-control" name="address" placeholder="Enter Your Property Address">
                                </div>
                            </div>
                            <?php if($errors->has('address')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('address')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('zipcode') ? 'has-error': ''); ?>">
                                    <label>ZipCode</label>
                                    <input type="text" class="form-control" name="zipcode" placeholder="Enter Your area Zipcode">
                                </div>
                            </div>
                            <?php if($errors->has('zipcode')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('zipcode')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <br>
                            <div class="col-md-12">
                                <div class="form-group <?php echo e($errors->has('description') ? 'has-error': ''); ?>">
                                    <label>Description</label>
                                    <textarea id="messageArea" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message..">

                                    </textarea>
                                </div>
                            </div>
                            <?php if($errors->has('description')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('description')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('price') ? 'has-error': ''); ?>">
                                    <label>Price</label>
                                    <input type="text" class="form-control" name="price" placeholder="Purpose"
                                           required="required" value="">
                                </div>
                            </div>
                            <?php if($errors->has('price')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('price')); ?></strong>
                                </span>
                            <?php endif; ?>

                            <div class="col-md-6">
                                <div class="form-group <?php echo e($errors->has('no_of_rooms') ? 'has-error': ''); ?>">
                                    <label>No. Of Rooms</label>
                                    <input type="text" class="form-control" name="no_of_rooms" placeholder="No. Of Rooms"
                                           required="required" value="">
                                </div>
                            </div>
                            <?php if($errors->has('no_of_rooms')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('no_of_rooms')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="col-md-4">
                                <div class="form-group <?php echo e($errors->has('area') ? 'has-error': ''); ?>">
                                    <label>Area</label>
                                    <input type="text" class="form-control" name="area" placeholder="area"
                                           required="required" value="">
                                </div>
                            </div>
                            <?php if($errors->has('area')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('area')); ?></strong>
                                </span>
                            <?php endif; ?>

                            <div class="col-md-4">
                                <div class="form-group<?php echo e($errors->has('area') ? 'has-error': ''); ?>">
                                    <label>Unit</label>
                                <select class="form-control" name="areaType">
                                    <option value="Square Feet">Square Feet</option>
                                    <option value="Square Yards">Square Yards</option>
                                    <option value="Square Meters">Square Meters</option>
                                    <option value="Marla">Marla</option>
                                    <option value="Kanal">Kanal</option>
                                </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?php echo e($errors->has('picture') ? 'has-error': ''); ?>">
                                    <label>Picture</label>
                            <input type="file" class="form-control" name="picture[]" multiple required>
                                </div>
                            </div>
                            <?php if($errors->has('picture')): ?>
                                <span class="help-block">
                            <strong><?php echo e($errors->first('picture')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Features Category:</label>
                                <select id="category_feature" class="form-control" name="feature_category">
                                    <option value="">Select Features Category</option>
                                    <?php $__currentLoopData = $feature_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feature_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($feature_category->id); ?>"><?php echo e($feature_category->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>


                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Features:</label>
                                <select id="feature" class="form-control" name="feature">

                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="feature_value">Value&nbsp;</label>
                                <input type="text" class="form-control" id="feature_value" name="feature_value" placeholder="Enter feature value">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="add-button">&nbsp;</label>
                                <button id="add" class="btn btn-success form-control"><i class="fa fa-plus-square"></i> Add</button>
                            </div>



                <div class="form-group col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Feature Category</th>
                            <th>Feature</th>
                            <th>Value</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="features_table">
                        </tbody>

                    </table>
                </div>

                            <div class="col-md-12">

                                <div class="form-group right-float">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-plus mr-5"></i> Add Purpose
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div>
                </form>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_script'); ?>
    <script>
        $('#category').on('change', function () {
                var id = $(this).find(":selected").val();
                $.ajax({
                    type: "GET",
                    url: '<?php echo e(route('adminfiltersubcat')); ?>',
                    data: {id: id},
                    success: function (result) {
                        $('#subcategory').html(result);
                        console.log(result);
                    }
                })
        });
        </script>
    <script>
        $('#state').on('change', function(){
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '<?php echo e(route('adminfiltersubcity')); ?>',
                data: {id: id},
                success: function (result) {
                    $('#city').html(result);
                    console.log(result);
                }
            })
        });
    </script>
    <script>
        $('#city').on('change', function(){
            var id = $(this).find(":selected").val();
                $.ajax({
                    type: "GET",
                    url: '<?php echo e(route('adminfiltercityproject')); ?>',
                    data: {id: id},
                    success: function (result) {
                        $('#cityProject').html(result);
                        console.log(result);
                    }
                })
            });

    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#category_feature").change(function()
            {
                var id=$(this).val();
                $.ajax
                ({
                    type: "GET",
                    url: "<?php echo e(route('adminfeatures_category.list')); ?>",
                    data: {
                        id : id
                    },
                    success: function(html)
                    {
                        $("#feature").html(html);
                    },
                    error : function (error) {
                        console.log(error);
                    }
                });

            });

        });
        $('#add').on('click', function (event) {
            event.preventDefault();
            var category = $('#category_feature option:selected')
            var feature = $('#feature option:selected');

            var feature_value = $('#feature_value');

            if(category.val() === '' || feature_value.val() === ''){
                alert('Please enter the values.');
            }
            else {

                var featureID = $('#feature').val();

                var html = '<tr>\n' +
                    '  <td>' + category.text() + '</td>' +
                    '  <td>' + feature.text() + '</td> ' +
                    '  <td><input type="text" class="form-control" name="feature_value[' + featureID + ']"  value="' + feature_value.val() + '"></td> ' +
                    '  <td><a class="btn btn-warning deleteFromList"><i class="fa fa-trash"></i></a></td> ' +
                    '</tr>';


                $('#features_table').append(html);

                $('#feature_value').val('');
            }

        });



        $(document).on('click', '.deleteFromList', function(){
            $(this).closest('tr').remove();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
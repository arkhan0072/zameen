<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Auth::routes();
////// --------- ADMIN ROUTES ----- ///////
Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function() {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::get('/category', 'CategoryController@index')->name('category.index');
    ///resource routes///
    Route::resource('/category', 'CategoryController');
    Route::resource('/states', 'StateController');
    Route::resource('/city', 'CityController');
    Route::resource('/features', 'FeaturesController');
    Route::resource('/featuresCategory', 'FeatureCategoryController');
    Route::resource('/purpose', 'PurposeController');
    Route::resource('/project', 'ProjectController');
    Route::post('/projectphoto/{id}', 'AdminPhotoController@updateProjectPhoto')->name('projectphotoupdate');
    Route::post('/destroyProjectPhoto/{id}', 'AdminPhotoController@destroyProjectPhoto')->name('destroyProjectPhoto');
    Route::get('/features/restore/{feature}', 'FeaturesController@restore')->name('features.restore');
    Route::get('/ajaxfeatures', 'FeaturesController@featuresList')->name('features.list');
    Route::get('/ajaxfeaturescategory', 'FeatureCategoryController@featuresCategoryList')->name('featuresCategory.list');
    Route::get('/featuresCategory/restore/{feature}', 'FeatureCategoryController@restore')->name('featuresCategory.restore');
    Route::resource('/alisting', 'AdminListingController');
    Route::resource('/aphoto', 'AdminPhotoController');
    Route::get('/adminajaxsubcat','AdminListingController@subCat')->name('adminfiltersubcat');
    Route::get('/adminajaxsubcity','ProjectController@subCity')->name('adminfiltersubcity');
    Route::get('/adminajaxcityproject','AdminListingController@cityProject')->name('adminfiltercityproject');
    Route::get('/adminfeatures-categories', 'AdminListingController@features_category')->name('adminfeatures_category.list');
});
///ADMIN ROUTE ENDS
Route::get('/','HomeController@index')->name('user.index');
Route::get('/properety/{slug}.hmtl','HomeController@single')->name('viewSingleListing');
Route::get('/ajaxfilter','HomeController@filter')->name('filters');
Route::get('/submitProperty','ListingController@create')->name('propertysubmit');
Route::get('/view','HomeController@view')->name('gridview');
Route::get('/view/search','HomeController@search')->name('view.search');
Route::get('/view/search/advanced','HomeController@advancedSearch')->name('advanced.search');
Route::get('/ajaxsubcat','HomeController@subCat')->name('filtersubcat');


//User Account

Route::group(['prefix' => 'users', 'middleware' => ['role:user|agent|agency']], function() {
    Route::get('/', 'UsersAccountController@myProfile')->name('user.profile');
    Route::resource('/listing', 'ListingController');
    Route::resource('/photo', 'PhotoController');
    Route::get('/submitProperty','ListingController@create')->name('propertysubmit');
    Route::get('/ajaxsubcat','ListingController@subCat')->name('filtersubcat');
    Route::get('/ajaxsubcity','ProjectController@subCity')->name('filtersubcity');
    Route::get('/ajaxcityproject','ListingController@cityProject')->name('filtercityproject');
    Route::get('/features-categories', 'ListingController@features_category')->name('features_category.list');
    Route::post('/update/my-profile/{id}', 'UsersAccountController@updateMyProfile')->name('user.updateProfile');
    Route::get('/social-media', 'UsersAccountController@socialMedia')->name('user.smedia');
    Route::post('/update/social-media', 'UsersAccountController@updateSocialMedia')->name('user.updateSmedia');
    Route::get('/properties', 'UsersAccountController@myProperties')->name('user.properties');
    Route::get('/favorited-properties', 'UsersAccountController@favoritedProperties')->name('user.fproperties');
    Route::get('/message', 'UsersAccountController@Message')->name('user.message');
    Route::get('/feedback-and-comments', 'UsersAccountController@feedbackAndComments')->name('user.feedback');
    Route::get('/payments-and-invoice', 'UsersAccountController@paymentsAndInvoice')->name('user.payments');
    Route::get('/change-password', 'UsersAccountController@changePassword')->name('user.changepassword');
    Route::post('/change-password/{id}', 'UsersAccountController@updatePassword')->name('user.updatePassword');
});



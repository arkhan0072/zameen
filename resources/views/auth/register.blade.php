@extends('indexlayout.master')
@section('content')
    <section id="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner_area">
                        <h3 class="page_title">Registration</h3>
                        {{--<div class="page_location">--}}
                            {{--<a href="index_1.html">Home</a>--}}
                            {{--<i class="fa fa-angle-right" aria-hidden="true"></i>--}}
                            {{--<span>Registration</span>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="reg_popup">
        <!-- Modal -->
        <div id="myModal">
            <div class="modal-dialog toggle_area" role="document">
                <div class="modal-header toggle_header">
                    <h4 class="inner-title">Create An Account</h4>
                </div>
                <div class="modal-body">
                        <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="signup_option">
                               <select class="selectpicker form-control"  data-live-search="true" name="is_agent" id="agency">
                                   <option value="" readonly="readonly">Signup As a*</option>
                                   <option value="2">Agent</option>
                                   <option value="3">General User</option>
                                   <option value="4">Agency</option>
                                   <option value="5">Partner</option>
                               </select>
                                <div class="form-group {{ $errors->has('first_name') ? ' is-invalid' : '' }}">
                                    <input id="fname" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus placeholder="First Name">
                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('last_name') ? ' is-invalid' : '' }}">
                                    <input id="lname" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus placeholder="Last Name">
                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                    <div class="form-group{{ $errors->has('mobile') ? ' is-invalid' : '' }}">
                                        <input id="number" type="number" class="form-control" name="mobile" value="{{ old('mobile') }}" required placeholder="Enter Your Mobile">
                                        @if ($errors->has('mobile'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('email') ? ' is-invalid' : '' }}">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Enter Your Email">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div id="agency_result">

                                    </div>
                                <br>
                                    <div class="form-group {{ $errors->has('password') ? ' is-invalid' : '' }}">
                                        <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                                    </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-default">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </section>
@endsection
@section('page_level_script')

<script>
    $(document).ready(function(){
        $("#agency").on('change',function()
        {
            var value=$(this).val();
            if(value==2) {
                $("#agency_result").html(' <div class="form-group {{ $errors->has('agency_name') ? ' is-invalid' : '' }}"> \n' +
                    '                                    <input id="aname" type="text" class="form-control" name="agency_name" value="{{ old('agency_name') }}" required autofocus placeholder="Agency Name">\n' +
                    '                                    @if ($errors->has('agency_name'))\n' +
                    '                                        <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('agency_name') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                    @endif\n' +
                    '                                </div>\n' +
                    '                                <div class="form-group {{ $errors->has('description') ? ' is-invalid' : '' }}">\n' +
                    '                                    <textarea id="bname" type="text" class="form-control" name="description" value="{{ old('description') }}" required autofocus placeholder="Description"></textarea>\n' +
                    '                                    @if ($errors->has('last_name'))\n' +
                    '                                        <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('last_name') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                    @endif\n' +
                    '                                </div>\n' +
                    '                                    <div class="form-group{{ $errors->has('number') ? ' is-invalid' : '' }}">\n' +
                    '                                        <input id="number" type="number" class="form-control" name="number" value="{{ old('number') }}" required placeholder="Agency Number">\n' +
                    '                                        @if ($errors->has('number'))\n' +
                    '                                            <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('number') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                        @endif \n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group{{ $errors->has('fax') ? ' is-invalid' : '' }}">\n' +
                    '                                        <input id="faxnumber" type="number" class="form-control" name="fax" value="{{ old('fax') }}" required placeholder="Agency Fax Number">\n' +
                    '                                        @if ($errors->has('fax'))\n' +
                    '                                            <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('fax') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                        @endif \n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group{{ $errors->has('address') ? 'is-invalid' : '' }}">\n' +
                    '                                        <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required placeholder="Agency Address">\n' +
                    '                                        @if ($errors->has('address'))\n' +
                    '                                            <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('address') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                        @endif\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group{{ $errors->has('agency_email') ? 'is-invalid' : '' }}">\n' +
                    '                                        <input id="agency_email" type="email" class="form-control" name="agency_email" value="{{ old('agency_email') }}" required placeholder="Agency Email Address">\n' +
                    '                                        @if ($errors->has('agency_email'))\n' +
                    '                                            <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('agency_email') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                        @endif\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group{{ $errors->has('logo') ? 'is-invalid' : '' }} browse_submit">\n' +
                    '                                       <input type="file" name="logo" id="fileupload-example-1"/>\n' +
                    '                                 <label class="fileupload-example-label" for="fileupload-example-1">Drop your photos here or Click</label>\n' +
                    '                                      @if ($errors->has('logo'))\n' +
                    '                                            <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('logo') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                        @endif\n' +
                    '                                    </div>');
            }else if(value==4){
                $("#agency_result").html(' <div class="form-group {{ $errors->has('agency_name') ? ' is-invalid' : '' }}"> \n' +
                    '                                    <input id="aname" type="text" class="form-control" name="agency_name" value="{{ old('agency_name') }}" required autofocus placeholder="Agency Name">\n' +
                    '                                    @if ($errors->has('agency_name'))\n' +
                    '                                        <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('agency_name') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                    @endif\n' +
                    '                                </div>\n' +
                    '                                <div class="form-group {{ $errors->has('description') ? ' is-invalid' : '' }}">\n' +
                    '                                    <textarea id="bname" type="text" class="form-control" name="description" value="{{ old('description') }}" required autofocus placeholder="Description"></textarea>\n' +
                    '                                    @if ($errors->has('last_name'))\n' +
                    '                                        <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('last_name') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                    @endif\n' +
                    '                                </div>\n' +
                    '                                    <div class="form-group{{ $errors->has('number') ? ' is-invalid' : '' }}">\n' +
                    '                                        <input id="number" type="number" class="form-control" name="number" value="{{ old('number') }}" required placeholder="Agency Number">\n' +
                    '                                        @if ($errors->has('number'))\n' +
                    '                                            <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('number') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                        @endif \n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group{{ $errors->has('fax') ? ' is-invalid' : '' }}">\n' +
                    '                                        <input id="faxnumber" type="number" class="form-control" name="fax" value="{{ old('fax') }}" required placeholder="Agency Fax Number">\n' +
                    '                                        @if ($errors->has('fax'))\n' +
                    '                                            <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('fax') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                        @endif \n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group{{ $errors->has('address') ? 'is-invalid' : '' }}">\n' +
                    '                                        <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required placeholder="Agency Address">\n' +
                    '                                        @if ($errors->has('address'))\n' +
                    '                                            <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('address') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                        @endif\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group{{ $errors->has('agency_email') ? 'is-invalid' : '' }}">\n' +
                    '                                        <input id="agency_email" type="email" class="form-control" name="agency_email" value="{{ old('agency_email') }}" required placeholder="Agency Email Address">\n' +
                    '                                        @if ($errors->has('agency_email'))\n' +
                    '                                            <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('agency_email') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                        @endif\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group{{ $errors->has('logo') ? 'is-invalid' : '' }} browse_submit">\n' +
                    '                                       <input type="file" name="logo" id="fileupload-example-1"/>\n' +
                    '                                 <label class="fileupload-example-label" for="fileupload-example-1"> Upload Your Logo </label>\n' +
                    '                                      @if ($errors->has('logo'))\n' +
                    '                                            <span class="invalid-feedback" role="alert">\n' +
                    '                                        <strong>{{ $errors->first('logo') }}</strong>\n' +
                    '                                    </span>\n' +
                    '                                        @endif\n' +
                    '                                    </div>');
            }else{
                $("#agency_result").hide();
            }
         });
    });
</script>
@endsection
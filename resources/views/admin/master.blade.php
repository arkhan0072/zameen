﻿<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from crypto-admin-templates.multipurposethemes.com/src2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Aug 2018 10:28:01 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://crypto-admin-templates.multipurposethemes.com/images/favicon.ico">

    <title>Crypto Admin - Dashboard</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="{{asset('crypto/assets/vendor_components/bootstrap/dist/css/bootstrap.css')}}">
    
	<!--amcharts -->
	<link href="../../www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />
	
	<!-- Bootstrap-extend -->
	<link rel="stylesheet" href="{{asset('crypto/css/bootstrap-extend.css')}}">
	
	<!-- theme style -->
	<link rel="stylesheet" href="{{asset('crypto/css/master_style.css')}}">
	
	<!-- Crypto_Admin skins -->
	<link rel="stylesheet" href="{{asset('crypto/css/skins/_all-skins.css')}}">
	<link rel="stylesheet" href="{{asset('crypto/assets/vendor_components/gallery/css/animated-masonry-gallery.css')}}">
	<link rel="stylesheet" href="{{asset('crypto/assets/vendor_components/lightbox-master/dist/ekko-lightbox.css')}}">


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


  </head>

<body class="hold-transition skin-yellow sidebar-mini">
<div class="wrapper">

  <!--add header here-->
  @include('admin.includes/header')
  <!--end header here-->
  
  <!-- Left side column. contains the logo and sidebar -->
  @include('admin.includes/sidebar')

  <!-- Content Wrapper. Contains page content -->
  @yield('content')

  <!-- /.content-wrapper -->
  
  @include('admin.includes/footer')
</div>
<!-- ./wrapper -->
  	
	 
	  
	<!-- jQuery 3 -->
	<script src="{{asset('crypto/assets/vendor_components/jquery/dist/jquery.js')}}"></script>
	
	<!-- popper -->
	<script src="{{asset('crypto/assets/vendor_components/popper/dist/popper.min.js')}}"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="{{asset('crypto/assets/vendor_components/bootstrap/dist/js/bootstrap.js')}}"></script>
	
	<!-- Slimscroll -->
	<script src="{{asset('crypto/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
	
	<!-- FastClick -->
	<script src="{{asset('crypto/assets/vendor_components/fastclick/lib/fastclick.js')}}"></script>
	
{{--   amcharts charts-- }}
{{--	<script src="../../www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>--}}
{{--	<script src="../../www.amcharts.com/lib/3/gauge.js" type="text/javascript"></script>--}}
{{--	<script src="../../www.amcharts.com/lib/3/serial.js'" type="text/javascript"></script>--}}
{{--	<script src="../../www.amcharts.com/lib/3/amstock.js" type="text/javascript"></script>--}}
{{--	<script src="../../www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>--}}
{{--	<script src="../../www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>--}}
{{--	<script src="../../www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>--}}
{{--	<script src="../../www.amcharts.com/lib/3/themes/patterns.js" type="text/javascript"></script>--}}
{{--	<script src="../../www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>	--}}
{{--	 -->--}}
	<!-- webticker -->
	<script src="{{asset('crypto/assets/vendor_components/Web-Ticker-master/jquery.webticker.min.js')}}"></script>


	<!-- EChartJS JavaScript -->
	<script src="{{asset('crypto/assets/vendor_components/echarts-master/dist/echarts-en.min.js')}}"></script>
	<script src="{{asset('crypto/assets/vendor_components/echarts-liquidfill-master/dist/echarts-liquidfill.min.js')}}"></script>
	
	<!-- This is data table -->
    <script src="{{asset('crypto/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js')}}"></script>
	
	<!-- Sparkline -->
	<script src="{{asset('crypto/assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
	
	<!-- Crypto_Admin App -->
	<script src="{{asset('crypto/js/template.js')}}"></script>
	
	<!-- Crypto_Admin dashboard demo (This is only for demo purposes) -->
	<script src="{{asset('crypto/js/pages/dashboard.js')}}"></script>
	<!--<script src="js/pages/dashboard-chart.js"></script>-->
	
	<!-- Crypto_Admin for demo purposes -->
	<script src="{{asset('crypto/js/demo.js')}}"></script>
<script src="{{asset('crypto/assets/vendor_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('crypto/assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- This is data table -->
<script src="{{asset('crypto/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js')}}"></script>

<!-- start - This is for export functionality only -->
<script src="{{asset('crypto/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('crypto/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('crypto/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js')}}"></script>
<script src="{{asset('crypto/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js')}}"></script>
<script src="{{asset('crypto/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js')}}"></script>
<script src="{{asset('crypto/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('crypto/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('crypto/assets/vendor_components/lightbox-master/dist/ekko-lightbox.js')}}"></script>
<!-- end - This is for export functionality only -->
<script src="{{asset('crypto/js/ckeditor.js')}}"></script>
<!-- Crypto_Admin for Data Table -->
<script src="{{asset('crypto/js/pages/data-table.js')}}"></script>
<script src="{{asset('crypto/js/pages/gallery.js')}}"></script>
@yield('page_level_script')

</body>
</html>

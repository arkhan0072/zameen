@extends('admin.master')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
    {{--<h1>--}}
    {{--Add New Category--}}
    {{--</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
    {{--<li class="breadcrumb-item"><a href="#">Category</a></li>--}}
    {{--<li class="breadcrumb-item active">New</li>--}}
    {{--</ol>--}}
    {{--</section>--}}

    <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Project</h3>
                </div>
                <!-- /.box-header -->
                <form role="form" method="post" action="{{route('project.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('title') ? 'has-error': ''}}">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" placeholder="Title"
                                           required="required" value="">
                                </div>
                            </div>
                            @if($errors->has('title'))
                                <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif

                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('country') ? 'has-error': ''}}">
                                    <label>Country</label>
                                    <select class="form-control" name="country">
                                        <option value="">Please Select Coutnry</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->country_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if($errors->has('country'))
                                <span class="help-block">
                            <strong>{{ $errors->first('country') }}</strong>
                                </span>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('state') ? 'has-error': ''}}">
                                    <label>State</label>
                                    <select class="form-control" id="state" name="state">
                                        <option value="">Please Select State</option>
                                        @foreach($states as $sat)
                                            <option value="{{$sat->id}}">{{$sat->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if($errors->has('state'))
                                <span class="help-block">
                            <strong>{{ $errors->first('state') }}</strong>
                                </span>
                            @endif

                            <div class="col-md-6" id="subcity">
                                <div class="form-group {{ $errors->has('city') ? 'has-error': ''}}">
                                    <label>City</label>
                                    <select class="form-control" id="city" name="city">

                                    </select>
                                </div>
                            </div>
                            @if($errors->has('city'))
                                <span class="help-block">
                            <strong>{{ $errors->first('city') }}</strong>
                                </span>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('address') ? 'has-error': ''}}">
                                    <label>Address</label>
                                    <input type="text" class="form-control" name="address" placeholder="Enter Your Property Address">
                                </div>
                            </div>
                            @if($errors->has('address'))
                                <span class="help-block">
                            <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('zipcode') ? 'has-error': ''}}">
                                    <label>ZipCode</label>
                                    <input type="text" class="form-control" name="zipcode" placeholder="Enter Your area Zipcode">
                                </div>
                            </div>
                            @if($errors->has('zipcode'))
                                <span class="help-block">
                            <strong>{{ $errors->first('zipcode') }}</strong>
                                </span>
                            @endif
                            <br>
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('description') ? 'has-error': ''}}">
                                    <label>Description</label>
                                    <textarea id="messageArea" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message..">

                                    </textarea>
                                </div>
                            </div>
                            @if($errors->has('description'))
                                <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('price') ? 'has-error': ''}}">
                                    <label>Price</label>
                                    <input type="text" class="form-control" name="price" placeholder="Price"
                                           required="required" value="">
                                </div>
                            </div>
                            @if($errors->has('price'))
                                <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('area') ? 'has-error': ''}}">
                                    <label>Area</label>
                                    <input type="text" class="form-control" name="area" placeholder="area"
                                           required="required" value="">
                                </div>
                            </div>
                            @if($errors->has('area'))
                                <span class="help-block">
                            <strong>{{ $errors->first('area') }}</strong>
                                </span>
                            @endif

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('area') ? 'has-error': ''}}">
                                    <label>Unit</label>
                                    <select class="form-control" name="areaType">
                                        <option value="Square Feet">Square Feet</option>
                                        <option value="Square Yards">Square Yards</option>
                                        <option value="Square Meters">Square Meters</option>
                                        <option value="Marla">Marla</option>
                                        <option value="Kanal">Kanal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('picture') ? 'has-error': ''}}">
                                    <label>Picture</label>
                                    <input type="file" class="form-control" name="picture[]" multiple required>
                                </div>
                            </div>
                            @if($errors->has('picture'))
                                <span class="help-block">
                            <strong>{{ $errors->first('picture') }}</strong>
                                </span>
                            @endif
                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Features Category:</label>
                                <select id="category_feature" class="form-control" name="feature_category">
                                    <option value="">Select Features Category</option>
                                    @foreach($feature_categories as $feature_category)
                                        <option value="{{$feature_category->id}}">{{$feature_category->name}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Features:</label>
                                <select id="feature" class="form-control" name="feature">

                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="feature_value">Value&nbsp;</label>
                                <input type="text" class="form-control" id="feature_value" name="feature_value" placeholder="Enter feature value">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="add-button">&nbsp;</label>
                                <button id="add" class="btn btn-success form-control"><i class="fa fa-plus-square"></i> Add</button>
                            </div>



                            <div class="form-group col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Feature Category</th>
                                        <th>Feature</th>
                                        <th>Value</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="features_table">
                                    </tbody>

                                </table>
                            </div>

                            <div class="col-md-12">

                                <div class="form-group right-float">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-plus mr-5"></i> Add Project
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div>
                </form>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@stop
@section('page_level_script')
    <script>
        $('#category').on('change', function () {
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '{{route('adminfiltersubcat')}}',
                data: {id: id},
                success: function (result) {
                    $('#subcategory').html(result);
                    console.log(result);
                }
            })
        });
    </script>
    <script>
        $('#state').on('change', function(){
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '{{route('adminfiltersubcity')}}',
                data: {id: id},
                success: function (result) {
                    $('#city').html(result);
                    console.log(result);
                }
            })
        });
    </script>
    <script>
        $('#city').on('change', function(){
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '{{route('adminfiltercityproject')}}',
                data: {id: id},
                success: function (result) {
                    $('#cityProject').html(result);
                    console.log(result);
                }
            })
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#category_feature").change(function()
            {
                var id=$(this).val();
                $.ajax
                ({
                    type: "GET",
                    url: "{{route('adminfeatures_category.list')}}",
                    data: {
                        id : id
                    },
                    success: function(html)
                    {
                        $("#feature").html(html);
                    },
                    error : function (error) {
                        console.log(error);
                    }
                });

            });

        });
        $('#add').on('click', function (event) {
            event.preventDefault();
            var category = $('#category_feature option:selected')
            var feature = $('#feature option:selected');

            var feature_value = $('#feature_value');

            if(category.val() === '' || feature_value.val() === ''){
                alert('Please enter the values.');
            }
            else {

                var featureID = $('#feature').val();

                var html = '<tr>\n' +
                    '  <td>' + category.text() + '</td>' +
                    '  <td>' + feature.text() + '</td> ' +
                    '  <td><input type="text" class="form-control" name="feature_value[' + featureID + ']"  value="' + feature_value.val() + '"></td> ' +
                    '  <td><a class="btn btn-warning deleteFromList"><i class="fa fa-trash"></i></a></td> ' +
                    '</tr>';


                $('#features_table').append(html);

                $('#feature_value').val('');
            }

        });



        $(document).on('click', '.deleteFromList', function(){
            $(this).closest('tr').remove();
        });
    </script>
@endsection

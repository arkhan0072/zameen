@extends('admin.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
    {{--<h1>--}}
    {{--Add New Category--}}
    {{--</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
    {{--<li class="breadcrumb-item"><a href="#">Category</a></li>--}}
    {{--<li class="breadcrumb-item active">New</li>--}}
    {{--</ol>--}}
    {{--</section>--}}

    <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Listings</h3>
                </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>

                                    @foreach($projects as $key=>$project)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$project->title}}</td>
                                            <td><a href="{{ route('project.edit', $project->id) }}"><button class="btn btn-primary">Edit</button></a></td>
                                            <td><input type="submit" class="btn btn-danger" onclick="frmdlt{{$project->id}}.submit();" value="Delete">
                                                <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="frmdlt{{$project->id}}" action="{{ route('project.destroy', $project->id)}}" method="post">
                                                    {!! method_field('delete') !!}
                                                    {{csrf_field()}}

                                                </form></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
            </div>
        </section>
                        </div>
                        <!-- /.box-body -->


@endsection
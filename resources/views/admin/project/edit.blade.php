@extends('admin.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
    {{--<h1>--}}
    {{--Add New Category--}}
    {{--</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
    {{--<li class="breadcrumb-item"><a href="#">Category</a></li>--}}
    {{--<li class="breadcrumb-item active">New</li>--}}
    {{--</ol>--}}
    {{--</section>--}}

    <!-- Main content -->
        <section class="content">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
        @endforeach
        <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Project</h3>
                </div>
                <!-- /.box-header -->
                <form role="form" method="post" action="{{route('project.update', $project->id)}}" enctype="multipart/form-data">
                    @csrf
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                    @method('PUT')
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('title') ? 'has-error': ''}}">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" placeholder="Title"
                                           required="required" value="{{old('title',$project->title)}}">
                                </div>
                            </div>
                            @if($errors->has('title'))
                                <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('country') ? 'has-error': ''}}">
                                    <label>Country</label>
                                    <select class="form-control" name="country">
                                        <option value="">Please Select Coutnry</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" selected>{{$country->country_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if($errors->has('country'))
                                <span class="help-block">
                            <strong>{{ $errors->first('country') }}</strong>
                                </span>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('state') ? 'has-error': ''}}">
                                    <label>State</label>
                                    <select class="form-control" id="state" name="state">
                                        <option value="">Please Select State</option>
                                        @foreach($states as $sat)
                                            @if($sat->id == $project->city->state->id)
                                                <option value="{{$sat->id}}" selected>{{$sat->name}}</option>
                                            @else
                                                <option value="{{$sat->id}}">{{$sat->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if($errors->has('state'))
                                <span class="help-block">
                            <strong>{{ $errors->first('state') }}</strong>
                                </span>
                            @endif

                            <div class="col-md-6" id="subcity">
                                <div class="form-group{{ $errors->has('city') ? 'has-error': ''}}">
                                    <label>City</label>
                                    <select class="form-control" id="city" name="city">
                                        @foreach($cities as $city)
                                            @if($city->id == $project->city_id)
                                                <option value="{{$city->id}}" selected>{{$city->name}}</option>
                                            @else
                                                <option value="{{$city->id}}">{{$city->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if($errors->has('city'))
                                <span class="help-block">
                            <strong>{{ $errors->first('city') }}</strong>
                                </span>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('address') ? 'has-error': ''}}">
                                    <label>Address</label>
                                    <input type="text" class="form-control" name="address" value="{{$project->address}}" placeholder="Enter Your Property Address">
                                </div>
                            </div>
                            @if($errors->has('address'))
                                <span class="help-block">
                            <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('zipcode') ? 'has-error': ''}}">
                                    <label>ZipCode</label>
                                    <input type="text" class="form-control" name="zipcode" value="{{$project->zipcode}}" placeholder="Enter Your area Zipcode">
                                </div>
                            </div>
                            @if($errors->has('zipcode'))
                                <span class="help-block">
                            <strong>{{ $errors->first('zipcode') }}</strong>
                                </span>
                            @endif
                            <br>
                            <div class="col-md-12">
                                <div class="form-group{{ $errors->has('description') ? 'has-error': ''}}">
                                    <label>Description</label>
                                    <textarea id="messageArea" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message..">
                                        {{$project->description}}
                                    </textarea>
                                </div>
                            </div>
                            @if($errors->has('description'))
                                <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('price') ? 'has-error': ''}}">
                                    <label>Price</label>
                                    <input type="text" class="form-control" name="price" placeholder="Purpose"
                                           required="required" value="{{old('price',$project->price)}}">
                                </div>
                            </div>
                            @if($errors->has('price'))
                                <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                            &nbsp;
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('price') ? 'has-error': ''}}">
                                    <label>Picture</label>
                                    <input type="file" class="form-control" name="picture_project[]" required="required" multiple>
                                </div>
                            </div>
                            @if($errors->has('price'))
                                <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                            &nbsp;
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('area') ? 'has-error': ''}}">
                                    <label>Area</label>
                                    <input type="text" class="form-control" name="area" placeholder="Area"
                                           required="required" value="{{$project->area}}">
                                </div>
                            </div>
                            @if($errors->has('area'))
                                <span class="help-block">
                            <strong>{{ $errors->first('area') }}</strong>
                                </span>
                            @endif

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('area') ? 'has-error': ''}}">
                                    <label>Unit</label>
                                    <select class="form-control" name="areaType">

                                        <option value="Square Feet">Square Feet</option>
                                        <option value="Square Yards">Square Yards</option>
                                        <option value="Square Meters">Square Meters</option>
                                        <option value="Marla">Marla</option>
                                        <option value="Kanal">Kanal</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Features Category:</label>
                                <select id="category_feature" class="form-control" name="feature_category">
                                    <option value="">Select Features Category</option>
                                    @foreach($feature_categories as $feature_category)
                                        <option value="{{$feature_category->id}}">{{$feature_category->name}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Features:</label>
                                <select id="feature" class="form-control" name="feature">

                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="feature_value">Value&nbsp;</label>
                                <input type="text" class="form-control" id="feature_value" name="feature_value" placeholder="Enter feature value">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="add-button">&nbsp;</label>
                                <button id="add" class="btn btn-success form-control"><i class="fa fa-plus-square"></i> Add</button>
                            </div>



                            <div class="form-group col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Feature Category</th>
                                        <th>Feature</th>
                                        <th>Value</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="features_table">
                                    @foreach($project->features as $feature)
                                        <tr class="deleteFromList">
                                            <td>{{$feature->category->name}}</td>
                                            <td>{{$feature->name}}</td>
                                            <td><input type="text" class="form-control" name="feature_value[{{$feature->id}}]"  value="{{$feature->pivot->feature_value}}"></td>
                                            <td><a class="btn btn-warning deleteFromList"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>&nbsp;
                            <div class="col-md-12">

                                <div class="form-group right-float">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-plus mr-5"></i> Update Project
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div>
                </form>
                <section class="content col-md-12">
                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header bg-dark">
                            <h4 class="box-title">Picture Gallery</h4>
                            <div class="box-controls pull-right">
                                <div class="pull-right btn-group">
                                    <button class="btn btn-info gallery-header-center-right-links-current" id="filter-all">All</button>
                                    <button class="btn btn-info gallery-header-center-right-links-current" id="filter-studio">Studio</button>
                                    <button class="btn btn-info gallery-header-center-right-links-current" id="filter-landscape">Landscapes</button>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">
                                        <div class="row">
                                            @foreach($project->photos as $key=>$project)
                                                <div class="col-md-4">
                                                    <div class="pull-right">
                                                        <a title="Edit" class="btn btn-info" data-id="{{$project->id}}" data-toggle="modal" data-target="#myModal{{$project->id}}"><i class="fa fa-edit "></i></a>
                                                        <a title="Delete" class="btn btn-danger" onclick="delphoto{{$project->id}}.submit();"><i class="fa fa-trash "></i></a>
                                                        <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="delphoto{{$project->id}}" action="{{route('destroyProjectPhoto',$project->id)}}" method="post">
                                                            {{csrf_field()}}
                                                        </form>
                                                    </div>
                                                    <div class="p-3">
                                                        <div id="myModal{{$project->id}}" class="modal fade" role="dialog">
                                                            <form action="{{route('projectphotoupdate', $project->id)}}" method="post" enctype="multipart/form-data">
                                                                @csrf

                                                                <div class="modal-dialog">

                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            <h4 class="modal-title">Modal Header</h4>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                            <input type="file" class="form-control" name="picture_project">
                                                                            <button type="submit" name="upload" data-id="{{$project->id}}" id="editpicture">Edit Picture</button>

                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </form>
                                                        </div>
                                                        <a href="{{asset('uploads/listings')}}/{{$project->name}}" data-toggle="lightbox" id="lightbox" data-gallery="multiimages" data-title="Image title will be apear here" ><img src="{{asset('uploads/project_pictures')}}/{{$project->name}}" alt="gallery" class="all studio" /> </a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </section>

                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@stop
@section('page_level_script')
    <script>
        $('#category').on('change',function () {
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '{{route('adminfiltersubcat')}}',
                data: {id: id},
                success: function (result) {
                    $('#subcategory').html(result);
                    console.log(result);
                }
            })
        });
    </script>
    <script>
        $('#state').on('change', function(){
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '{{route('adminfiltersubcity')}}',
                data: {id: id},
                success: function (result) {
                    $('#city').html(result);
                    console.log(result);
                }
            })
        });
    </script>
    <script>
        $('#city').on('change', function(){
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '{{route('adminfiltercityproject')}}',
                data: {id: id},
                success: function (result) {
                    $('#cityProject').html(result);
                    console.log(result);
                }
            })
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#category_feature").change(function()
            {
                var id=$(this).val();
                $.ajax
                ({
                    type: "GET",
                    url: "{{route('adminfeatures_category.list')}}",
                    data: {
                        id : id
                    },
                    success: function(html)
                    {
                        $("#feature").html(html);
                    },
                    error : function (error) {
                        console.log(error);
                    }
                });

            });

        });
        $('#add').on('click', function (event) {
            event.preventDefault();
            var category = $('#category_feature option:selected')
            var feature = $('#feature option:selected');

            var feature_value = $('#feature_value');

            if(category.val() === '' || feature_value.val() === ''){
                alert('Please enter the values.');
            }
            else {

                var featureID = $('#feature').val();

                var html = '<tr>\n' +
                    '  <td>' + category.text() + '</td>' +
                    '  <td>' + feature.text() + '</td> ' +
                    '  <td><input type="text" class="form-control" name="feature_value[' + featureID + ']"  value="' + feature_value.val() + '"></td> ' +
                    '  <td><a class="btn btn-warning deleteFromList"><i class="fa fa-trash"></i></a></td> ' +
                    '</tr>';


                $('#features_table').append(html);

                $('#feature_value').val('');
            }

        });



        $(document).on('click', '.deleteFromList', function(){
            $(this).closest('tr').remove();
        });
        $(document).on('click','editpicture',function(event){
            event.preventDefault();
        });
    </script>
    {{--<script>--}}
    {{--$('#editpic').on('click',function () {--}}
    {{--var id =$(this).data('id');--}}
    {{--alert('good');--}}
    {{--url = "{{route('photo.update', ':id')}}";--}}
    {{--$.ajax--}}
    {{--({--}}
    {{--type: "GET",--}}
    {{--url: url.replace(':id', id),--}}
    {{--data: {--}}
    {{--id : id,--}}
    {{--_token: "{{@csrf_token()}}"--}}
    {{--},--}}
    {{--success: function(html)--}}
    {{--{--}}
    {{--$("#feature").html(html);--}}
    {{--},--}}
    {{--error : function (error) {--}}
    {{--console.log(error);--}}
    {{--}--}}
    {{--});--}}
    {{--});--}}
    {{--</script>--}}
@endsection

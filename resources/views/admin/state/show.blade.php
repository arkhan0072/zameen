@extends('admin.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">State -> {{$cat}}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Sr No#</th>
                                        <th>City Name</th>
                                        <th>View Projects</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>

                                    @foreach($cities as $key=>$cat)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$cat->name}}</td>
                                            <td><a href="{{ route('city.show', $cat->slug)}}"><button class="btn btn-primary">View Projects</button></a></td>
                                            <td><a href="{{ route('city.edit', $cat->id)}}"><button class="btn btn-primary">Edit</button></a></td>
                                            <td><input type="submit" class="btn btn-danger" onclick="frmdlt{{$cat->id}}.submit();" value="Delete">
                                                <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="frmdlt{{$cat->id}}" action="{{ route('city.destroy', $cat->id)}}" method="post">
                                                    {!! method_field('delete') !!}
                                                    {{csrf_field()}}

                                                </form></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
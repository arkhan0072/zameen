@extends('admin.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
    {{--<h1>--}}
    {{--Add New Category--}}
    {{--</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
    {{--<li class="breadcrumb-item"><a href="#">Category</a></li>--}}
    {{--<li class="breadcrumb-item active">New</li>--}}
    {{--</ol>--}}
    {{--</section>--}}

    <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">States</h3>
                </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>ID</th>
                                        <th>State Name</th>
                                        <th>View Cities</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>

                                    @foreach($states as $key=>$cat)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$cat->name}}</td>
                                            <td><a href="{{ route('states.show', $cat->slug) }}"><button class="btn btn-primary">View Cities</button></a></td>
                                            <td><a href="{{ route('states.edit', $cat->id) }}"><button class="btn btn-primary">Edit</button></a></td>
                                            <td><input type="submit" class="btn btn-danger" onclick="frmdlt{{$cat->id}}.submit();" value="Delete">
                                                <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="frmdlt{{$cat->id}}" action="{{ route('states.destroy', $cat->id)}}" method="post">
                                                    {!! method_field('delete') !!}
                                                    {{csrf_field()}}

                                                </form></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
            </div>
        </section>
                        </div>

@endsection
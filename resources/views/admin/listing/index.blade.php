@extends('admin.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
    {{--<h1>--}}
    {{--Add New Category--}}
    {{--</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
    {{--<li class="breadcrumb-item"><a href="#">Category</a></li>--}}
    {{--<li class="breadcrumb-item active">New</li>--}}
    {{--</ol>--}}
    {{--</section>--}}

    <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Listings</h3>
                </div>

            </div>
                <div class="grid-container">
                    @foreach($listing as $key=>$list)
                    <div class="col-md-12 col-xl-12">
                        <div class="box pull-up">
                            <div class="box-body">
                                <div class="row align-center">
                                    <div class="col-3">
                                       <img src="{{asset('uploads/listings')}}/{{$list->photos[0]->name}}" class="img-thumb">
                                    </div>
                                    <div class="col-6">
                                        <h1>{{$list->title}}</h1>
                                        <h4>{{$list->project->title}},{{$list->project->city->name}}</h4>
                                        <h4>{{$list->price}}</h4>
                                        <p>{{strip_tags( str_limit($list->description,'170', ' ...' ))}}</p>
                                        <p>{{$list->area}}</p>
                                    </div>
                                    <div class="col-3">
                                        <a href="{{ route('alisting.edit', $list->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        <a onclick="frmdlt{{$list->id}}.submit();" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> Delete</a>
                                    </div>
                                    <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="frmdlt{{$list->id}}" action="{{ route('alisting.destroy', $list->id)}}" method="post">
            {{method_field('delete')}}
            {{csrf_field()}}

            </form>
</div>
</div>
</div>
</div>
@endforeach
                        {{ $listing->links() }}
</div>


        {{--<td>{{$key+1}}</td>--}}
                                            {{--<td></td>--}}
                                            {{--<td><a href="{{ route('adminlisting.edit', $list->id) }}"><button class="btn btn-primary">Edit</button></a></td>--}}
                                            {{--<td><input type="submit" class="btn btn-danger" onclick="frmdlt{{$list->id}}.submit();" value="Delete">--}}
                                                {{--<form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="frmdlt{{$list->id}}" action="{{ route('adminlisting.destroy', $list->id)}}" method="post">--}}
                                                    {{--{!! method_field('delete') !!}--}}
                                                    {{--{{csrf_field()}}--}}

                                                {{--</form></td>--}}
                                        {{--</tr>--}}


        </section>
                        </div>
                        <!-- /.box-body -->


@endsection
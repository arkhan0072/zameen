@extends('admin.master')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
    {{--<h1>--}}
    {{--Add New Category--}}
    {{--</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
    {{--<li class="breadcrumb-item"><a href="#">Category</a></li>--}}
    {{--<li class="breadcrumb-item active">New</li>--}}
    {{--</ol>--}}
    {{--</section>--}}

    <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Features</h3>
                </div>

        <div class="box-body">
            <table id="feature" class="display" style="width:100%">
              <thead>
                      <tr>
                          <th>Id</th>
                          <th>Category</th>
                          <th>Feature Name</th>
                          <th>Action</th>
                      </tr>
              </thead> 
              <tbody>
              </tbody>
              
            </table>
        </div>
</div>
        </section>
    </div>

@endsection

@section('page_level_script')
 <script type="text/javascript">
 // $(document).ready(function() {

        $('#feature').DataTable({
            dom: 'lBftip',
            buttons: ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5', 'print'],
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{route("features.list")}}',
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'category.name', name: 'category.name'},
                {data: 'name', name: 'name'},
                {data: 'action'},
            ],
                success:function (data) {
                    console.log(data);
                }
            });
                        
            $(document).on('click', '.feature_delete', function(){
              var e = $(this);
              var id = e.data('id');
              var url = "{{route('features.destroy', ':id')}}";
              $.ajax({
                  type: "POST",
                  url: url.replace(':id', id),
                  data: {
                        _method : 'DELETE',
                        _token : "{{@csrf_token()}}"
                  },
                  success: function(data) {
                        e.console.log('Restore');
                        e.addClass('btn-success feature_restore').removeClass('btn-danger feature_delete');
                  }
            });
                  });
            $(document).on('click', '.feature_restore', function(){
              var e = $(this);
              var id = $(this).data('id');
              var url = "{{route('features.restore', ':id')}}";
              $.ajax({
                  type: "GET",
                  url: url.replace(':id', id),
                  success: function(data) {
                        e.html('Delete');
                        e.addClass('btn-danger feature_delete').removeClass('btn-success feature_restore');
                  }
            });
                  });
 // });
  </script>
@endsection
@extends('admin.master')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
    {{--<h1>--}}
    {{--Add New Category--}}
    {{--</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
    {{--<li class="breadcrumb-item"><a href="#">Category</a></li>--}}
    {{--<li class="breadcrumb-item active">New</li>--}}
    {{--</ol>--}}
    {{--</section>--}}

    <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Category</h3>
                </div>
                <!-- /.box-header -->
                <form method="post" action="{{route('category.update',$category->id)}}">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="box-body">

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('name') ? 'has-error': ''}}">
                                    <label>Name</label>
                                    <small class="sidetitle">E.g. Home Office Plot</small>
                                    <input type="text" name="name" value="{{old('name',$category->name)}}" class="form-control" id="username" required>
                                </div>
                                @if($errors->has('name'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Parent Category</label>
                                    <select class="form-control" id="exampleFormControlSelect1" name="parent" required>
                                        <option value="" disabled="disabled">Select Category Type</option>
                                        <option value="0">Parent</option>
                                        @foreach($categories as $cat)
                                            @if($cat->id == $category->parent)
                                                <option value="{{$cat->id}}" selected="selected">{{$cat->name}} </option>
                                            @else
                                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group right-float">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-plus mr-5"></i> Add Category
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div>
                </form>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@section('page_level_style')
    <script type="text/javascript">
        $(document).ready(function() {
            // Initialize "states" example
            var $states = $(".js-source-states");
            var statesOptions = $states.html();
            $states.remove();

            $(".js-states").append(statesOptions);
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        function formatState (state) {
            if (!state.id) {
                return state.text;
            }

            var $state = $(
                '<span><i class="fa '+ state.element.dataset.icon.toLowerCase() +'" aria-hidden="true"></i> ' + state.text + '</span>'
            );
            return $state;
        };

        $(".js-example-templating").select2({
            templateResult: formatState
        }).addClass("form-control");
    </script>
@endsection
@stop

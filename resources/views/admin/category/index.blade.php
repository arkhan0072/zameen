@extends('admin.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Categories</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>ID</th>
                                        <th>NAME</th>
                                        <th>Parent</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    @foreach($categories as $key=>$cat)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$cat->name}}</td>
                                        <td>{{$cat->parent == 0 ? "Parent":''}}</td>

                                        <td><a href="{{ route('category.edit', $cat->id) }}"><button class="btn btn-primary">Edit</button></a></td>
                                        <td><input type="submit" class="btn btn-danger" onclick="frmdlt{{$cat->id}}.submit();" value="Delete">
                                            <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="frmdlt{{$cat->id}}" action="{{ route('category.destroy', $cat->id)}}" method="post">
                                                {!! method_field('delete') !!}
                                                {{csrf_field()}}

                                            </form></td>
                                    </tr>
                                        @foreach($cat->subCategories as $subCat)
                                            <tr>
                                                <td><ul><li></li></ul></td>
                                                <td>{{$subCat->name}}</td>
                                                <td>{{$cat->name}}</td>

                                                <td><a href="{{ route('category.edit', $subCat->id) }}"><button class="btn btn-primary">Edit</button></a></td>
                                                <td><input type="submit" class="btn btn-danger" onclick="frmdlt{{$subCat->id}}.submit();" value="Delete">
                                                    <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="frmdlt{{$subCat->id}}" action="{{ route('category.destroy', $subCat->id)}}" method="post">
                                                        {!! method_field('delete') !!}
                                                        {{csrf_field()}}

                                                    </form></td>
                                            </tr>
                                            @endforeach
                                        @endforeach
                                </table>
                            </div>
                            {{ $categories->links() }}
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
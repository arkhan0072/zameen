@extends('admin.master')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
    {{--<h1>--}}
    {{--Add New Category--}}
    {{--</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
    {{--<li class="breadcrumb-item"><a href="#">Category</a></li>--}}
    {{--<li class="breadcrumb-item active">New</li>--}}
    {{--</ol>--}}
    {{--</section>--}}

    <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Features</h3>
                </div>
          <form role="form" action="{{route('featuresCategory.store')}}" method="post"  enctype="multipart/form-data">
          @csrf
              <div class="box-body">
                  <div class="row">
                      <div class="col-md-6">
                      <div class="form-group {{ $errors->has('name') ? 'has-error': ''}}">
                          <label for="name">Feature:</label>
                          <input type="text" required="required" name="name" value="{{ old('name') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
                          @if($errors->has('name'))
                              <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                      <div class="col-md-12">
                  <div class="float-right">
                        <button type="submit" class="btn btn-primary" name="frmsubmit">Save</button>
                  </div>
                      </div>
              </div>
          </div>
        </form>
            </div>
        </section>
    </div>
@endsection
@extends('indexlayout.master')
@section('content')
<!-- Banner Section Start -->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner_area">
                    <h3 class="page_title">Social Media</h3>
                    <div class="page_location">
                        <a href="index_1.html">Home</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <a href="index_1.html">Pages</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <span>Social Media</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->
<!-- Profile Setting Start -->
<section id="profile_setting">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                @include('users.settings')
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <form class="profile_area" method="post" action="{{route('user.updateSmedia', Auth::id())}}">
                            @csrf
                            <div class="media_links">
                                <h4 class="inner-title">social media links</h4>
                                <p>Euismod Ac penatibus magna vel tempor, porttitor ullamcorper urna, massa natoque venenatis mollis libero neque velit risus, magnis vehicula nam.</p>
                                <div class="information_form">
                                    <div class="input-group  {{ $errors->has('facebook') ? ' is-invalid' : '' }}">
                                        <label class="profile_label">facebbok:</label>
                                        <input type="text" class="form-control" name="facebook" placeholder="Your Profile Link" value="{{Auth::user()->facebook}}" required>
                                        @if ($errors->has('facebook'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('facebook') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="input-group  {{ $errors->has('twitter') ? ' is-invalid' : '' }}">
                                        <label class="profile_label">twitter :</label>
                                        <input type="text" class="form-control setting_input" name="twitter" placeholder="Your Profile Link"  value="{{Auth::user()->twitter}}" required>
                                        @if ($errors->has('twitter'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('twitter') }}</strong>
                                    </span>
                                            @endif

                                    </div>
                                    <div class="input-group  {{ $errors->has('linked_in') ? ' is-invalid' : '' }}">
                                        <label class="profile_label">linked in :</label>
                                        <input type="text" class="form-control setting_input" name="linked_in" placeholder="Your Profile Link"  value="{{Auth::user()->linked_in}}" required>
                                        @if ($errors->has('linked_in'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('linked_in') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="input-group  {{ $errors->has('google_plus') ? ' is-invalid' : '' }}">
                                        <label class="profile_label">google plus :</label>
                                        <input type="text" class="form-control setting_input" name="google_plus" placeholder="Your Profile Link"  value="{{Auth::user()->google_plus}}" required>
                                        @if ($errors->has('google_plus'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('google_plus') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="input-group  {{ $errors->has('vimo') ? ' is-invalid' : '' }}">
                                        <label class="profile_label">vimeo :</label>
                                        <input type="text" class="form-control setting_input" name="vimo" placeholder="Your Profile Link"  value="{{Auth::user()->vimo}}" required>
                                        @if ($errors->has('vimo'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('vimo') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="save_change">
                                <button class="btn btn-default" type="submit">Save</button>
                                <p><span>Note : </span>Morbi nibh dis. Pede. Erat, porta urna. Adipiscing Ipsum nibh morbi taciti proin quisque sit quam curae; vulputate ridiculus. Dictumst ullamcorper nullam Parturient, urna. Etiam nascetur enim lectus torquent pellentesque.</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Profile Setting End -->
@endsection


@extends('indexlayout.singlemaster')
@section('content')
<!-- Banner Section Start -->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner_area">
                    <h3 class="page_title">Change password</h3>
                    <div class="page_location">
                        <a href="index_1.html">Home</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <a href="index_1.html">Pages</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <span>Change password</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->

<!-- Profile Setting Start -->
<section id="profile_setting">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                @include('users.settings')
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h4 class="inner-title">Change Your Password</h4>
                        <form class="submit_form" action="{{route('user.updatePassword', Auth::id())}}" method="post">
                            @csrf
                            <div class="change_password">
                                {{--<div class="form-group">--}}
                                    {{--<input type="password" name="current_password" class="form-control" placeholder="Current Password">--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="New Password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password_confirmation" class="form-control" placeholder="New Password Again" required>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input id="send" class="btn btn-default" value="Save Change" type="submit">
                                </div>
                                <div class="alert alert-warning">Password must be in 8 characters with minimum 1 special characters</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Profile Setting End -->
@endsection

@extends('indexlayout.singlemaster')
@section('content')
  <!-- Banner Section Start -->
  <section id="banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="banner_area">
            <h3 class="page_title">My Profile</h3>
            <div class="page_location">
              <a href="index_1.html">Home</a>
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <a href="index_1.html">Pages</a>
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <span>My Profile</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Banner Section End -->

  <!-- Profile Setting Start -->
  <section id="profile_setting">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-12">
          @include('users.settings')
        </div>
        <div class="col-md-8 col-sm-12">
          <div class="row">
            <form class="profile_area" method="post" action="{{route('user.updateProfile', $user->id)}}" enctype="multipart/form-data">
              @csrf
              @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
              @endforeach
              <div class="col-md-8 col-sm-12">


                {{--@method('PUT')--}}
                <div class="personal_infor">
                  <h4 class="inner-title">personal information</h4>
                  <p>Euismod Ac penatibus magna vel tempor, porttitor ullamcorper urna, massa natoque venenatis mollis libero neque velit risus, magnis vehicula nam.</p>
                  <div class="information_form">
                    <div class="row">
                      @if($user->hasRole('agent'))
                        <div class="col-md-12">
                          <label class="profile_label">Agent ID</label>
                          <input type="text" name="agentid" class="form-control" value="{{ str_limit($user->agency[0]->name, $limit = 1,$end="") }}{{$user->agency[0]->id}}{{ str_limit($user->first_name, $limit = 1,$end="") }}{{$user->id}}" disabled >
                        </div>
                      @elseif($user->hasRole('agency'))
                        <div class="col-md-12">
                          <label class="profile_label">Agency ID</label>
                          <input type="text" name="agencyid" class="form-control" value="{{ str_limit($user->agency[0]->name, $limit = 1,$end="") }}{{$user->agency[0]->id}}{{ str_limit($user->first_name, $limit = 1,$end="") }}{{$user->id}}" disabled >
                        </div>
                      @endif
                      <div class="col-md-6 {{ $errors->has('first_name') ? ' is-invalid' : '' }}">
                        <label class="profile_label">First Name</label>
                        <input type="text" name="first_name" class="form-control" value="{{$user->first_name}}">
                        @if ($errors->has('first_name'))
                          <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                        @endif
                      </div>
                      <div class="col-md-6 {{ $errors->has('last_name') ? ' is-invalid' : '' }}">
                        <label class="profile_label">Last Name</label>
                        <input type="text" name="last_name" class="form-control" value="{{$user->last_name}}">
                        @if ($errors->has('last_name'))
                          <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                        @endif
                      </div>
                      @if($user->hasRole('agent'))
                        <div class="col-md-12">
                          <label class="profile_label">Your Title</label>
                          <input type="text" name="agent_title" class="form-control" value="Property Agent" disabled="disabled">
                        </div>
                      @elseif($user->hasRole('agent'))
                        <div class="col-md-12">
                          <label class="profile_label">Your Title</label>
                          <input type="text" name="agency_title" class="form-control" value="Agency" disabled="disabled">
                        </div>
                      @endif
                      @if($user->hasRole('user'))
                        <div class="col-md-12 {{ $errors->has('email') ? ' is-invalid' : '' }}">
                          <label class="profile_label">Email Address :</label>
                          <input type="text" name="email" class="form-control" placeholder="Enter Your Email" value="{{$user->email}}">
                          @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                          @endif
                        </div>
                        <div class="col-md-12 {{ $errors->has('phone') ? ' is-invalid' : '' }}">
                          <label class="profile_label">Phone :</label>
                          <input type="tel" name="phone" class="form-control" placeholder="000-000-000" value="{{str_limit($user->mobile, $limit = 11,$end="")}}">
                          @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                          @endif
                        </div>
                      @elseif($user->hasRole('agent'))
                        <div class="col-md-12 {{ $errors->has('email') ? ' is-invalid' : '' }}">
                          <label class="profile_label">Email Address :</label>
                          <input type="text" name="email" class="form-control" placeholder="Enter Your Email" value="{{$user->email}}">
                          @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                          @endif
                        </div>
                        <div class="col-md-12 {{ $errors->has('phone') ? ' is-invalid' : '' }}">
                          <label class="profile_label">Phone :</label>
                          <input type="tel" name="phone" class="form-control" placeholder="000-000-000" value="{{str_limit($user->mobile, $limit = 11,$end="")}}">
                          @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                          @endif
                        </div>
                        <div class="col-md-12 {{ $errors->has('a_email') ? ' is-invalid' : '' }}">
                          <label class="profile_label">Agency Email Address :</label>
                          <input type="text" name="a_email" class="form-control" placeholder="Enter Your Email" value="{{$user->agency[0]->agency_email}}">
                          @if ($errors->has('a_email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('a_email') }}</strong>
                                    </span>
                          @endif
                        </div>
                        <div class="col-md-12 {{ $errors->has('a_phone') ? ' is-invalid' : '' }}">
                          <label class="profile_label">Agency Phone Number:</label>
                          <input type="tel" name="a_phone" class="form-control" placeholder="000-000-000" value="{{str_limit($user->agency[0]->number, $limit = 11,$end="")}}">
                          @if ($errors->has('a_phone'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('a_phone') }}</strong>
                                    </span>
                          @endif
                        </div>
                      @elseif($user->hasRole('agency'))
                        <div class="col-md-12 {{ $errors->has('a_email') ? ' is-invalid' : '' }}">
                          <label class="profile_label">Agency Email Address :</label>
                          <input type="text" name="a_email" class="form-control" placeholder="Enter Your Email" value="{{$user->agency[0]->agency_email}}">
                          @if ($errors->has('a_email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('a_email') }}</strong>
                                    </span>
                          @endif
                        </div>
                        <div class="col-md-12 {{ $errors->has('a_phone') ? ' is-invalid' : '' }}">
                          <label class="profile_label">Agency Phone Number:</label>
                          <input type="tel" name="a_phone" class="form-control" placeholder="000-000-000" value="{{str_limit($user->agency[0]->number, $limit = 11,$end="")}}">
                          @if ($errors->has('a_phone'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('a_phone') }}</strong>
                                    </span>
                          @endif
                        </div>
                      @endif
                      @if($user->hasRole('user'))
                        <div class="col-md-12 {{ $errors->has('about_me') ? ' is-invalid' : '' }}">
                          <label class="profile_label">About Me :</label>
                          <textarea class="form-control" placeholder="Write About Here....." name="about_me">{{$user->about}}</textarea>
                          @if ($errors->has('about_me'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about_me') }}</strong>
                                    </span>
                          @endif
                        </div>
                      @elseif($user->hasRole('agent'))
                        <div class="col-md-12 {{ $errors->has('about_me') ? ' is-invalid' : '' }}">
                          <label class="profile_label">About Me :</label>
                          <textarea class="form-control" placeholder="Write About Here....." name="about_me">{{$user->about}}</textarea>
                          @if ($errors->has('about_me'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about_me') }}</strong>
                                    </span>
                          @endif
                        </div>
                        <div class="col-md-12 {{ $errors->has('about_agency') ? ' is-invalid' : '' }}">
                          <label class="profile_label">About My Agency :</label>
                          <textarea class="form-control" placeholder="Write About Here....." name="about_agency">{{$user->agency[0]->about_agency}}</textarea>
                          @if ($errors->has('about_agency'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about_agency') }}</strong>
                                    </span>
                          @endif
                        </div>
                      @elseif($user->hasRole('agency'))
                        <div class="col-md-12 {{ $errors->has('about_agency') ? ' is-invalid' : '' }}">
                          <label class="profile_label">About Agency :</label>
                          <textarea class="form-control" placeholder="Write About Here....." name="about_agency">{{$user->agency[0]->about_agency}}</textarea>
                          @if ($errors->has('about_agency'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about_agency') }}</strong>
                                    </span>
                          @endif
                        </div>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="more_info">
                  <h4 class="inner-title">More Info</h4>
                  <div class="information_form">
                    <div class="row">
                      <div class="col-md-12 form-group {{ $errors->has('address') ? ' is-invalid' : '' }}">
                        <label class="profile_label">Address</label>
                        <input type="text" name="address" class="form-control" placeholder="Your Location Address" value="{{$user->address}}">
                        @if ($errors->has('address'))
                          <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                        @endif
                      </div>
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-md-6 col-xs-12 {{ $errors->has('city_state') ? ' is-invalid' : '' }}">
                            <label class="profile_label">City/state :</label>
                            <input type="text" name="city_state" class="form-control" placeholder="City Or State" value="{{$user->city_state}}">
                            @if ($errors->has('city_state'))
                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city_state') }}</strong>
                                    </span>
                            @endif
                          </div>
                          <div class="col-md-6 col-xs-12 {{ $errors->has('zipcode') ? ' is-invalid' : '' }}">
                            <label class="profile_label">Zip code :</label>
                            <input type="text" name="zipcode" class="form-control" placeholder="Zip Code" value="{{$user->zipcode}}">
                            @if ($errors->has('zipcode'))
                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('zipcode') }}</strong>
                                    </span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12 {{ $errors->has('dob') ? ' is-invalid' : '' }}">
                        <label class="profile_label">Date of Birth :</label>
                        <input type="date" class="form-control name" name="dob" value="{{$user->dob}}">
                        @if ($errors->has('dob'))
                          <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                        @endif
                      </div>
                      <div class="col-md-12 {{ $errors->has('skype_id') ? ' is-invalid' : '' }}">
                        <label class="profile_label">Skype ID :</label>
                        <input type="text" class="form-control" name="skype_id" placeholder="Your Skype ID" value="{{$user->skype_id}}">
                        @if ($errors->has('skype_id'))
                          <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('skype_id') }}</strong>
                                    </span>
                        @endif
                      </div>
                      <div class="form-group {{ $errors->has('password') ? ' is-invalid' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
                        @if ($errors->has('password'))
                          <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                      </div>
                      <div class="form-group {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                      </div>
                    </div>
                  </div>
                </div>
                @if($user->hasRole('agent|agency'))
                  <input type="hidden" name="agency" value="{{$user->agency[0]->id}}">
                @endif
                <div class="save_change">
                  <button class="btn btn-default" type="submit">Save Changes</button>
                  <p><span>Note : </span>Morbi nibh dis. Pede. Erat, porta urna. Adipiscing Ipsum nibh morbi taciti proin quisque sit quam curae; vulputate ridiculus. Dictumst ullamcorper nullam Parturient, urna. Etiam nascetur enim lectus torquent pellentesque.</p>
                </div>

              </div>
              <div class="col-md-4 col-sm-12">
                <div class="prifile_picture avata-form">

                  <img src="{{asset('uploads/profile_pictures')}}/{{$user->profile_picture}}" alt="" />
                  <input type="file" name="user_image" id="avata-upload">
                  <label class="avata-edit" for="avata-upload"><i class="flaticon-tool-1"></i></label>

                </div>
                {{--<div class="avata-info"><span>Name:</span> Harry E. Ewalt</div>--}}
                {{--<div class="avata-info"><span>Email:</span> ewalt32@info.com</div>--}}
                {{--<div class="avata-info"><span>Role:</span> Agent</div>--}}
              </div>
            </form>

          </div>
        </div>

      </div>
    </div>
  </section>
  <!-- Profile Setting End -->
@endsection

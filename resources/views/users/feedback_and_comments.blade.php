@extends('indexlayout.singlemaster')
@section('content')
    <!-- Banner Section Start -->
    <section id="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner_area">
                        <h3 class="page_title">Comments</h3>
                        <div class="page_location">
                            <a href="index_1.html">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <a href="index_1.html">Pages</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span>Comments</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Section End -->

    <!-- Profile Setting Start -->
    <section id="profile_setting">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    @include('users.settings')
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="comments-list">
                        <h3 class="inner-title">All Property Comments</h3>
                        <ul class="user-comments">
                            <li>
                                <div class="comment_description">
                                    <div class="author_img">
                                        <img src="img/blog_detail/comment-1.png" alt="">
                                    </div>
                                    <div class="author_text">
                                        <div class="author_info">
                                            <h5 class="author_name">Rebecca D. Nagy </h5>
                                            <span>12 Hours Ago</span>
                                            <a href="#" class="btn-link action"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                                            <a href="#" class="btn-link action"><i class="fa fa-eye-slash" aria-hidden="true"></i> Hide</a>
                                        </div>
                                        <p>Fermentum mus porttitor tempor arcu posuere. Nibh consectetuer condimentum ultricies pulvinar eget pede litora interdum magna aenean ullamcorper nisi dis. Viverra. Vulputate. Quisque neque luctus quis rhoncus.</p>
                                        <div class="comment_description replied">
                                            <form class="replay-form" action="#" method="POST">
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <textarea class="form-control" name="replay" placeholder="Your Comments"></textarea>
                                                        <button type="submit" class="btn btn-default">Replay</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="comment_description">
                                    <div class="author_img">
                                        <img src="img/blog_detail/comment-2.png" alt="">
                                    </div>
                                    <div class="author_text">
                                        <div class="author_info">
                                            <h5 class="author_name">Charles F. Bush</h5>
                                            <span>1 Days Ago</span>
                                            <a href="#" class="btn-link action"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                                            <a href="#" class="btn-link action"><i class="fa fa-eye-slash" aria-hidden="true"></i> Hide</a>
                                        </div>
                                        <p>Ullamcorper parturient elit, mauris congue duis morbi lacus eget id pellentesque commodo porta bibendum ullamcorper mauris dui fusce dolor massa class ultricies.</p>
                                        <div class="comment_description replied">
                                            <div class="author_img">
                                                <img src="img/blog_detail/comment-1.png" alt="">
                                            </div>
                                            <div class="author_text">
                                                <div class="author_info">
                                                    <h5 class="author_name">Patty Hurd</h5>
                                                    <span>10 Hours Ago</span>
                                                    <a href="#" class="btn-link action"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                                                    <a href="#" class="btn-link action"><i class="fa fa-eye-slash" aria-hidden="true"></i> Hide</a>
                                                </div>
                                                <p>Pretium urna nonummy sodales, dictumst blandit, magna. Quis porttitor lobortis lectus fringilla nam at Sociis vel pharetra enim per praesent viverra consequat litora, pharetra turpis magna tincidunt curae; molestie non.</p>
                                            </div>
                                            <form class="replay-form" action="#" method="POST">
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <textarea class="form-control" name="replay" placeholder="Your Comments"></textarea>
                                                        <button type="submit" class="btn btn-default">Replay</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="comment_description">
                                    <div class="author_img">
                                        <img src="img/blog_detail/comment-4.png" alt="">
                                    </div>
                                    <div class="author_text">
                                        <div class="author_info">
                                            <h5 class="author_name">Joseph Richard </h5>
                                            <span>3 Days Ago</span>
                                            <a href="#" class="btn-link action"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                                            <a href="#" class="btn-link action"><i class="fa fa-eye-slash" aria-hidden="true"></i> Hide</a>
                                        </div>
                                        <p>Hac placerat. Morbi. Parturient. Nulla porta duis. Donec fames vel. Quam sem. Purus odio ultrices augue. Diam ridiculus cras luctus.</p>
                                        <div class="comment_description replied">
                                            <form class="replay-form" action="#" method="POST">
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <textarea class="form-control" name="replay" placeholder="Your Comments"></textarea>
                                                        <button type="submit" class="btn btn-default">Replay</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Profile Setting End -->
@endsection

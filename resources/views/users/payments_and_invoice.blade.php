@extends('indexlayout.singlemaster')
@section('content')
<!-- Banner Section Start -->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner_area">
                    <h3 class="page_title">Invoice</h3>
                    <div class="page_location">
                        <a href="index_1.html">Home</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <a href="index_1.html">Pages</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <span>Invoice</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->

<!-- Profile Setting Start -->
<div id="profile_setting">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                @include('users.settings')
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="invoice-list">
                    <table class="list_table" cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <td>Date</td>
                            <td>Invoice ID</td>
                            <td>Description</td>
                            <td>Status</td>
                            <td>Value</td>
                            <td>Paid</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>09 Sep, 2017</td>
                            <td><a href="invoice_details.html">UKJ81712</a></td>
                            <td><a href="invoice_details.html">Paid Office Rent and Installment</a></td>
                            <td class="due">Due</td>
                            <td>$12,000</td>
                            <td>$12,000</td>
                        </tr>
                        <tr>
                            <td>01 Sep, 2017</td>
                            <td><a href="invoice_details.html">SAD76672</a></td>
                            <td><a href="invoice_details.html">Rent Appartment</a></td>
                            <td class="due">Due</td>
                            <td>$8,000</td>
                            <td>$8,000</td>
                        </tr>
                        <tr>
                            <td>20 Aug, 2017</td>
                            <td><a href="invoice_details.html">KHH12312</a></td>
                            <td><a href="invoice_details.html">Paid Installemtn</a></td>
                            <td>Paid</td>
                            <td>$1,200</td>
                            <td>$12,000</td>
                        </tr>
                        <tr>
                            <td>12 Aug, 2017</td>
                            <td><a href="invoice_details.html">LJA878962</a></td>
                            <td><a href="invoice_details.html">Paid Office Rent and Installment</a></td>
                            <td>Paid</td>
                            <td>$1,200</td>
                            <td>$1,200</td>
                        </tr>
                        <tr>
                            <td>25 July, 2017</td>
                            <td><a href="invoice_details.html">KAS90121</a></td>
                            <td><a href="invoice_details.html">Appartment Purchase</a></td>
                            <td>Paid</td>
                            <td>$5,000</td>
                            <td>$5,000</td>
                        </tr>
                        <tr>
                            <td>30 June, 2017</td>
                            <td><a href="invoice_details.html">ULH723412</a></td>
                            <td><a href="invoice_details.html">Paid Sevice Fees</a></td>
                            <td>Paid</td>
                            <td>$2,000</td>
                            <td>$2,000</td>
                        </tr>
                        <tr>
                            <td>15 May, 2017</td>
                            <td><a href="invoice_details.html">KAL76978</a></td>
                            <td><a href="invoice_details.html">Purchase House</a></td>
                            <td>Paid</td>
                            <td>$12,000</td>
                            <td>$12,000</td>
                        </tr>
                        <tr>
                            <td>01 Sep, 2017</td>
                            <td><a href="invoice_details.html">SAD76672</a></td>
                            <td><a href="invoice_details.html">Rent Appartment</a></td>
                            <td>Paid</td>
                            <td>$8,000</td>
                            <td>$8,000</td>
                        </tr>
                        <tr>
                            <td>20 Aug, 2017</td>
                            <td><a href="invoice_details.html">KHH12312</a></td>
                            <td><a href="invoice_details.html">Paid Installment</a></td>
                            <td>Paid</td>
                            <td>$1,200</td>
                            <td>$12,000</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- Pagination -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="pagination_area">
                            <nav aria-label="Page navigation">
                                <ul class="pagination pagination_edit">
                                    <li>
                                        <a href="#" aria-label="Previous">
                                            <span aria-hidden="true">prev</span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">next</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- End Pagination -->
            </div>
        </div>
    </div>
</div>
<!-- Profile Setting End -->
@endsection

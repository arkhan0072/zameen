@extends('indexlayout.singlemaster')
@section('content')
    <!-- Banner Section Start -->
    <section id="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner_area">
                        <h3 class="page_title">Inbox</h3>
                        <div class="page_location">
                            <a href="index_1.html">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <a href="index_1.html">Pages</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span>Inbox</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Section End -->

    <!-- Profile Setting Start -->
    <div id="profile_setting">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    @include('users.settings')
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="message_table">
                        <ul>
                            <li>
                                <a href="message_view.html" class="unread">
                                    <span class="ping">New</span>
                                    <img src="img/blog_detail/comment-1.png" alt="">
                                    <span class="sender_name">Connor Webber</span>
                                    <p>Quis fringilla sagittis condimentum eu nisi velit vehicula</p>
                                    <span class="datetime">12 Sep, 2017<br> 10:45AM</span>
                                </a>
                            </li>
                            <li>
                                <a href="message_view.html" class="unread">
                                    <span class="ping">New</span>
                                    <img src="img/blog_detail/comment-2.png" alt="">
                                    <span class="sender_name">Connor Webber</span>
                                    <p>Quis fringilla sagittis condimentum eu nisi velit vehicula</p>
                                    <span class="datetime">10 Sep, 2017<br> 10:00PM</span>
                                </a>
                            </li>
                            <li>
                                <a href="message_view.html">
                                    <img src="img/blog_detail/comment-3.png" alt="">
                                    <span class="sender_name">Connor Webber</span>
                                    <p>Quis fringilla sagittis condimentum eu nisi velit vehicula</p>
                                    <span class="datetime">09 Sep, 2017<br> 08:00PM</span>
                                </a>
                            </li>
                            <li>
                                <a href="message_view.html">
                                    <img src="img/blog_detail/comment-4.png" alt="">
                                    <span class="sender_name">Connor Webber</span>
                                    <p>Quis fringilla sagittis condimentum eu nisi velit vehicula</p>
                                    <span class="datetime">09 Sep, 2017<br> 11:47AM</span>
                                </a>
                            </li>
                            <li>
                                <a href="message_view.html">
                                    <img src="img/blog_detail/comment-1.png" alt="">
                                    <span class="sender_name">Connor Webber</span>
                                    <p>Quis fringilla sagittis condimentum eu nisi velit vehicula</p>
                                    <span class="datetime">12 August, 2017<br> 09:12AM</span>
                                </a>
                            </li>
                            <li>
                                <a href="message_view.html">
                                    <img src="img/blog_detail/comment-2.png" alt="">
                                    <span class="sender_name">Connor Webber</span>
                                    <p>Quis fringilla sagittis condimentum eu nisi velit vehicula</p>
                                    <span class="datetime">01 August, 2017<br> 05:37AM</span>
                                </a>
                            </li>
                            <li>
                                <a href="message_view.html">
                                    <img src="img/blog_detail/comment-3.png" alt="">
                                    <span class="sender_name">Connor Webber</span>
                                    <p>Quis fringilla sagittis condimentum eu nisi velit vehicula</p>
                                    <span class="datetime">30 July, 2017<br> 03:30AM</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Profile Setting End -->
@endsection

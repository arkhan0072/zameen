@extends('indexlayout.master')
@section('content')

    <!-- Banner Section Start -->
    <section id="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner_area">
                        <h3 class="page_title">Edit Property</h3>
                        <div class="page_location">
                            <a href="index_1.html">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <a href="index_1.html">Pages</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span>Edit Property</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Section End -->
    <!-- Submit Property Start -->
    <section id="submit_property">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <form action="{{route('listing.update', $listing->id)}}" method="post" class="submit_form" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                        <div class="basic_information">
                            <h4 class="inner-title">Basic Information</h4>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 form-group {{ $errors->has('title') ? 'has-error': ''}}">
                                    <input type="text" name="title" placeholder="Property Title" class="form-control" value="{{$listing->title}}">
                                </div>
                                @if($errors->has('title'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                                {{--<div class="col-md-6 col-sm-6">--}}
                                {{--<input type="text" placeholder="Property Status" class="form-control">--}}
                                {{--</div>--}}
                                <div class="col-md-6 col-sm-6 form-group {{ $errors->has('category') ? 'has-error': ''}}">
                                    <select class="selectpicker form-control" data-live-search="true" id="category" name="category">
                                        <option value="">Please Select Category</option>
                                        @foreach($categories as $cat)
                                            <option value="{{$cat->id}}" selected>{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('category'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('category') }}</strong>
                                </span>
                                @endif
                                <div class="col-md-6 col-sm-6 form-group {{ $errors->has('subcategory') ? 'has-error': ''}}" id="subcat">
                                    <select class="selectpicker form-control" data-live-search="true" id="subcategory" name="subcategory">
                                        <option value="{{$SubCat[0]->id}}">{{$SubCat[0]->name}}</option>
                                    </select>
                                </div>
                                @if($errors->has('subcategory'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('subcategory') }}</strong>
                                </span>
                                @endif
                                <div class="col-md-6 col-sm-6 form-group {{ $errors->has('purpose') ? 'has-error': ''}}">
                                    <select class="selectpicker form-control" name="purpose">
                                        <option value="">Please Select Purpose</option>
                                        @foreach($purposes as $purpose)
                                            <option value="{{$purpose->id}}" selected>{{$purpose->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('purpose'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('purpose') }}</strong>
                                </span>
                                @endif
                                <div class="col-md-6 col-sm-6 form-group {{ $errors->has('area') ? 'has-error': ''}}">
                                    <input type="text" name="area" placeholder="Ares (Sqft)" class="form-control" value="{{$listing->area}}">
                                    @if($errors->has('area'))
                                        <span class="help-block">
                            <strong>{{ $errors->first('area') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="col-md-6 col-sm-6 form-group {{$errors->has('areaType') ? 'has-error':''}}">
                                    <select class="selectpicker form-control" name="areaType">
                                        <option value="Square Feet" selected>Square Feet</option>
                                        <option value="Square Yards">Square Yards</option>
                                        <option value="Square Meters">Square Meters</option>
                                        <option value="Marla">Marla</option>
                                        <option value="Kanal">Kanal</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <input type="text" placeholder="Price (USD)" name="price" class="form-control" value="{{$listing->price}}">
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <input type="text" placeholder="No. Of Rooms" name="rooms" class="form-control" value="{{$listing->room_number}}">
                                </div>
                                <div class="col-md-12">
                                    @php
                                            if($listing->is_installment){
                                            $is_installment = 'checked';
                                            }
                                            else{
                                             $is_installment = '';
                                            }
                                            if($listing->is_insurance){
                                            $is_insurance = 'checked';
                                            }
                                            else{
                                             $is_insurance = '';
                                            }
                                            if($listing->is_labilities){
                                            $is_labilities = 'checked';
                                            }
                                            else{
                                            $is_labilities = '';
                                            }

                                    @endphp
                                    <div class="important_facts">
                                        <label>Do you have any installment system for payment?</label>
                                        <div class="radio_check">
                                            <input type="checkbox" id="radio_1" name="question1" value="1" {{$is_installment}}>
                                            <label for="radio_1"><span>No</span><span>Yes</span></label>
                                        </div>
                                    </div>
                                    <div class="important_facts">
                                        <label>Have any insurance of this property?</label>
                                        <div class="radio_check">
                                            <input type="checkbox" id="radio_2" name="question2" value="1" {{$is_insurance}}>
                                            <label for="radio_2"><span>No</span><span>Yes</span></label>
                                        </div>
                                    </div>
                                    <div class="important_facts">
                                        <label>Is there any labilities of third party with this property?</label>
                                        <div class="radio_check">
                                            <input type="checkbox" id="radio_3" name="question3" value="1" {{$is_labilities}}>
                                            <label for="radio_3"><span>No</span><span>Yes</span></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-warning">Please input the correct information of your property. Because it will effect on property search.</div>
                        </div>
                        <div class="description">
                            <h4 class="inner-title">Description</h4>
                            <textarea name="description" placeholder="Type Description..." class="form_description">{{$listing->description}}</textarea>
                            <div class="alert alert-warning">Need a proper description about the property. So that client can easily understand about the property.</div>
                        </div>
                        <div class="property_location">
                            <h4 class="inner-title">property location</h4>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <input type="text" placeholder="Property Adddress" name="address" class="form-control" value="{{$addresses[0]->address}}">
                                        </div>
                                        <div class="col-md-6 col-sm-6 form-group {{ $errors->has('state') ? 'has-error': ''}}">
                                            <select class="selectpicker form-control" id="state" name="state">
                                                <option value="">Please Select State</option>
                                                @foreach($states as $sat)
                                                    <option value="{{$sat->id}}" selected>{{$sat->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if($errors->has('state'))
                                            <span class="help-block">
                            <strong>{{ $errors->first('state') }}</strong>
                                </span>
                                        @endif
                                        <div class="col-md-4 col-sm-4" id="subcity">
                                            <select class="selectpicker form-control" id="city" name="city">

                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <select class="selectpicker form-control" id="cityProject" name="project">
                                                <option value="0">Please Select Project</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <input type="text" placeholder="Zip Code" name="zipcode" class="form-control">
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <select class="selectpicker form-control" data-live-search="true" name="country">
                                                <option value="0">Selcet Country</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}" selected>{{$country->country_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="exampleInputEmail1">Features Category:</label>
                            <select id="category_feature" class="form-control" name="feature_category">
                                <option value="">Select Features Category</option>
                                @foreach($feature_categories as $feature_category)
                                    <option value="{{$feature_category->id}}">{{$feature_category->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group col-md-3">
                            <label for="exampleInputEmail1">Features:</label>
                            <select id="feature" class="form-control" name="feature">

                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="feature_value">Value&nbsp;</label>
                            <input type="text" class="form-control" id="feature_value" name="feature_value" placeholder="Enter feature value">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="add-button">&nbsp;</label>
                            <button id="add" class="btn btn-success form-control"><i class="fa fa-plus-square"></i> Add</button>
                        </div>



                        <div class="form-group col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Feature Category</th>
                                    <th>Feature</th>
                                    <th>Value</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="features_table">
                                @foreach($listing->features as $feature)
                                    <tr class="deleteFromList">
                                        <td>{{$feature->category->name}}</td>
                                        <td>{{$feature->name}}</td>
                                        <td><input type="text" class="form-control" name="feature_value[{{$feature->id}}]"  value="{{$feature->pivot->feature_value}}"></td>
                                        <td><a class="btn btn-warning deleteFromList"><i class="fa fa-trash"></i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>&nbsp;
                        {{--<div class="check_feature">--}}
                            {{--<h4 class="inner-title">Add Feature</h4>--}}
                            {{--<div class="row">--}}

                                {{--<div class="col-md-4 col-sm-6">--}}
                                    {{--<select id="category_feature" class="selectpicker form-control" name="feature_category">--}}
                                        {{--<option value="">Select Features Category</option>--}}
                                        {{--@foreach($feature_categories as $feature_category)--}}
                                            {{--<option value="{{$feature_category->id}}">{{$feature_category->name}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-3 col-sm-6">--}}
                                    {{--<select id="feature" class="selectpicker form-control" name="feature">--}}

                                    {{--</select>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-3 col-sm-6">--}}
                                    {{--<input type="text" class="form-control" id="feature_value" name="feature_value" placeholder="Enter feature value">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-2 col-sm-6">--}}
                                    {{--<button id="add" class="btn btn-success"><i class="fa fa-plus-square"></i> Add</button>--}}
                                {{--</div>--}}
                                {{--<div class="form-group col-md-12">--}}
                                    {{--<table class="table table-responsive">--}}
                                        {{--<thead>--}}
                                        {{--<tr>--}}
                                            {{--<th>Feature Category</th>--}}
                                            {{--<th>Feature</th>--}}
                                            {{--<th>Value</th>--}}
                                            {{--<th></th>--}}
                                        {{--</tr>--}}
                                        {{--</thead>--}}
                                        {{--<tbody id="features_table">--}}
                                        {{--@foreach($listing->features as $feature)--}}
                                            {{--<tr>--}}
                                                {{--<td>{{$feature->category->name}}</td>--}}
                                                {{--<td>{{$feature->name}}</td>--}}
                                                {{--<td><input type="text" class="form-control" value="{{$feature->pivot->feature_value}}"  name="feature_value[$listing->pivot->feature_value]"></td>--}}
                                                {{--<td><a class="btn btn-warning deleteFromList"><i class="fa fa-trash"></i></a></td>--}}
                                            {{--</tr>--}}
                                        {{--@endforeach--}}
                                        {{--</tbody>--}}

                                    {{--</table>--}}
                                {{--</div>--}}
                                {{--<ul>--}}
                                {{--<li>--}}
                                {{--<input id="feature_1" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_1"></label>--}}
                                {{--<span>garden include</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_2" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_2"></label>--}}
                                {{--<span>Garage Facility</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_3" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_3"></label>--}}
                                {{--<span>Fire Security</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_4" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_4"></label>--}}
                                {{--<span>Swiming Pool</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_5" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_5"></label>--}}
                                {{--<span>Fire Place Facility</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_6" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_6"></label>--}}
                                {{--<span>Play Ground</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_7" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_7"></label>--}}
                                {{--<span>Ferniture Include</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_8" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_8"></label>--}}
                                {{--<span>Marbel Floor</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_9" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_9"></label>--}}
                                {{--<span>Store Room</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_10" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_10"></label>--}}
                                {{--<span>Hight Class Door</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_11" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_11"></label>--}}
                                {{--<span>Floor Heating System</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_12" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_12"></label>--}}
                                {{--<span>Air Condition Facility</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_13" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_13"></label>--}}
                                {{--<span>Fire Security</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_14" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_14"></label>--}}
                                {{--<span>Fitness Room</span>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="feature_15" type="checkbox" class="submit_checkbox">--}}
                                {{--<label for="feature_15"></label>--}}
                                {{--<span>Bathtub and Shaware</span>--}}
                                {{--</li>--}}
                                {{--</ul>--}}

                            {{--</div>--}}
                            {{--<div class="alert alert-warning">Check the extra features and facility of the property, it will show with the property.</div>--}}
                        {{--</div>--}}
                        <div class="upload_media">
                            <h4 class="inner-title">Upload Phto and Document</h4>
                            <p>Felis etiam erat curabitur bibendum iaculis quisque placerat egestas. Nullam, lacus dis et consectetuer rhoncus etiam. Non vitae turpis curae; lacus sociosqu. Quisque. Lobortis aliquam penatibus mi. </p>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="browse_submit">
                                        <span>add photos</span>
                                        <input type="file" name="picture[]" multiple id="fileupload-example-1"/>
                                        <label class="fileupload-example-label" for="fileupload-example-1">Drop your photos here or Click</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="browse_submit">
                                        <span>add documents</span>
                                        <input type="file" id="fileupload-example-2"/>
                                        <label class="fileupload-example-label" for="fileupload-example-2">Drop your document file here or Click</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="video_upload">
                                        <input type="url" placeholder="Add property video links or URL" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-warning">Please uplaod the photo of the property, please keep the photo size 760X410 ratio and please upload the PDF or Doc file at the document attachment.</div>
                        </div>
                        <div class="property_owner">
                            <h4 class="inner-title">Input Contact Details</h4>
                            <p>Felis etiam erat curabitur bibendum iaculis quisque placerat egestas. Nullam, lacus dis et consectetuer rhoncus etiam. Non vitae turpis curae; lacus sociosqu. Quisque. Lobortis aliquam penatibus mi. </p>
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" placeholder="Full Name" class="form-control" value="">
                                </div>
                                <div class="col-md-4">
                                    <input type="email" placeholder="Email Address" class="form-control" >
                                </div>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Phone Number" class="form-control" >
                                </div>
                            </div>
                            <div class="browse_submit">
                                <button name="submit" class="btn btn-default">submit property</button>
                                <p><span>note</span> : Your property will under review for 24 Hours after submission *</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Submit Property End -->

@stop
@section('page_level_script')
    <script>
        $('#category').on('change',function () {
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '{{route('filtersubcat')}}',
                data: {id: id},
                success: function (result) {
                    $('#subcategory').html(result);
                    $('#subcategory').selectpicker('refresh'); // without the []
                    console.log(result);
                }
            })
        });
    </script>
    <script>
        $('#state').on('change', function(){
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '{{route('filtersubcity')}}',
                data: {id: id},
                success: function (result) {
                    $('#city').html(result);
                    $('#city').selectpicker('refresh'); // without the []
                    console.log(result);
                }
            })
        });
    </script>
    <script>
        $('#city').on('change', function(){
            var id = $(this).find(":selected").val();
            $.ajax({
                type: "GET",
                url: '{{route('filtercityproject')}}',
                data: {id: id},
                success: function (result) {
                    $('#cityProject').html(result);
                    $('#cityProject').selectpicker('refresh');
                    console.log(result);
                }
            })
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#category_feature").change(function()
            {
                var id=$(this).val();
                $.ajax
                ({
                    type: "GET",
                    url: "{{route('features_category.list')}}",
                    data: {
                        id : id
                    },
                    success: function(html)
                    {
                        $("#feature").html(html);
                        $("#feature").selectpicker('refresh');
                    },
                    error : function (error) {
                        console.log(error);
                    }
                });

            });

        });
        $('#add').on('click', function (event) {
            event.preventDefault();
            var category = $('#category_feature option:selected')
            var feature = $('#feature option:selected');

            var feature_value = $('#feature_value');

            if(category.val() === '' || feature_value.val() === ''){
                alert('Please enter the values.');
            }
            else {

                var featureID = $('#feature').val();

                var html = '<tr>\n' +
                    '  <td>' + category.text() + '</td>' +
                    '  <td>' + feature.text() + '</td> ' +
                    '  <td><input type="text" class="form-control" name="feature_value[' + featureID + ']"  value="' + feature_value.val() + '"></td> ' +
                    '  <td><a class="btn btn-warning deleteFromList"><i class="fa fa-trash"></i></a></td> ' +
                    '</tr>';


                $('#features_table').append(html);

                $('#feature_value').val('');
            }

        });



        $(document).on('click', '.deleteFromList', function(){
            $(this).closest('tr').remove();
        });
        $(document).on('click','editpicture',function(event){
            event.preventDefault();
        });
    </script>
    {{--<script>--}}
    {{--$('#editpic').on('click',function () {--}}
    {{--var id =$(this).data('id');--}}
    {{--alert('good');--}}
    {{--url = "{{route('photo.update', ':id')}}";--}}
    {{--$.ajax--}}
    {{--({--}}
    {{--type: "GET",--}}
    {{--url: url.replace(':id', id),--}}
    {{--data: {--}}
    {{--id : id,--}}
    {{--_token: "{{@csrf_token()}}"--}}
    {{--},--}}
    {{--success: function(html)--}}
    {{--{--}}
    {{--$("#feature").html(html);--}}
    {{--},--}}
    {{--error : function (error) {--}}
    {{--console.log(error);--}}
    {{--}--}}
    {{--});--}}
    {{--});--}}
    {{--</script>--}}
@endsection


@extends('indexlayout.master')
@section('content')
<!-- Banner Section Start -->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner_area">
                    <h3 class="page_title">Properties Grid</h3>
                    <div class="page_location">
                        <a href="index_1.html">Home</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <span>Property Grid</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->

<!-- Property Grid Start -->
<section id="property_area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="property_sorting">
                            <form class="property_filter" action="#" method="post">
                                <div class="property_show">
                                    <select class="selectpicker form-control">
                                        <option>Any Type</option>
                                        <option>For Rent</option>
                                        <option>For Sale</option>
                                    </select>
                                </div>
                                <div class="property_view">
                                    <ul>
                                        <li>
                                            <span>Order:</span>
                                            <select class="selectpicker form-control">
                                                <option>Default Order</option>
                                                <option>Featured</option>
                                                <option>Price Hight</option>
                                                <option>Price Low</option>
                                                <option>Latest Item</option>
                                                <option>Oldest Item</option>
                                            </select>
                                        </li>
                                        <li>
                                            <a href="property_grid.html"><i class="fa fa-th" aria-hidden="true"></i></a>
                                        </li>
                                        <li>
                                            <a class="active" href="property_list.html"><i class="fa fa-th-list" aria-hidden="true"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Property Grids -->
                <div class="row">
                    @foreach($listing as $list)
                        <div class="col-md-6 col-sm-6">
                            <div class="property_grid">
                                <div class="img_area">
                                    <div class="sale_btn">{{$list->purpose->name}}</div>
                                    <a href="{{route('viewSingleListing',$list->slug)}}"><img src="{{asset('uniland/img/property_grid/property_grid-4.png')}}" alt=""></a>
                                    <div class="sale_amount">2 Hours Ago</div>
                                    <div class="hover_property">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-text">
                                    <a href="{{route('viewSingleListing',$list->slug)}}">
                                        <h5 class="property-title">{{$list->title}}</h5>
                                    </a> <span><i class="fa fa-map-marker" aria-hidden="true"></i> 4213 Duff Avenue South Burlington, VT 05403 </span>
                                    <div class="quantity">
                                        <ul>
                                            <li><span>Area</span>{{$list->area}}&nbsp;J                                                                                             Sqft</li>
                                            <li><span>Rooms</span>1200</li>
                                            <li><span>Beds</span>5</li>
                                            <li><span>Baths</span>4</li>
                                            <li><span>Garage</span>1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bed_area">
                                    <ul>
                                        <li>${{$list->price}}/mo</li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach

                </div>
                <!-- End property Grids -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="pagination_area">
                            <nav aria-label="Page navigation">
                                <ul class="pagination pagination_edit">
                                    <li>
                                        <a href="#" aria-label="Previous">
                                            <span aria-hidden="true">prev</span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">next</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Sidebar -->
            <div class="col-md-4">
                <div class="property_sidebar">
                    <form method="get" class="search_form" action="{{route('advanced.search')}}">
                    <div class="price_range sidebar-widget">
                        <h4 class="widget-title">Price</h4>
									<span class="price-slider">
										<input class="filter_price" type="text" name="price" value="0;{{$max_price[0]->price}}" />
									</span>
                    </div>
                    <div class="price_range sidebar-widget area-filter">
                        <h4 class="widget-title">Area</h4>
									<span class="price-slider">
										<input class="area_filter" type="text"  name="area" value="0;{{$max_area[0]->area}}" />
                                    </span>

                    </div>
                    <div class="advance_search sidebar-widget">
                        <h4 class="widget-title">Advance Search</h4>

                            <div class="row">
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control"  name="purpose" data-live-search="true">
                                        <option value="">All Purposes</option>
                                        @foreach($purposes as $purpose)
                                            <option value="{{$purpose->id}}">{{$purpose->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control"  name="category" data-live-search="true">
                                        <option value="">All Categories</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control"  name="subcategory" data-live-search="true">
                                        <option value="">Sub Categories</option>
                                        @foreach($sub_categories as $sub_category)
                                            <option value="{{$sub_category->id}}">{{$sub_category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control" name="state" data-live-search="true">
                                        <option value="">All States</option>
                                        @foreach($states as $state)
                                             <option value="{{$state->id}}">{{$state->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control" name="city" data-live-search="true">
                                        <option value="">All City</option>
                                        @foreach($cities as $city)
                                             <option>{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <select class="selectpicker form-control">
                                        <option>Beds</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>6</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <select class="selectpicker form-control">
                                        <option>Baths</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <button class="btn btn-default" type="submit">search property</button>
                                </div>
                            </div>

                    </div>
                    </form>
                    <div class="sidebar_agent sidebar-widget">
                        <h4 class="widget-title">Our Agent</h4>
                        <div class="member-widget-view">
                            <div class="item">
                                <div class="team_img">
                                    <div class="profile-pic"><img src="img/teams/1.jpg" alt=""></div>
                                    <div class="member_info">
                                        <a href="agent_profile.html"><h5 class="member-name">Taylor Swift</h5></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="team_img">
                                    <div class="profile-pic"><img src="img/teams/2.jpg" alt=""></div>
                                    <div class="member_info">
                                        <a href="agent_profile.html"><h5 class="member-name">Susan White</h5></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="team_img">
                                    <div class="profile-pic"><img src="img/teams/3.jpg" alt=""></div>
                                    <div class="member_info">
                                        <a href="agent_profile.html"><h5 class="member-name">Johan Dow</h5></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="team_img">
                                    <div class="profile-pic"><img src="img/teams/4.jpg" alt=""></div>
                                    <div class="member_info">
                                        <a href="agent_profile.html"><h5 class="member-name">Lisa Milk</h5></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="team_img">
                                    <div class="profile-pic"><img src="img/teams/5.jpg" alt=""></div>
                                    <div class="member_info">
                                        <a href="agent_profile.html"><h5 class="member-name">Susan White</h5></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="team_img">
                                    <div class="profile-pic"><img src="img/teams/6.jpg" alt=""></div>
                                    <div class="member_info">
                                        <a href="agent_profile.html"><h5 class="member-name">Taylor Swift</h5></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar_tesimonial">
                        <h4 class="widget-title">Clients Feedback</h4>
                        <div class="feedback_small">
                            <div class="item">
                                <div class="testimonial_person">
                                    <img class="avata" src="img/testimonial/Johan-swift.png" alt="">
                                    <h6 class="client_name">Leandro Crocker</h6>
                                    <span>commercial leader</span>
                                </div>
                                <p>“ Nisi fermentum augue etiam convallis mollis consectetuer bibendum. Gravida. Purus praesent urna fringilla magna lacinia praesent vivamus. Quis adipiscing. Dictum sociosqu lectus. Nulla nascetur vitae praesent elit habitasse.”</p>
                            </div>
                            <div class="item">
                                <div class="testimonial_person">
                                    <img class="avata" src="img/testimonial/Johan-swift.png" alt="">
                                    <h6 class="client_name">Leandro Crocker</h6>
                                    <span>commercial leader</span>
                                </div>
                                <p>“ Nisi fermentum augue etiam convallis mollis consectetuer bibendum. Gravida. Purus praesent urna fringilla magna lacinia praesent vivamus. Quis adipiscing. Dictum sociosqu lectus. Nulla nascetur vitae praesent elit habitasse.”</p>
                            </div>
                            <div class="item">
                                <div class="testimonial_person">
                                    <img class="avata" src="img/testimonial/Johan-swift.png" alt="">
                                    <h6 class="client_name">Leandro Crocker</h6>
                                    <span>commercial leader</span>
                                </div>
                                <p>“ Nisi fermentum augue etiam convallis mollis consectetuer bibendum. Gravida. Purus praesent urna fringilla magna lacinia praesent vivamus. Quis adipiscing. Dictum sociosqu lectus. Nulla nascetur vitae praesent elit habitasse.”</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End SIdebar -->
        </div>
    </div>
</section>
<!-- Property Grid End -->

@stop

@section('page_level_script')
    <script>
        $(".filter_price").slider({
            from: 0,
            to: "{{$max_price[0]->price}}",
            step: 1000,
            smooth: true,
            round: 0,
            dimension: "$",
            skin: "plastic"
        });
        $(".area_filter").slider({
            from: 0,
            to: "{{$max_area[0]->area}}",
            step: 10,
            smooth: true,
            round: 0,
            dimension: "sq ft",
            skin: "plastic"
        });
        $(document).ready(function () {

            $(".price-slider").change(function(){
                var url = $(window.parent.location).attr('href') + "?price=" + $(".filter_price").val();
                window.location.replace(url);
            });
        });

    </script>
@endsection
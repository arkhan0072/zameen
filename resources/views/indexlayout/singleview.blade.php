@extends('indexlayout.singlemaster')
@section('content')
<!-- Banner Section Start -->
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner_area">
                    <h3 class="page_title">{{$listing->title}}</h3>
                    {{--<div class="page_location">--}}
                        {{--<a href="index_1.html">Home</a>--}}
                        {{--<i class="fa fa-angle-right" aria-hidden="true"></i>--}}
                        {{--<span>Property Single</span>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->

<!-- Single Property Start -->
<section id="single_property">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="property_slider">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            {{--<div class="item active">--}}
                                {{--<img src="img/single_property/1.png" alt="">--}}
                            {{--</div>--}}
                            @foreach($listing->photos as $key => $picture)
                                    <div class="item {{$key ?'':'active'}}">

                                <img src="{{asset('uploads/listings')}}/{{$picture->name}}" alt="" style="height: 620px!important;width: 1170px!important;">
                            </div>
                            @endforeach
                            {{--<div class="item">--}}
                                {{--<img src="img/single_property/3.png" alt="">--}}
                            {{--</div>--}}
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <div class="lef_btn">prev</div>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <div class="right_btn">next</div>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="property-summary2">
                    <div class="profile_data">
                        <ul>
                            <li><span>Property ID :</span> {{ str_limit($listing->project->city->state->name, $limit = 1,$end="") }}{{ $listing->project->city->state->id}}{{ str_limit($listing->project->city->name, $limit = 1,$end="") }}{{ $listing->project->city_id}}{{$listing->id}}</li>
                            <li><span>Listing Type :</span> {{$listing->purpose->name}}</li>
                            <li><span>Property Type :</span> {{$listing->category->name}}</li>
                            <li><span>Price :</span> ${{$listing->price}}</li>
                            <li><span>Area :</span> {{$listing->area}} sqft</li>
                            <li><span>Car Garage :</span> Yes ( 5 Capacity )</li>
                            <li><span>Swimming :</span> Yes ( 1 Large )</li>
                            <li><span>Garden :</span> Yes</li>
                            <li><span>Rating :</span>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i> (12)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="detail_text">
                            <div class="property-text">
                                <h4 class="property-title">{{$listing->title}}</h4>
                                <span><i class="fa fa-map-marker" aria-hidden="true"></i> {{$listing->project->city->name}},{{$listing->project->city->state->name}} </span>
                                <blockquote>Dis at habitasse viverra nisl. Arcu nec id imperdiet venenatis, lacus sollicitudin in et libero sed senectus ullamcorper conubia velit metus lacinia duis fermentum dictumst ut phasellus aptent tempor lectus.</blockquote>
                                <p>{{strip_tags($listing->description)}}</p>
                            </div>
                        </div>
                        <div class="more_information">
                            <h4 class="inner-title">More Information</h4>
                            <div class="profile_data">
                                <ul>
                                    <li><span>Age :</span> 10 Years</li>
                                    <li><span>Type :</span> Appartment</li>
                                    <li><span>Installment Facility :</span> Yes</li>
                                    <li><span>Insurance :</span> Yes</li>
                                    <li><span>3rd Party :</span> No</li>
                                    <li><span>Swiming Pool :</span> Yes</li>
                                    <li><span>Garden and Field :</span> 2400sqft</li>
                                    <li><span>Total Floor :</span> 3 Floor</li>
                                    <li><span>Security :</span> 3 Step Security</li>
                                    <li><span>Alivator :</span> 2 Large</li>
                                    <li><span>Dining Capacity :</span> 20 People</li>
                                    <li><span>Exit :</span> 3 Exit Gate</li>
                                    <li><span>Fire Place :</span> Yes</li>
                                    <li><span>Heating System :</span> Floor Heating</li>
                                </ul>
                            </div>
                        </div>
                        <div class="single_video">
                            <video width="400" controls poster="video/poster.png">
                                <source src="{{asset('uniland/video/real_estate.html')}}" type="video/mp4">
                            </video>
                        </div>
                        <div class="single_feature">
                            <h4 class="inner-title">Features</h4>
                            <div class="info-list">
                                <ul>
                                    @foreach($listing->features as $key=>$feature)
                                    <li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>{{$feature->pivot->feature_value}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="single_map">
                            <h4 class="inner-title">Location</h4>
                            <div id="map" class="map-canvas"> </div>
                        </div>
                        {{--<div class="rating-box">--}}
                            {{--<h4 class="inner-title">Give a Review</h4>--}}
                            {{--<form action="#" method="post" class="rating-form">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-12 rating-animation">--}}
                                        {{--<div class="box-header">Move Mouse for Rating</div>--}}
                                        {{--<div class="star-rating">--}}
                                            {{--<select id="example-reversed" name="rating" autocomplete="off">--}}
                                                {{--<option value="Strongly Disagree">Very Bad Review</option>--}}
                                                {{--<option value="Disagree">Bad Review</option>--}}
                                                {{--<option value="Neither Agree or Disagree" selected="selected">Good Review</option>--}}
                                                {{--<option value="Agree">Very Good Quality</option>--}}
                                                {{--<option value="Strongly Agree">Excellent Quality</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group col-md-6 col-sm-12">--}}
                                        {{--<input class="form-control" type="text" name="yourname" Placeholder="Your Name" >--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group col-md-6 col-sm-12">--}}
                                        {{--<input class="form-control" type="text" name="youremail" Placeholder="Your Email" >--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group col-md-12 col-sm-12">--}}
                                        {{--<textarea class="ratingcomments" name="ratingcomments" placeholder="Your Comments"></textarea>--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group col-md-12 col-sm-12">--}}
                                        {{--<input id="send" class="btn btn-default" value="Send" type="submit">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    </div>
                </div>

                {{--<!-- Property Commsnts -->--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<div class="people-rating ">--}}
                            {{--<h4 class="inner-title">Review and Comments</h4>--}}
                            {{--<ul class="user-comments">--}}
                                {{--<li>--}}
                                    {{--<div class="comment_description">--}}
                                        {{--<div class="author_img">--}}
                                            {{--<img src="img/blog_detail/comment-1.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="author_text">--}}
                                            {{--<div class="author_info">--}}
                                                {{--<h5 class="author_name">Rebecca D. Nagy </h5>--}}
                                                {{--<span>27 February, 2017 at 3.27 pm</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="user-rating">--}}
                                                {{--<ul>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="deactive"></li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                            {{--<p>Fermentum mus porttitor tempor arcu posuere. Nibh consectetuer condimentum ultricies pulvinar eget pede litora interdum magna aenean ullamcorper nisi dis. Viverra. Vulputate. Quisque neque luctus quis rhoncus.</p>--}}
                                            {{--<a href="#" class="btn btn-default">Replay</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="comment_description">--}}
                                        {{--<div class="author_img">--}}
                                            {{--<img src="img/blog_detail/comment-2.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="author_text">--}}
                                            {{--<div class="author_info">--}}
                                                {{--<h5 class="author_name">Charles F. Bush</h5>--}}
                                                {{--<span>12 February, 2017 at 10.32 pm</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="user-rating">--}}
                                                {{--<ul>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                            {{--<p>Ullamcorper parturient elit, mauris congue duis morbi lacus eget id pellentesque commodo porta bibendum ullamcorper mauris dui fusce dolor massa class ultricies.</p>--}}
                                            {{--<a href="#" class="btn btn-default">Replay</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="comment_description replied">--}}
                                        {{--<div class="author_img">--}}
                                            {{--<img src="img/blog_detail/comment-1.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="author_text">--}}
                                            {{--<div class="author_info">--}}
                                                {{--<h5 class="author_name">Patty Hurd</h5>--}}
                                                {{--<span>17 February, 2017 at 11.17 am</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="user-rating">--}}
                                                {{--<ul>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="deactive"></li>--}}
                                                    {{--<li class="deactive"></li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                            {{--<p>Pretium urna nonummy sodales, dictumst blandit, magna. Quis porttitor lobortis lectus fringilla nam at Sociis vel pharetra enim per praesent viverra consequat litora, pharetra turpis magna tincidunt curae; molestie non.</p>--}}
                                            {{--<a href="#" class="btn btn-default">Replay</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="comment_description">--}}
                                        {{--<div class="author_img">--}}
                                            {{--<img src="img/blog_detail/comment-4.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="author_text">--}}
                                            {{--<div class="author_info">--}}
                                                {{--<h5 class="author_name">Joseph Richard </h5>--}}
                                                {{--<span>30 January, 2017 at 10.32 pm</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="user-rating">--}}
                                                {{--<ul>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="active"></li>--}}
                                                    {{--<li class="deactive"></li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                            {{--<p>Hac placerat. Morbi. Parturient. Nulla porta duis. Donec fames vel. Quam sem. Purus odio ultrices augue. Diam ridiculus cras luctus.</p>--}}
                                            {{--<a href="#" class="btn btn-default">Replay</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- End Property Comments -->--}}
            </div>
            <div class="col-md-4">
                <div class="property_sidebar">
                    <div class="advance_search sidebar-widget">
                        <h4 class="widget-title">Advance Search</h4>
                        <form method="post" class="search_form" action="#">
                            <div class="row">
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control">
                                        <option>Any Status</option>
                                        <option>For Rent</option>
                                        <option>For Sale</option>
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control">
                                        <option>Any Type</option>
                                        <option>House</option>
                                        <option>Office</option>
                                        <option>Appartment</option>
                                        <option>Condos</option>
                                        <option>Villa</option>
                                        <option>Small Family</option>
                                        <option>Single Room</option>
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control" data-live-search="true">
                                        <option>Any States</option>
                                        <option>New York</option>
                                        <option>Sydney</option>
                                        <option>Washington</option>
                                        <option>Las Vegas</option>
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <select class="selectpicker form-control" data-live-search="true">
                                        <option>All City</option>
                                        <option>New York</option>
                                        <option>Sydney</option>
                                        <option>Washington</option>
                                        <option>Las Vegas</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <select class="selectpicker form-control">
                                        <option>Beds</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>6</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <select class="selectpicker form-control">
                                        <option>Baths</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="min-price" placeholder="Min Price (USD)">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="email" placeholder="Max Price (USD)">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <button name="search" class="btn btn-default" type="submit">search property</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="contact_agent sidebar-widget">
                        <div class="agent_details">
                            <div class="author_img">
                                <img src="img/testimonial/2.png" alt="">
                            </div>
                            <div class="agent_info">
                                <h5 class="author_name">{{$listing->user->name}}</h5>
                                <span>+( 81 ) 84 538 91231</span>
                            </div>
                            <form class="message_agent" action="#" method="post">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="name" placeholder="Your Name">
                                </div>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="email" placeholder="Your Email">
                                </div>
                                <div class="input-group">
                                    <textarea class="form-control" name="message" placeholder="Message"></textarea>
                                </div>
                                <div class="input-group">
                                    <button type="submit" class="btn btn-default" name="submit">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="featured_sidebar sidebar-widget">
                        <h4 class="widget-title">Featured Property</h4>
                        <div class="slide_featured">
                            <div class="item">
                                <div class="thumb">
                                    <div class="sidebar_img">
                                        <img src="img/property_grid/property_grid-4.png" alt="">
                                        <div class="sale_btn">sale</div>
                                        <div class="quantity">
                                            <ul>
                                                <li><span>Area</span>1600 Sqft</li>
                                                <li><span>Rooms</span>7</li>
                                                <li><span>Beds</span>4</li>
                                                <li><span>Baths</span>3</li>
                                                <li><span>Garage</span>1</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="property-text">
                                        <a href="#"><h6 class="property-title">New Developed Condos</h6></a>
                                        <div class="property_price">$150,000</div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="sidebar_img">
                                        <img src="img/property_grid/property_grid-5.png" alt="">
                                        <div class="sale_btn">sale</div>
                                        <div class="quantity">
                                            <ul>
                                                <li><span>Area</span>1600 Sqft</li>
                                                <li><span>Rooms</span>7</li>
                                                <li><span>Beds</span>4</li>
                                                <li><span>Baths</span>3</li>
                                                <li><span>Garage</span>1</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="property-text">
                                        <a href="#"><h6 class="property-title">New Developed Condos</h6></a>
                                        <div class="property_price">$150,000</div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="sidebar_img">
                                        <img src="img/property_grid/property_grid-6.png" alt="">
                                        <div class="sale_btn">sale</div>
                                        <div class="quantity">
                                            <ul>
                                                <li><span>Area</span>1600 Sqft</li>
                                                <li><span>Rooms</span>7</li>
                                                <li><span>Beds</span>4</li>
                                                <li><span>Baths</span>3</li>
                                                <li><span>Garage</span>1</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="property-text">
                                        <a href="#"><h6 class="property-title">New Developed Condos</h6></a>
                                        <div class="property_price">$150,000</div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumb">
                                    <div class="sidebar_img">
                                        <img src="img/property_grid/property_grid-7.png" alt="">
                                        <div class="sale_btn">sale</div>
                                        <div class="quantity">
                                            <ul>
                                                <li><span>Area</span>1600 Sqft</li>
                                                <li><span>Rooms</span>7</li>
                                                <li><span>Beds</span>4</li>
                                                <li><span>Baths</span>3</li>
                                                <li><span>Garage</span>1</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="property-text">
                                        <a href="#"><h6 class="property-title">New Developed Condos</h6></a>
                                        <div class="property_price">$150,000</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Related Property -->
        <div class="row">
            <div class="col-md-12">
                <div class="inner-feature">
                    <h4 class="inner-title">Related Properties</h4>
                    <div class="property_slide">
                        <div class="item">
                            <div class="property_grid">
                                <div class="img_area">
                                    <div class="sale_btn">Rent</div>
                                    <a href="#"><img src="img/property_grid/property_grid-1.png" alt=""></a>
                                    <div class="sale_amount">12 Hours Ago</div>
                                    <div class="hover_property">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-text">
                                    <a href="#">
                                        <h5 class="property-title">Park Road Appartment Rent</h5>
                                    </a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 3225 George Street Brooksville, FL 34610</span>
                                    <div class="quantity">
                                        <ul>
                                            <li><span>Area</span>1600 Sqft</li>
                                            <li><span>Rooms</span>9</li>
                                            <li><span>Beds</span>4</li>
                                            <li><span>Baths</span>3</li>
                                            <li><span>Garage</span>1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bed_area">
                                    <ul>
                                        <li>$1250/mo</li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="property_grid">
                                <div class="img_area">
                                    <div class="sale_btn">Sale</div>
                                    <div class="featured_btn">Featured</div>
                                    <a href="#"><img src="img/property_grid/property_grid-2.png" alt=""></a>
                                    <div class="sale_amount">12 Hours Ago</div>
                                    <div class="hover_property">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-text">
                                    <a href="#">
                                        <h5 class="property-title">Park Road Appartment Rent</h5>
                                    </a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 3494 Lyon Avenue Middleboro, MA 02346 </span>
                                    <div class="quantity">
                                        <ul>
                                            <li><span>Area</span>1600 Sqft</li>
                                            <li><span>Rooms</span>9</li>
                                            <li><span>Beds</span>4</li>
                                            <li><span>Baths</span>3</li>
                                            <li><span>Garage</span>1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bed_area">
                                    <ul>
                                        <li>$1,410,000</li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="property_grid">
                                <div class="img_area">
                                    <div class="sale_btn">Rent</div>
                                    <a href="#"><img src="img/property_grid/property_grid-3.png" alt=""></a>
                                    <div class="sale_amount">12 Hours Ago</div>
                                    <div class="hover_property">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-text">
                                    <a href="#">
                                        <h5 class="property-title">Park Road Appartment Rent</h5>
                                    </a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 39 Parrill Court Oak Brook, IN 60523 </span>
                                    <div class="quantity">
                                        <ul>
                                            <li><span>Area</span>1600 Sqft</li>
                                            <li><span>Rooms</span>9</li>
                                            <li><span>Beds</span>4</li>
                                            <li><span>Baths</span>3</li>
                                            <li><span>Garage</span>1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bed_area">
                                    <ul>
                                        <li>$1200/mo</li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="property_grid">
                                <div class="img_area">
                                    <div class="sale_btn">Rent</div>
                                    <a href="#"><img src="img/property_grid/property_grid-4.png" alt=""></a>
                                    <div class="sale_amount">12 Hours Ago</div>
                                    <div class="hover_property">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="property-text">
                                    <a href="#">
                                        <h5 class="property-title">Central Road Appartment Rent</h5>
                                    </a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 39 Parrill Court Oak Brook, IN 60523 </span>
                                    <div class="quantity">
                                        <ul>
                                            <li><span>Area</span>1600 Sqft</li>
                                            <li><span>Rooms</span>9</li>
                                            <li><span>Beds</span>4</li>
                                            <li><span>Baths</span>3</li>
                                            <li><span>Garage</span>1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bed_area">
                                    <ul>
                                        <li>$1200/mo</li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
                                        <li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Related Property -->
    </div>
</section>
<!-- Single Property End -->
    @endsection
		
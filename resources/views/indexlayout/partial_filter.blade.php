@foreach($listing as $list)
    <div class="property_grid">
        <div class="img_area">
            <div class="sale_btn">Rent</div>
            <a href="#"><img src="{{asset('uniland/img/property_grid/property_grid-2.png')}}" alt=""></a>
            <div class="sale_amount">12 Hours Ago</div>
            <div class="hover_property">
                <ul>
                    <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="property-text">
            <a href="#"><h6 class="property-title">Park Road Appartment Rent</h6></a>
            <span><i class="fa fa-map-marker" aria-hidden="true"></i> 3225 George Street Brooksville, FL 34610</span>
            <div class="quantity">
                <ul>
                    <li><span>Area</span>1600 Sqft</li>
                    <li><span>Rooms</span>9</li>
                    <li><span>Beds</span>4</li>
                    <li><span>Baths</span>3</li>
                    <li><span>Garage</span>1</li>
                </ul>
            </div>
        </div>
        <div class="bed_area">
            <ul>
                <li>$1250/month</li>
                <li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
                <li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
            </ul>
        </div>
    </div>
@endforeach